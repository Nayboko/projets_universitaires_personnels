package csp_etud;

import java.io.BufferedReader;
import java.io.FileReader;

public class MesReponses {

	public static void main(String[] args) throws Exception {
		
		System.out.println("### Solveur CSP - TP NOTE ###");

		System.out.println(" >> Question 1 <<");
		String fileName="csp1.txt";
		Network myNetwork = null;
		System.out.println("Chargement du fichier : "+ new java.io.File(".").getCanonicalPath()+ "/"+ fileName);
		try {
			BufferedReader readFile = new BufferedReader(new FileReader(fileName));
			myNetwork = new Network(readFile);
			readFile.close();
			System.out.println("Chargement termine avec succes !");
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		
		System.out.println("# Execution du solveur CSP #");
		
		CSP csp1 = new CSP(myNetwork);
		CSP csp1All = new CSP(myNetwork);
		csp1.searchSolution(); 			// Une solution du probleme
		csp1All.searchAllSolutions();	// Toutes les solutions
		
		
		
		
		System.out.println(" >> Question 2 <<");
		String fileName2="csp2.txt";
		Network myNetwork2 = null ;
		System.out.println("Chargement du fichier : "+ new java.io.File(".").getCanonicalPath()+ "/"+ fileName2);
		try {
			BufferedReader readFile = new BufferedReader(new FileReader(fileName2));
			myNetwork2 = new Network(readFile);
			readFile.close();
			System.out.println("Chargement termine avec succes !");
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		
		System.out.println("# Execution du solveur CSP #");
		
		CSP csp2 = new CSP(myNetwork2);
		CSP csp2All = new CSP(myNetwork2);
		csp2.searchSolution(); 			// Une solution du probleme
		csp2All.searchAllSolutions();	// Toutes les solutions
		
		System.out.println(" >> Question 3 <<");
		String fileName3="5reines.txt";
		Network myNetwork3 = null ;
		System.out.println("Chargement du fichier : "+ new java.io.File(".").getCanonicalPath()+ "/"+ fileName3);
		try {
			BufferedReader readFile = new BufferedReader(new FileReader(fileName3));
			myNetwork3 = new Network(readFile);
			readFile.close();
			System.out.println("Chargement termine avec succes !");
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		
		System.out.println("# Execution du solveur CSP #");
		
		CSP csp3 = new CSP(myNetwork3);
		CSP csp3All = new CSP(myNetwork3);
		csp3.searchSolution(); 			// Une solution du probleme
		csp3All.searchAllSolutions();	// Toutes les solutions
		
		System.out.println(" >> Question 4 <<");
		String fileName4="8reines.txt";
		Network myNetwork4 = null ;
		System.out.println("Chargement du fichier : "+ new java.io.File(".").getCanonicalPath()+ "/"+ fileName4);
		try {
			BufferedReader readFile = new BufferedReader(new FileReader(fileName4));
			myNetwork4 = new Network(readFile);
			readFile.close();
			System.out.println("Chargement termine avec succes !");
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		
		System.out.println("# Execution du solveur CSP #");
		
		CSP csp4 = new CSP(myNetwork4);
		CSP csp4All = new CSP(myNetwork4);
		csp4.searchSolution(); 			// Une solution du probleme
		csp4All.searchAllSolutions();	// Toutes les solutions
		
		System.out.println(" >> Question 5 <<");
		String fileName5="colore.txt";
		Network myNetwork5 = null ;
		System.out.println("Chargement du fichier : "+ new java.io.File(".").getCanonicalPath()+ "/"+ fileName5);
		try {
			BufferedReader readFile = new BufferedReader(new FileReader(fileName5));
			myNetwork5 = new Network(readFile);
			readFile.close();
			System.out.println("Chargement termine avec succes !");
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		
		System.out.println("# Execution du solveur CSP #");
		
		CSP csp5 = new CSP(myNetwork5);
		CSP csp5All = new CSP(myNetwork5);
		csp5.searchSolution(); 			// Une solution du probleme
		csp5All.searchAllSolutions();	// Toutes les solutions
		
		System.out.println(" >> Question 6 <<");
		String fileName6="5reinesExp.txt";
		Network myNetwork6 = null ;
		System.out.println("Chargement du fichier : "+ new java.io.File(".").getCanonicalPath()+ "/"+ fileName6);
		try {
			BufferedReader readFile = new BufferedReader(new FileReader(fileName6));
			myNetwork6 = new Network(readFile);
			readFile.close();
			System.out.println("Chargement termine avec succes !");
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		
		System.out.println("# Execution du solveur CSP #");
		
		CSP csp6 = new CSP(myNetwork6);
		CSP csp6All = new CSP(myNetwork6);
		csp6.searchSolution(); 			// Une solution du probleme
		csp6All.searchAllSolutions();	// Toutes les solutions
		
		System.out.println(" >> Question 7 <<");
		String fileName7="cryptoMoney.txt";
		Network myNetwork7 = null ;
		System.out.println("Chargement du fichier : "+ new java.io.File(".").getCanonicalPath()+ "/"+ fileName7);
		try {
			BufferedReader readFile = new BufferedReader(new FileReader(fileName7));
			myNetwork7 = new Network(readFile);
			readFile.close();
			System.out.println("Chargement termine avec succes !");
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		
		System.out.println("# Execution du solveur CSP #");
		
		CSP csp7 = new CSP(myNetwork7);
		CSP csp7All = new CSP(myNetwork7);
		csp7.searchSolution(); 			// Une solution du probleme
		csp7All.searchAllSolutions();	// Toutes les solutions
		
		System.out.println(" >> Question 8 <<");
		String fileName8="cryptoMoney.txt";
		Network myNetwork8 = null ;
		System.out.println("Chargement du fichier : "+ new java.io.File(".").getCanonicalPath()+ "/"+ fileName8);
		try {
			BufferedReader readFile = new BufferedReader(new FileReader(fileName8));
			myNetwork8 = new Network(readFile);
			readFile.close();
			System.out.println("Chargement termine avec succes !");
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		
		System.out.println("# Execution du solveur CSP #");
		
		CSP csp8 = new CSP(myNetwork8);
		CSP csp8All = new CSP(myNetwork8);
		csp8.searchSolution(); 			// Une solution du probleme
		csp8All.searchAllSolutions();	// Toutes les solutions
	}

}
