package csp_etud;

import java.io.BufferedReader;
import java.util.ArrayList;

/**
 * Classe abstraite pour une contrainte. Cette classe a vocation
 * a etre specialisee pour chaque type de contrainte en redefinissant :
 * un constructeur specifique, un constructeur permettant de lire la contrainte
 * a partir d'une representation textuelle, une methode de test de la violation de la contrainte,
 * la methode de representation textuelle de la contrainte.
 * 
 */
public abstract class Constraint {
	
	protected static int num = 0; 			// pour generer un nom different pour chaque contrainte
	protected String name; 					// nom de la contrainte
	protected ArrayList<String> varList;	// portee de la contrainte : la liste des differentes variables
											//                           mises en jeu par la contrainte
	
	/**
	 * Construit une contrainte portant sur une liste de variables.
	 * Les variables doivent etre differentes. L'arite de la contrainte
	 * est la taille de la liste.
	 * Le nom de la contrainte est genere automatiquement
	 * 
	 * @param vars les variables sur lesquelles la contrainte porte
	 * 
	 */
	public Constraint(ArrayList<String> vars) {
		num++;
		this.name = "C"+num;
		varList = vars; 
	}
	
	/**
	 * Construit une contrainte portant sur une liste de variables.
	 * Les variables doivent etre differentes. L'arite de la contrainte
	 * est la taille de la liste.
	 * Le nom de la contrainte est passe en parametre
	 * 
	 * @param vars les variables de la contrainte 
	 * @param name le nom de la contrainte
	 */
	public Constraint(ArrayList<String> vars, String name) {
		num++;
		this.name = name;
		varList = vars; 
	}
	
	/**
	 * Construit une contrainte a partir d'une representation textuelle de
	 * la contrainte. La liste de variables est donnee sous la forme : Var1;Var2;...
	 * Les variables doivent etre differentes. L'arite de la contrainte
	 * correspond au nombre de variables lues.
	 * Le nom de la contrainte est genere automatiquement.
	 * 
	 * @param in le buffer de lecture de la representation textuelle de la contrainte
	 * @throws Exception en cas d'erreur de format 
	 */
	public Constraint(BufferedReader in) throws Exception {
		num++;
		this.name = "C"+num;
		varList = new ArrayList<String>();
		for (String v : in.readLine().split(";")) varList.add(v);	// Var1;Var2;...;Var(arity)
	}
	
	/**
	 * Retourne l'arite de la contrainte
	 * 
	 * @return l'arite
	 */
	public int getArity() {
		return varList.size();
	}
	
	/**
	 * Retourne le nom de la contrainte
	 * 
	 * @return son nom
	 */
	public String getName() {
		return name;
	}
	
	
	/**
	 * Retourne la portee de la contrainte, c'est a dire la liste de ses variables 
	 * 
	 * @return la liste des variables de la contrainte
	 */
	public ArrayList<String> getVars() {
		return varList;
	}

	/**
	 * 
	 * Teste si une assignation viole la contrainte. 
	 * Doit etre implantee dans la classe concrete. 
	 * @param a l'assignation a tester
	 * @return vrai ssi l'assignation viole la contrainte 
	 */
	public abstract boolean violation(Assignment a);	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "\n\t"+ name + " " + varList; 
	}

}
