package csp_etud;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Solveur : permet de resoudre un probleme de contrainte par Backtrack : 
 * 	Calcul d'une solution, 
 * 	Calcul de toutes les solutions
 *
 */
public class CSP {

		private Network network;					// le reseau a resoudre
		private ArrayList<String> varList;			// l'ordonnancement des variables
		private ArrayList<Assignment> solutions; 	// les solutions du reseau (resultat de searchAllSolutions)
		protected Assignment assignment;			// l'assignation courante (resultat de searchSolution)
		int cptr;									// le compteur de noeuds explores

		
		/**
		 * Cr�ee un probleme de resolution de contraintes pour un reseau donne
		 * 
		 * @param r le reseau de contraintes a� resoudre
		 */
		public CSP(Network r) {
			network = r;
			varList = this.network.getVars();
			solutions = new ArrayList<Assignment>();
			assignment = new Assignment();

		}
		
		/********************** ORDONNANCEMENT CHOIX VARIABLES *******************************************/
		
		public ArrayList<String> ordoVar(ArrayList<String> listVar) {
			ArrayList<String> ordoList = new ArrayList<String>();
			ArrayList<String> tmpList = new ArrayList<String>();
			ArrayList<String> varRest = listVar;
			// Choix variable dom+deg+lexico
			
			// Tant qu'on a pas ordonn� toutes les variables
			while(!varRest.isEmpty()){
				tmpList.clear();
				tmpList = minDom(varRest);
				if(tmpList.size() == 1) {
					ordoList.add(tmpList.get(0));
					varRest.remove(tmpList.get(0));
				}
				else{
					tmpList = maxDeg(tmpList);
					if(tmpList.size() == 1) {
						ordoList.add(tmpList.get(0));
						varRest.remove(tmpList.get(0));
					}
					else{
						tmpList = lex(tmpList);
						ordoList.add(tmpList.get(0));
						varRest.remove(tmpList.get(0));
					}
				}	
			}

			return ordoList;
		}
		


		private ArrayList<String> minDom(ArrayList<String> listVar) {
			ArrayList<String> minDom = new ArrayList<String>();
			ArrayList<Integer> taille = new ArrayList<Integer>();
			
			// On r�cup�re la taille des domaines de chaque variable de listVar
			for(String x: listVar) {
				taille.add(this.network.getDomSize(x));
			}
			
			// On cherche quelle est la taille la plus petite
			int min = Collections.min(taille);
			
			// Puis on ajoute dans minDom les variables ayant une taille de domaine minimale
			for(int i = 0; i < listVar.size(); i++) {
				if(taille.get(i) == min) {
					minDom.add(listVar.get(i));
				}
			}
			return minDom;
		}
		
		private ArrayList<String> maxDeg(ArrayList<String> listVar) {
			ArrayList<String> maxDeg = new ArrayList<String>();
			ArrayList<Integer> taille = new ArrayList<Integer>();
			
			// On r�cup�re la taille des domaines de chaque variable de listVar
			for(String x: listVar) {
				taille.add(this.network.getConstraints(x).size());
			}
			
			// On cherche quelle est le degr� le plus grand
			int max = Collections.max(taille);
			
			// Puis on ajoute dans maxDeg les variables ayant un degr� maximal
			for(int i = 0; i < listVar.size(); i++) {
				if(taille.get(i) == max) {
					maxDeg.add(listVar.get(i));
				}
			}
			
			return maxDeg;
		}
		
		private ArrayList<String> lex(ArrayList<String> listVar) {
			Collections.sort(listVar);
			return listVar;
		}
		


		/********************** BACKTRACK UNE SOLUTION *******************************************/
		 
		/**
		 * Cherche une solution au reseau de contraintes
		 * 
		 * @return une assignation solution du reseau, ou null si pas de solution
		 */
		
		public Assignment searchSolution() {
			System.out.println("\nRecherche d'une solution par backtracking du r�seau de contraintes\n");
			cptr=1;
			
			System.out.println("Avant:\t"+ varList);
			varList = ordoVar(varList);
			System.out.println("Apres:\t" + varList);
			// Implanter appel a backtrack
			Assignment sol = this.backtrack();
			
			System.out.println("\tUne solution a ete trouvee : "+ sol);
			
			System.out.println("\tIl y a "+ this.cptr + " noeuds qui ont ete explore");
			
			return sol;
		}
		

		/* La methode bactrack ci-dessous travaille directement sur l'attribut assignment. 
		 * On peut aussi choisir de ne pas utiliser cet attribut et de creer plutot un objet Assignment local a searchSolution : 
		 * dans ce cas il faut le passer en parametre de backtrack
		 */
		/**
		 * Execute l'algorithme de backtrack a la recherche d'une solution en etendant l'assignation courante
		 * Utilise l'attribut assignment
		 * @return la prochaine solution ou null si pas de nouvelle solution
		 */
		
		private Assignment backtrack() {
		    // A IMPLANTER
		    // AJOUTER UN PARAMETRE DE TYPE ASSIGNMENT SI ON NE TRAVAILLE PAS DIRECTEMENT SUR L'ATTRIBUT assignment
		    // quelque part : cptr++
			
			// Si |A| = |R.X| alors je retourne mon assignation
			if(this.assignment.size() == this.network.getVarNumber()) {
				// System.out.println(assignment.toString());
				return this.assignment;
			}
			// Choix d'une variable non assignee selon strategie de chooseVar()
			String x = this.chooseVar();
			ArrayList<Object> Dom = new ArrayList<Object>();
			Dom = this.network.getDom(x);
			Dom = this.tri(Dom);
			// On va parcourir le domaine de notre nouvelle variable
			for(Object v: Dom) {
				this.assignment.put(x, v);
				// System.out.println("a("+x+") :" + " " + v);
				cptr++; // Nombre de noeuds explores
				// Si elle ne viole pas de contrainte
				if(this.consistant(x)) {
					Assignment b = this.backtrack();
					// A-t'on assigne toutes les variables ?
					// Si b n'est pas une feuille
					if(b != null) return b;
				}
				// Si l'assignation trouvee viole une contrainte
					this.assignment.remove(x,v);
			}
		    // System.err.println("Pas de solutions.");
		    return null;
		}
		
		
		
		/********************** BACKTRACK TOUTES SOLUTIONS *******************************************/
		
		/**
		 * Calcule toutes les solutions au reseau de contraintes
		 * 
		 * @return la liste des assignations solution
		 * 
		 */
		public ArrayList<Assignment> searchAllSolutions(){
			System.out.println("\nRecherche de toutes les solutions par backtracking du r�seau de contraintes\n");
			cptr=1;
			solutions.clear(); // SI ON CHOISIT DE TRAVAILLER DIRECTEMENT SUR L'ATTRIBUT SOLUTIONS
			varList = ordoVar(varList);
			this.backtrackAll();
			System.out.println("Pour afficher toutes les solutions, veuillez d�commenter la ligne 219 du fichier CSP.java");
			System.out.println("\tNombre de solutions : "+ this.solutions.size());
			System.out.println("\tNombre de noeuds explores : " + this.cptr);
			return solutions;
		}
		
		/**
		 * Execute l'algorithme de backtrack a la recherche de toutes les solutions
		 * etendant l'assignation courante
		 * 
		 */
		private void backtrackAll() {
		    // AJOUTER UN PARAMETRE DE TYPE ArrayList<Assignment> SI ON NE TRAVAILLE PAS DIRECTEMENT SUR L'ATTRIBUT solutions
		    // quelque part : cptr++;
			if(this.assignment.size() == this.network.getVarNumber()) {
				// System.out.println(assignment.toString());
				this.solutions.add(this.assignment.clone());
			}else {
				// Choix d'une variable non assignee
				String x = this.chooseVar();
				ArrayList<Object> Dom = new ArrayList<Object>();
				Dom = this.network.getDom(x);
				Dom = this.tri(Dom);
				// On va parcourir le domaine
				for(Object v: Dom) {
					this.assignment.put(x, v);
					this.cptr++; // Nombre de noeuds explores
					if(this.consistant(x)) {
						this.backtrackAll();
					}
					this.assignment.remove(x, v);
				}
			}
		}
		
		
		/**
		 * Retourne la prochaine variable a assigner etant donne assignment (qui doit contenir la solution partielle courante)
		 *  
		 * @return une variable non encore assignee
		 */
		private String chooseVar() {
		    // return this.network.getVars().get(this.assignment.getVars().size());
			return this.varList.get(this.assignment.getVars().size());
		}
		
		/*****************************************************************/
		
		/**
		 * Retourne la prochaine variable a assigner etant donne la solution partielle passee en parametre
		 *  
		 *  @param sol solution partielle courante
		 * @return une variable non encore assignee
		 */
		/* private String chooseVar(Assignment sol) {
			// A IMPLANTER
			System.err.println("Methode chooseVar() a implanter !!!");
			return null;
		}*/
		
		
		private ArrayList<Object> tri(ArrayList<Object> values) {
			return values; // donc en l'etat n'est pas d'une grande utilite !
		}
		


  // IMPLANTER l'UNE DES DEUX METHODES CONSISTANT CI-DESSOUS (SELON QUE L'ASSIGNATION COURANTE EST PASSEE EN PARAMETRE OU PAS)
		
		/**
		 * Teste si l'assignation courante stokee dans l'attribut assignment est consistante, c'est a dire qu'elle
		 * ne viole aucune contrainte.
		 * 
		 * @param lastAssignedVar la variable que l'on vient d'assigner a cette etape
		 * @return vrai ssi l'assignment courante ne viole aucune contrainte
		 */
		private boolean consistant(String lastAssignedVar) {
			// On parcourt les contraintes contenant 'lastAssignedVar'
			for(Constraint c : this.network.getConstraints(lastAssignedVar)) {
				if(c.violation(this.assignment)) return false; // Violation donc pas consistant
			}
			return true;
		}
		
		/**
		 * Teste si l'assignation courante stockee dans assignment est consistante par rapport a sol, c'est a dire qu'elle
		 * ne viole aucune contrainte.
		 * 
		 * @param sol solution partielle courante
		 * @param lastAssignedVar la variable que l'on vient d'assigner a cette etape
		 * @return vrai ssi l'assignment courante ne viole aucune contrainte
		 */
		/* private boolean consistant(Assignment sol, String lastAssignedVar) {
			// A IMPLANTER
			System.err.println("Methode consistant() a implanter !!!");
			return true;

		}*/
}
