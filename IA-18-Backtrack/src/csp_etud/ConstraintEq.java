package csp_etud;

import java.io.BufferedReader;

public class ConstraintEq extends Constraint {
	public ConstraintEq(BufferedReader in) throws Exception{
		super(in);
	}

	@Override
	public boolean violation(Assignment a) {
		//pour chaque variable mise en jeu par la contrainte
		for(int i = 0; i < this.varList.size(); i++) {
			// si la variable n'est pas dans l'assignation
			if(!a.containsKey(this.varList.get(i))) {
				return false; //pas de violation
			}
		}
		
		Object temoin = a.get(varList.get(0));
		for(int i = 1; i < this.varList.size(); i++) {
			if(!a.get(varList.get(i)).equals(temoin)) return true;
		}
		
		return false;
	}
	
	public String toString() {
		return "\n\tEquals : " + super.toString();
	}
}
