package csp_etud;

import java.io.BufferedReader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ConstraintExprBool extends Constraint {

    String expression;
    ScriptEngineManager mgr;
    ScriptEngine engine;
    
    public ConstraintExprBool(BufferedReader in) throws Exception {
        super(in);
        expression= in.readLine();
        mgr = new ScriptEngineManager();
        engine = mgr.getEngineByName("JavaScript");
    }

    public boolean violation(Assignment a) {
        int i=0;
        String expTemp=expression;
        for(i=0;i<varList.size();i++){
            if(!a.getVars().contains(varList.get(i))){ //Si la case = null alors au moins une var n'a pas �t� assign�e -> pas de violation
                return false;
            }
            expTemp=expTemp.replace(varList.get(i),""+a.get(varList.get(i)));
        }
        try {
            return !(boolean) engine.eval(expTemp); // r�sultat de l'�valuation
        }
       catch (ScriptException e) {
           System.err.println("Probleme dans: "+ expTemp);
           return true;
       } 
    }
	public String toString() {
		return "\n\t Expression booleenne : "+ super.toString(); 
	}

}