package csp_etud;

import java.io.BufferedReader;
import java.util.ArrayList;

/**
 * Pour manipuler des contraintes en extension
 *
 */
public class ConstraintExt extends Constraint{
	
	private ArrayList<ArrayList<Object>> tuples;	// ensemble des tuples de la contrainte
	
	/**
	 * Construit une contrainte d'extension vide a partir
	 * d'une liste de variables
	 * 
	 * @param var la liste de variables
	 */
	public ConstraintExt(ArrayList<String> var) {
		super(var);
		tuples = new ArrayList<ArrayList<Object>>();
	}
	
	/**
	 * Construit une contrainte d'extension vide a partir
	 * d'une liste de variables et d'un nom
	 * 
	 * @param var la liste de variables
	 * @param name son nom
	 */
	public ConstraintExt(ArrayList<String> var, String name) {
		super(var,name);
		tuples = new ArrayList<ArrayList<Object>>();
	}
	
	/**
	 * Construit une contrainte en extension a partir d'une representation
	 * textuelle de la contrainte. La liste de variables est donnee sous la forme : Var1;Var2;...
	 * Puis le nombre de tuples est indique et enfin chaque tupe est donne sous la forme d'une
	 * suite de valeurs "String" : Val1;Val2;...
	 * Aucune verification n'est prevue si la syntaxe n'est pas respectee !!
	 * 
	 * @param in le buffer de lecture de la representation textuelle de la contrainte
	 * @throws Exception en cas d'erreur de format
	 */
	public ConstraintExt(BufferedReader in) throws Exception{
		super(in);
		tuples = new ArrayList<ArrayList<Object>>();
		int nbTuples = Integer.parseInt(in.readLine());		// nombre de tuples de valeurs
		for(int j=1;j<=nbTuples;j++) {
			ArrayList<Object> tuple = new ArrayList<Object>();
			for (String v : in.readLine().split(";")) tuple.add(v);	// Val1;Val2;...;Val(arity)
			if(tuple.size() != varList.size()) 
				System.err.println("Le tuple " + tuple + " n'a pas l'aritee " + varList.size() + " de la contrainte " + name);
			else if(!tuples.add(tuple)) System.err.println("Le tuple " + tuple + " est deja present dans la contrainte "+ name);
		}
	}
	
	/**
	 * Ajoute un tuple de valeur a la contrainte
	 * 
	 * @param valTuple le tuple a ajouter
	 */
	public void addTuple(ArrayList<Object> valTuple) {
		if(valTuple.size() != varList.size()) 
			System.err.println("Le tuple " + valTuple + " n'a pas l'arite " + varList.size() + " de la contrainte " + name);
		else if(!tuples.add(valTuple)) System.err.println("Le tuple " + valTuple + " est deja present dans la contrainte "+ name);
	}
	

	/**
	 * Teste si une assignation viole la contrainte. 
	 * La violation "classique" n'est averee que si :
     *      - toutes les variables de la contrainte ont une valeur associee dans l'assignation testee
     *      - le tuple de valeur pour les variables de la contrainte dans l'assignation testee n'appartient
     *        pas aux tuples autorises par la contrainte
	 * @param a l'assignation a tester
	 * @return vrai ssi l'assignation viole la contrainte 
	 */
	public boolean violation(Assignment a) {
		for(int i = 0;i<this.varList.size();i++) { // pour chaque variables mises en jeu par la contrainte
	            if(a.containsKey(this.varList.get(i)) == false) // pas de violation si la clef de l'assignement est contenue dans la contrainte
	                return false;					// violation sinon car pas arc consistante
	    }
		
        for (ArrayList<Object> t : this.tuples)	{
	            if(autorized(t,a)) 			// si tuple autoris�, donc est un tuple support
	                return false;			// pas de violation
        } 
        return true;
	}
	
	public boolean autorized(ArrayList<Object> tuple, Assignment a){
		
        for(int i = 0; i<varList.size(); i++){
            if(a.get(varList.get(i)).equals((tuple.get(i))) == false) // si valeur assignee de la contrainte
                return false;									// n'est pas egale a la valeur du tuple
            													// alors l'assignation n'est pas bonne
        }
        return true; // sinon toutes les valeurs assignee sont des tuples de contrainte, c'est une bonne assignation
    }
	
	/* (non-Javadoc)
	 * @see Constraint#toString()
	 */
	public String toString() {
		return "\n\t Ext "+ super.toString() + " : " + tuples; 
	}

}
