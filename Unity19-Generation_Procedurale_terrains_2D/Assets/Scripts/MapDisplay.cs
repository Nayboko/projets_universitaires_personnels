﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapDisplay : MonoBehaviour
{
    public Renderer textureRenderer;

    public void DrawTexture(Texture2D texture)
    {
        textureRenderer.sharedMaterial.mainTexture = texture; // shareMaterial à la place de material car on le veut dans la scène et non pas dans le runtime (bouton play)
        textureRenderer.transform.localScale = new Vector3(texture.width, 1, texture.height); //height en troisième car correspond à la profondeur
    }
}
