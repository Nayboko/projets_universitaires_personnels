﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class coinAmount : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Text>().text = "0";
    }


    public void SaveCoin()
    {
        int coinAlreadyCollected = PlayerPrefs.GetInt("CoinAmount");
        PlayerPrefs.SetInt("CoinAmount", coinAlreadyCollected + int.Parse(gameObject.GetComponent<Text>().text));
    }
}
