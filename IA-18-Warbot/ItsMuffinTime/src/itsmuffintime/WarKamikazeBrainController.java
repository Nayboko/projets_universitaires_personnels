package itsmuffintime;

import edu.warbot.agents.agents.WarExplorer;
import edu.warbot.agents.agents.WarKamikaze;
import edu.warbot.agents.enums.WarAgentType;
import edu.warbot.agents.percepts.WarAgentPercept;
import edu.warbot.brains.brains.WarKamikazeBrain;
import edu.warbot.communications.WarMessage;

import java.util.List;

public abstract class WarKamikazeBrainController extends WarKamikazeBrain {

	private double DistSelfToEnemy;
	private double AngleSelfToEnemy;
	private Vector2 myPosition;
	
    public WarKamikazeBrainController() {
        super();
        this.DistSelfToEnemy = -1;
        this.AngleSelfToEnemy = -1;
        this.myPosition = new Vector2();
    }

    @Override
    public String action() {
    	
    	this.rcvMessage();
    	
    	if(this.myRoles("Attack").contains("Explode") && this.getHealth() <= 0.2 * this.getMaxHealth())
    		return WarKamikaze.ACTION_FIRE;
    	
        List <WarAgentPercept> percepts = getPercepts();
        
        for (WarAgentPercept p : percepts) {
            if(p.getType().equals(WarAgentType.WarBase) && this.isEnemy(p)) {
            	broadcastMessageToAll("Ennemi Base Found", String.valueOf(p.getAngle()), String.valueOf(p.getDistance()));
            	setHeading(p.getAngle);
            	// Si ma base m'a demand� d'attaquer
            	if(this.myRoles("Attack").contains("Explode")) {
                	if(this.getPerceptsAllies().size() <= 1 &&  p.getDistance() <= 1){
                    	return WarKamikaze.ACTION_FIRE;
                    }
                } // sinon je poursuis mon chemin
            }
        }

        if (isBlocked())
            setRandomHeading();
        return WarExplorer.ACTION_MOVE;
    }
    
    
    public void rcvMessage() {
    	List<WarMessage> messages = this.getMessages();
    	for(WarMessage message : messages) {
    		if(message.getMessage().equals("This is yours coords")) {
    			String[] contents = message.getContent();
    			if(myPosition == null) {
    				myPosition = new Vector2();
    			}
    			myPosition.x = Float.parseFloat(contents[0]);
    			myPosition.y = Float.parseFloat(contents[1]);
    		}
    		// On me demande d'attaquer la base ennemie
			if (message.getMessage().equals("Fire in the hole!")) {
	         	String[] content = message.getContent();
	         	
	     		double DistBaseToEnemy = Double.parseDouble(content[0]);
	     		double AngleBaseToEnemy = Double.parseDouble(content[1]);
	
	     		this.DistSelfToEnemy = Calculs.DistObjectToSelf(DistBaseToEnemy, AngleBaseToEnemy, message.getDistance(), message.getAngle());
	         	this.AngleSelfToEnemy = Calculs.AngleObjectToSelf(Double.parseDouble(content[0]), Double.parseDouble(content[1]), message.getDistance(), message.getAngle());
	     		
	         	this.setHeading(AngleSelfToEnemy);
	         	this.requestRole("Attack", "Explode");
	     	}
    	}
    }
}
