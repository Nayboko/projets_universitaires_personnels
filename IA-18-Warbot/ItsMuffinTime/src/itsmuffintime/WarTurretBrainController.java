package itsmuffintime;


import edu.warbot.agents.agents.WarTurret;
import edu.warbot.agents.enums.WarAgentType;
import edu.warbot.agents.percepts.WarAgentPercept;
import edu.warbot.brains.brains.WarTurretBrain;


import java.util.List;

public abstract class WarTurretBrainController extends WarTurretBrain {

    private int _sight;
    private Double[] ennemiPosition = {-1.0,-1.0,-1.0};
    WarAgentPercept ennemy;
    
    public WarTurretBrainController() {
        super();

        _sight = 0;
        ennemy = null;
    }

    @Override
    public String action() {
    	
    	boolean ennemyStillHere = false;

    	if(ennemiPosition[2] == -1.0) {
	        _sight += 90;
	        if (_sight == 360) {
	            _sight = 0;
	        }
	        setHeading(_sight);
    	}

        if (getNbElementsInBag() >= 0 && getHealth() <= 0.8 * getMaxHealth())
            return WarTurret.ACTION_EAT;
        if(this.getNbElementsInBag() <= 3)
        	this.broadcastMessageToAgentType(WarAgentType.WarExplorer, "Food please", "");
        
        List <WarAgentPercept> percepts = getPercepts();
        if(ennemiPosition[2] != -1.0)
	        for(WarAgentPercept p : percepts) {
	        	if(p.getID() == this.ennemiPosition[2]) {
	        		ennemyStillHere = true;
	        		ennemy = p;
	        	}
	        }
        if(ennemyStillHere) {
        	this.setHeading(Calculs.CalculAnticipation(ennemy, WarAgentType.WarTurret));
        	this.ennemiPosition[0] = ennemy.getDistance();
        	this.ennemiPosition[1] = ennemy.getAngle();
        	this.ennemiPosition[2] = (double)ennemy.getID();
        	if(this.isReloaded()) return WarTurret.ACTION_FIRE;
        	else return WarTurret.ACTION_RELOAD;
        }else {
        	this.ennemiPosition[0] = -1.0;
        	this.ennemiPosition[1] = -1.0;
        	this.ennemiPosition[2] = -1.0;
        	ennemy = null;
        }
        
        for (WarAgentPercept p : percepts) {
        	WarAgentType pT = p.getType();
        	// Si c'est un ennemi et que ce n'est pas une ressource
            if(this.isEnemy(p) && !pT.equals(WarAgentType.WarFood)) {
            	if(pT.equals(WarAgentType.WarLight))
            		this.broadcastMessageToAgentType(WarAgentType.WarHeavy, "Help", String.valueOf(p.getDistance()), String.valueOf(p.getAngle()));
            	else if(pT.equals(WarAgentType.WarHeavy))
            		this.broadcastMessageToAgentType(WarAgentType.WarRocketLauncher, "Help", String.valueOf(p.getDistance()), String.valueOf(p.getAngle()));
            	else if(pT.equals(WarAgentType.WarRocketLauncher))
            		this.broadcastMessageToAgentType(WarAgentType.WarLight, "Help", String.valueOf(p.getDistance()), String.valueOf(p.getAngle()));
            	
            	if(this.ennemiPosition[2] == -1.0) {
            		this.ennemiPosition[0] = p.getDistance();
            		this.ennemiPosition[1] = p.getAngle();
            		this.ennemiPosition[2] = (double)p.getID();
            		this.setHeading(p.getAngle());
            	}
            	
            	// Si on peut attaquer alors
            	// if(this.isReloaded()) return WarTurret.ACTION_FIRE;
            	// else return WarTurret.ACTION_RELOAD;
            }
        }
        
        
        return WarTurret.ACTION_IDLE;
    }
}
