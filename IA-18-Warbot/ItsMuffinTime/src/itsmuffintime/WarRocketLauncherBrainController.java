package itsmuffintime;

import java.util.List;

import edu.warbot.agents.enums.WarAgentType;
import edu.warbot.brains.WarBrain;
import edu.warbot.brains.brains.WarRocketLauncherBrain;
import edu.warbot.communications.WarMessage;

public abstract class WarRocketLauncherBrainController extends WarRocketLauncherBrain {

	protected double DistSelfToBase;
	protected double AngleSelfToBase;
	protected double CoordX;
	protected double CoordY;
	protected double DistSelfToEnemy;
	protected double AngleSelfToEnemy;
	protected boolean GoToEnemy;
	protected WarTask ctask;
	
	
    public WarRocketLauncherBrainController() {
        super();
        this.DistSelfToBase = -1;
        this.AngleSelfToBase = -1;
        this.CoordX = -1;
        this.CoordY = -1;
        this.DistSelfToEnemy = -1;
        this.AngleSelfToEnemy = -1;
        this.GoToEnemy = false;
        this.ctask = AwaitingDecision;
    }
    
    static WarTask AwaitingDecision = new WarTask() {
    	String exec(WarBrain b) {
    		WarRocketLauncherBrainController self = (WarRocketLauncherBrainController) b;
//    		self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.WHERE_ARE_BASES, "");
//    		List<WarMessage> messages = self.getMessages();
//    		for(WarMessage message : messages) {
//    			if(message.getMessage().equals("I'm here") && message.getSenderType().equals(WarAgentType.WarBase)) {
//    				String[] contents = message.getContent();
//    				double dist = Calculs.DistObjectToSelf(Double.parseDouble(contents[0]), Double.parseDouble(contents[1]), message.getDistance(), message.getAngle());
//    				if(dist < WarTurret.DISTANCE_OF_VIEW) {
//    					self.setRandomHeading();
//    					return ACTION_MOVE;
//    				}
//    			}
//    		}	
    		self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.WHERE_ARE_ENEMIES, "");
    		List<WarMessage> mes = self.getMessages();
    		for(WarMessage message : mes) {
    			if(message.getMessage().equals(WarUtil.GO_TO_ENEMY) && message.getSenderType().equals(WarAgentType.WarBase)) {
    				self.ctask = AttackMode;
    			}
    		}
    		
    		return ACTION_IDLE;
    	}
    };
    
    static WarTask AttackMode = new WarTask() {
    	String exec(WarBrain b) {
    		WarRocketLauncherBrainController self = (WarRocketLauncherBrainController) b;
    		self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.WHERE_ARE_ENEMIES, "");
    		List<WarMessage> messages = self.getMessages();
    		for(WarMessage message : messages) {
    			if(message.getMessage().equals(WarUtil.GO_TO_ENEMY) && message.getSenderType().equals(WarAgentType.WarBase)) {
    				String[] contents = message.getContent();
    				self.AngleSelfToEnemy = Calculs.AngleObjectToSelf(Double.parseDouble(contents[0]), Double.parseDouble(contents[1]), message.getDistance(), message.getAngle());
    				self.DistSelfToEnemy = Calculs.DistObjectToSelf(Double.parseDouble(contents[0]), Double.parseDouble(contents[1]), message.getDistance(), message.getAngle());
    				self.setHeading(self.AngleSelfToEnemy);
    				if(self.DistSelfToEnemy <= 150) {
    					self.ctask = KillNine;
    				}
    				
    			}
    		}
    		
    		if(self.isBlocked()) self.setRandomHeading();
    		return ACTION_MOVE;
    	}
    };
    
    static WarTask KillNine = new WarTask() {
    	String exec(WarBrain b) {
    		WarRocketLauncherBrainController self = (WarRocketLauncherBrainController) b;
    		self.setTargetDistance(self.DistSelfToEnemy);
    		if(self.isReloaded()) {
    			return ACTION_FIRE;
    		}else {
    			return ACTION_RELOAD;
    		}
    		
    	}
    };
    
    public void getSelfCoord() {
    	this.broadcastMessageToAgentType(WarAgentType.WarBase,"Where is the base", "");
    	for(WarMessage message : this.getMessages()) {
    		if(message.getSenderType().equals(WarAgentType.WarBase)) {
    			this.DistSelfToBase = message.getDistance();
    			this.AngleSelfToBase = Math.toRadians((message.getAngle() + 180)%360);
    			this.CoordX = this.DistSelfToBase * Math.cos(AngleSelfToBase);
    			this.CoordY = this.DistSelfToBase * Math.sin(AngleSelfToBase);
    		}
    	}
    }
    
    @Override
    public String action() {
    	String state = ctask.exec(this);
    	if(state.equals(null)) {
    		if(this.isBlocked())
    			this.setRandomHeading();
    		return ACTION_MOVE;
    	}
    	return state;
    }

//    @Override
//    public String action() {
//    	this.getSelfCoord(); // Coordonnees actuelles
//        for (WarAgentPercept wp : getPerceptsEnemies()) {
//            if (wp.getType().equals(WarAgentType.WarBase) && this.isEnemy(wp)) {
//                setHeading(wp.getAngle());
//                this.setTargetDistance(wp.getDistance());
//                if (isReloaded())
//                    return ACTION_FIRE;
//                else if (isReloading())
//                    return ACTION_IDLE;
//                else
//                    return ACTION_RELOAD;
//            }
//        }
//        
//        for (WarMessage message : getMessages()) {
//            if (message.getSenderType().equals(WarAgentType.WarBase)){
//            	if(message.getMessage().equals("I'm under attack") && message.getDistance() <= this.MAX_DISTANCE) {
//            		this.setHeading(message.getAngle());
//            	}
//            	else if(message.getMessage().equals("Enemy Base")) {
//            		String[] content = message.getContent();
//            		
//            		
//            		double DistBaseToEnemy = Double.parseDouble(content[0]);
//            		double AngleBaseToEnemy = Double.parseDouble(content[1]);
//
//            		if(message.getDistance() >= (1/3)*DistBaseToEnemy) {
//            			this.DistSelfToEnemy = Calculs.DistObjectToSelf(DistBaseToEnemy, AngleBaseToEnemy, message.getDistance(), message.getAngle());
//                		this.AngleSelfToEnemy = Calculs.AngleObjectToSelf(Double.parseDouble(content[0]), Double.parseDouble(content[1]), message.getDistance(), message.getAngle());
//            			this.GoToEnemy = true;
//            		}
//            			
//            	}
//                
//            }
//        }
//        
//        if(this.GoToEnemy) {
//        	this.setDebugString("Go to Enemy Base ");
//        	this.setHeading(this.AngleSelfToEnemy);
//        }
//        
//        
//
//        if (isBlocked())
//            setRandomHeading();
//
//        return ACTION_MOVE;
//    }

}