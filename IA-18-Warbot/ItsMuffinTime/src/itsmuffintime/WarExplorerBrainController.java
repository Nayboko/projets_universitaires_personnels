package itsmuffintime;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import edu.warbot.agents.agents.WarExplorer;
import edu.warbot.agents.enums.WarAgentCategory;
import edu.warbot.agents.enums.WarAgentType;
import edu.warbot.agents.percepts.WarAgentPercept;
import edu.warbot.agents.resources.WarFood;
import edu.warbot.brains.WarBrain;
import edu.warbot.brains.brains.WarExplorerBrain;
import edu.warbot.communications.WarMessage;
import java.util.concurrent.ThreadLocalRandom;

public abstract class WarExplorerBrainController extends WarExplorerBrain {
	
	protected WarTask ctask;
	protected List<WarAgentPercept> myBases;
	public int tick;

    public WarExplorerBrainController() {
        super();
        this.ctask = LookingForFood;
        this.myBases = new ArrayList<WarAgentPercept>();
        this.tick = 0;
    }
    
    static WarTask LookingForFood = new WarTask(){
    	String exec(WarBrain b) {
    		WarExplorerBrainController self = (WarExplorerBrainController)b;
    		self.setDebugString("LookingForFood");
    		if(self.getNumberOfAgentsInRole("OverWatch", "Explorers") == 0) {
    			self.requestRole("OverWatch", "Explorers");
    			self.ctask = OverWatch;
    			return ACTION_IDLE;
    		}
    		
    		self.myBases = self.getPerceptsAlliesByType(WarAgentType.WarBase);
    		// Si le sac est plein, il faut retourner � la base
    		if(self.isBagFull()) {
    			self.ctask = GoHome;
    			return ACTION_IDLE;
    		}


    		/*if(!self.myBases.isEmpty() && self.myBases != null) {
				self.broadcastMessageToAgentType(WarAgentType.WarBase,WarUtil.WHERE_ARE_MUFFINS, "");
				// Y aller
				}
    		*/
    		
    		// Recherche de la nourriture
    		List<WarAgentPercept> percepts = self.getPercepts();
    		for(WarAgentPercept p: percepts) {
    			WarAgentType pType = p.getType();
    			if(pType.equals(WarAgentType.WarFood)) {
    				if(!self.isBagFull()) {
    					if(p.getDistance() <= WarFood.MAX_DISTANCE_TAKE) {
    						// self.setDebugStringColor(Color.green);
    						self.setDebugString(WarUtil.FOUND_MUFFINS);
							self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.FOUND_MUFFINS, String.valueOf(p.getDistance()), String.valueOf(p.getAngle()));
							return WarExplorer.ACTION_TAKE;
    					}else self.setHeading(p.getAngle());
    				}
    			}
    		}
    		
    		if(self.isBlocked())
    			self.setRandomHeading();
    		self.setHeading(self.getEsquiveAngle());
			return WarExplorer.ACTION_MOVE;
    	}
    };
    
    static WarTask GoHome = new WarTask(){
    	String exec(WarBrain b) {
    		WarExplorerBrainController self = (WarExplorerBrainController)b;
    		    		
    		// Le sac est vide, il faut retourner en patrouille
    		if(self.isBagEmpty()) {
    			self.ctask = LookingForFood;
    			self.setHeading((self.getHeading() + 180) % 360);
    			return ACTION_IDLE;
    		}
    		self.setDebugString("GoHome Angle: " + self.getHeading());
    		if(self.isBlocked())
    			self.setRandomHeading();
    		// Si il n'est tombe sur aucune base alliee
    		self.myBases = self.getPerceptsAlliesByType(WarAgentType.WarBase);
    		if(self.myBases.size() == 0 || self.myBases.equals(null)) {
    			self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.WHERE_ARE_BASES, "");
				for (WarMessage message : self.getMessages()) {
					if(message != null) {
						self.setHeading(message.getAngle());
					}
			          
		        }
				return ACTION_MOVE;
    		}
    		
    		List<WarAgentPercept> percepts = self.getPercepts();       	
        	// Recherche des percpetions g�n�rales
    		for(WarAgentPercept p : percepts) {
    			WarAgentType pType = p.getType();
	    		if(pType.equals(WarAgentType.WarBase) && !self.isEnemy(p)) {
					if(self.getNbElementsInBag() != 0) {
						if(p.getDistance() <= WarFood.MAX_DISTANCE_TAKE) {
							self.setIdNextAgentToGive(p.getID()); // Fix� sur la WarBase
							self.setDebugString("Livraison !");
							return WarExplorer.ACTION_GIVE;
						}
					}
				}
    		}    		
    		self.setHeading(self.getEsquiveAngle());
			return WarExplorer.ACTION_MOVE;
    	}
    };
    
    static WarTask OverWatch = new WarTask(){
    	String exec(WarBrain b) {
    		WarExplorerBrainController self = (WarExplorerBrainController)b;
    		self.setDebugStringColor(Color.orange);
    		self.setDebugString("OverWatch");
    		
    		
    		
    		return ACTION_MOVE;
    	}
    };
    
    
    public boolean lookingForEnemyBase(WarExplorerBrainController self) {
    	List<WarAgentPercept> percepts = self.getPerceptsEnemies();
    	for(WarAgentPercept e : percepts) {
			if(e.getType().equals(WarAgentType.WarBase) && self.isEnemy(e)) {
				self.setDebugStringColor(Color.magenta);
				self.setDebugString(WarUtil.ENEMY_FUNDED);;
				self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.ENEMY_FUNDED, String.valueOf(e.getDistance()), String.valueOf(e.getAngle()));
				return true;
			}
		}
		return false;
    }
    
    public void pingBase() {
    	this.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.STILL_ALIVE, "");
    }
    
    public double getEsquiveAngle() {
    	double AngleToGo = this.getHeading();
    	List<WarAgentPercept> percepts = this.getPercepts();
    	
    	for(WarAgentPercept wp : percepts) {
    		if(wp.getType().getCategory().equals(WarAgentCategory.Projectile) || ((wp.getType().equals(WarAgentType.WarHeavy) || wp.getType().equals(WarAgentType.WarLight) || wp.getType().equals(WarAgentType.WarRocketLauncher) || wp.getType().equals(WarAgentType.Wall)) && this.isEnemy(wp) )) {    			
    			int randomDir = ThreadLocalRandom.current().nextInt(0, 2);
    			double randomAngle = ThreadLocalRandom.current().nextDouble(0, 45);
    			if(randomDir == 0) {
    				AngleToGo += randomAngle;
    			}else {
    				AngleToGo -= randomAngle;
    			}
    		}
    		if(wp.getType().equals(WarAgentType.WarBase) && this.isEnemy(wp)) {
    			AngleToGo += 180;
    			AngleToGo = AngleToGo % 360;
    		}
    		if(wp.getType().equals(WarAgentType.WarTurret) && this.isEnemy(wp)) {
    			this.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.TURRET_FUNDED, String.valueOf(wp.getDistance()), String.valueOf(wp.getAngle()));
    			int randomDir = ThreadLocalRandom.current().nextInt(0, 2);
    			double randomAngle = ThreadLocalRandom.current().nextDouble(0, 45);
    			if(randomDir == 0) {
    				AngleToGo += randomAngle;
    			}else {
    				AngleToGo -= randomAngle;
    			}
    		}
    	}
		return AngleToGo;
    }
    
    
    
    @Override
    public String action() {
    	this.tick += 1;
    	// REFLEXES
    	lookingForEnemyBase(this);
    	pingBase();
    	// STATE
    	String state = ctask.exec(this);
    	if(state.equals(null)) {
    		if(this.isBlocked())
    			this.setRandomHeading();
    		return WarExplorer.ACTION_MOVE;
    	}
    	return state;
    }

//    @Override
//    public String action() {
//    	
//    	List<WarAgentPercept> percepts = this.getPercepts();
//    	
//    	// Recherche des percpetions g�n�rales
//		for(WarAgentPercept p : percepts) {
//			WarAgentType pType = p.getType();
//			// L'Explorateur tombe sur de la nourriture
//			if(pType.equals(WarAgentType.WarFood)) {
//				if(!isBagFull()) {	
//					if(p.getDistance() <= WarFood.MAX_DISTANCE_TAKE) {
//						this.broadcastMessageToAgentType(WarAgentType.WarExplorer, "Nourriture", "Find Food");
//						this.setDebugString("Oh! Des provisions !");
//						return WarExplorer.ACTION_TAKE;
//					}else this.setHeading(p.getAngle());
//				}else if(isBagFull()) {
//					this.broadcastMessageToAll("Where is the base", "Where is the base");
//					for (WarMessage message : getMessages()) {
//			            if (message.getMessage().equals("I'm here")) {
//			                this.setHeading(message.getAngle());
//			            	this.setDebugString("Je rentre � la base");
//			            }
//			        }
//					
//				}
//			}
//			// L'Explorateur tombe sur une base amie
//			if(pType.equals(WarAgentType.WarBase) && !this.isEnemy(p)) {
//				if(this.getNbElementsInBag() != 0) {
//					if(p.getDistance() <= WarFood.MAX_DISTANCE_TAKE) {
//						this.setIdNextAgentToGive(p.getID()); // Fix� sur la WarBase
//						this.setDebugString("Livraison !");
//						return WarExplorer.ACTION_GIVE;
//					}
//				}
//			}
//    	}
//		
//		// Recherche des perceptions ennemies
//		for(WarAgentPercept e : this.getPerceptsEnemies()) {
//			if(e.getType().equals(WarAgentType.WarBase) && this.isEnemy(e)) {
//				this.setDebugString("Enemy base detected");
//				//this.broadcastMessageToAgentType(WarAgentType.WarRocketLauncher, "", content)
//				this.broadcastMessageToAgentType(WarAgentType.WarBase, "Where is the base", "");
//				this.broadcastMessageToAgentType(WarAgentType.WarBase, "Enemy base detected", String.valueOf(e.getDistance()), String.valueOf(e.getAngle()));
//			}
//		}
//		
//		// Recherche de la base amie pour livrer nourriture
//		if(isBagFull()) {
//			this.broadcastMessageToAll(WarUtil.WHERE_ARE_BASES, "Where is the base");
//			for (WarMessage message : getMessages()) {
//	            if (message.getMessage().equals("I'm here")) {
//	                this.setHeading(message.getAngle());
//	            	this.setDebugString("Je rentre � la base");
//	            }
//	        }
//		}
//		
//		this.setHeading(this.getEsquiveAngle(percepts));
//		this.setDebugString("Angle actuel : " + this.getHeading());
//		
//        if (isBlocked())
//            setRandomHeading();
//        return WarExplorer.ACTION_MOVE;
//    }
    
    

}
