package itsmuffintime;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import edu.warbot.agents.agents.WarExplorer;
import edu.warbot.agents.agents.WarTurret;
import edu.warbot.agents.enums.WarAgentCategory;
import edu.warbot.agents.enums.WarAgentType;
import edu.warbot.agents.percepts.WarAgentPercept;
import edu.warbot.agents.resources.WarFood;
import edu.warbot.brains.WarBrain;
import edu.warbot.brains.brains.WarEngineerBrain;
import edu.warbot.communications.WarMessage;

public abstract class WarEngineerBrainController extends WarEngineerBrain {

	protected WarTask ctask;
	
    public WarEngineerBrainController() {
        super();
        this.ctask = AwaitingDecision;
    }
    
    static WarTask AwaitingDecision = new WarTask() {
		@Override
		String exec(WarBrain b) {
			WarEngineerBrainController self = (WarEngineerBrainController) b;
			self.setDebugString("AwaitingDecision");
			for(WarMessage message : self.getMessages()) {
				if(message.getMessage().equals(WarUtil.BUILD_TURRET) && self.getHealth() >= 0.85 * self.getMaxHealth()) {
					self.ctask = BuildTurret;
				}
			}			
			if(self.isBlocked()) self.setRandomHeading();
			return ACTION_MOVE;
		}
    };
    
    static WarTask GoHome = new WarTask() {
		@Override
		String exec(WarBrain b) {
			WarEngineerBrainController self = (WarEngineerBrainController) b;
	    	self.setDebugString("GoHome");
			self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.WHERE_ARE_BASES, "");
			for(WarMessage message : self.getMessages()) {
				if(message.getMessage().equals("I'm here")) {
					self.setHeading(message.getAngle());
				}
			}
			for(WarAgentPercept p : self.getPercepts()) {
				if(p.getType().equals(WarAgentType.WarBase) && !self.isEnemy(p)) {
					if(p.getDistance() < 4) {
						self.ctask = AwaitingDecision;
						return ACTION_IDLE;
					}
					if(p.getType().equals(WarAgentType.WarFood) && !self.isBagFull()) {
						self.setHeading(p.getAngle());
						if(p.getDistance() <= WarFood.MAX_DISTANCE_TAKE) {
							return ACTION_TAKE;
						}else {
							return ACTION_MOVE;
						}
					}
				}
			}
			
			return ACTION_MOVE;
		}
    };
    
    static WarTask BuildTurret = new WarTask() {
		@Override
		String exec(WarBrain b) {
			WarEngineerBrainController self = (WarEngineerBrainController) b;
			self.setDebugString("BuildTurret");
			self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.WHERE_ARE_MUFFINS, "");
			for(WarMessage message : self.getMessages()) {
				if(message.getMessage().equals(WarUtil.MUFFIN_ZONE) && self.getHealth() >= 0.85 * self.getMaxHealth()) {
					String[] contents = message.getContent();
    				double angle = Calculs.AngleObjectToSelf(Double.parseDouble(contents[0]), Double.parseDouble(contents[1]), message.getDistance(), message.getAngle());
    				double dest = Calculs.DistObjectToSelf(Double.parseDouble(contents[0]), Double.parseDouble(contents[1]), message.getDistance(), message.getAngle());
    				self.setHeading(angle);
    				if(dest <= WarTurret.MAX_DISTANCE_GIVE) {
    					self.setNextBuildingToBuild(WarAgentType.WarTurret);
    					self.ctask = GoHome;
    					return ACTION_BUILD;
    				}
				}
			}
			if(self.isBlocked()) self.setRandomHeading();
			return ACTION_MOVE;
		}
    };
    
    static WarTask LookingForFood = new WarTask(){
    	String exec(WarBrain b) {
    		WarEngineerBrainController self = (WarEngineerBrainController)b;
    		self.setDebugString("LookingForFood");
    		// Si le sac est plein, il faut retourner � la base
    		if(self.isBagFull()) {
    			self.ctask = GoHome;
    			return ACTION_IDLE;
    		}
    		
    		// Recherche de la nourriture
    		List<WarAgentPercept> percepts = self.getPercepts();
    		for(WarAgentPercept p: percepts) {
    			WarAgentType pType = p.getType();
    			if(pType.equals(WarAgentType.WarFood)) {
    				if(!self.isBagFull()) {
    					if(p.getDistance() <= WarFood.MAX_DISTANCE_TAKE) {
    						self.setDebugString(WarUtil.FOUND_MUFFINS);
							self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.FOUND_MUFFINS, String.valueOf(p.getDistance()), String.valueOf(p.getAngle()));
							return ACTION_TAKE;
    					}else self.setHeading(p.getAngle());
    				}
    			}
    		}
    		
    		if(self.isBlocked())
    			self.setRandomHeading();
    		self.setHeading(self.getEsquiveAngle());
			return WarExplorer.ACTION_MOVE;
    	}
    };
    
    public boolean HealMyself() {
    	if(this.getNbElementsInBag()> 0 && this.getHealth() < 0.85 * this.getMaxHealth()) {
    		return true;
    	}
    	return false;
    }

    @Override
    public String action() {
       	// REFLEXES
    	if(this.HealMyself()) return ACTION_EAT;
    	// STATE
    	String state = ctask.exec(this);
    	if(state.equals(null)) {
    		if(this.isBlocked())
    			this.setRandomHeading();
    		return WarExplorer.ACTION_MOVE;
    	}
    	return state;
    }
    
    public double getEsquiveAngle() {
    	double AngleToGo = this.getHeading();
    	List<WarAgentPercept> percepts = this.getPercepts();
    	
    	for(WarAgentPercept wp : percepts) {
    		if(wp.getType().getCategory().equals(WarAgentCategory.Projectile) || ((wp.getType().equals(WarAgentType.WarHeavy) || wp.getType().equals(WarAgentType.WarLight) || wp.getType().equals(WarAgentType.WarRocketLauncher)) && this.isEnemy(wp) )) {    			
    			int randomDir = ThreadLocalRandom.current().nextInt(0, 2);
    			double randomAngle = ThreadLocalRandom.current().nextDouble(0, 45);
    			if(randomDir == 0) {
    				AngleToGo += randomAngle;
    			}else {
    				AngleToGo -= randomAngle;
    			}
    		}
    		if(wp.getType().equals(WarAgentType.WarBase) && this.isEnemy(wp)) {
    			AngleToGo += 180;
    			AngleToGo = AngleToGo % 360;
    		}
    		if(wp.getType().equals(WarAgentType.WarTurret) && this.isEnemy(wp)) {
    			this.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.TURRET_FUNDED, String.valueOf(wp.getDistance()), String.valueOf(wp.getAngle()));
    			int randomDir = ThreadLocalRandom.current().nextInt(0, 2);
    			double randomAngle = ThreadLocalRandom.current().nextDouble(0, 45);
    			if(randomDir == 0) {
    				AngleToGo += randomAngle;
    			}else {
    				AngleToGo -= randomAngle;
    			}
    		}
    	}
		return AngleToGo;
    }
}
