package itsmuffintime;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import edu.warbot.agents.enums.WarAgentCategory;
import edu.warbot.agents.enums.WarAgentType;
import edu.warbot.agents.percepts.WarAgentPercept;
import edu.warbot.brains.WarBrain;
import edu.warbot.brains.brains.WarHeavyBrain;
import edu.warbot.communications.WarMessage;

public abstract class WarHeavyBrainController extends  WarHeavyBrain {

	protected WarTask ctask;

    public WarHeavyBrainController() {
        super();
        this.ctask = AwaitingDecision;
    }
    
    static WarTask AwaitingDecision = new WarTask() {
    	String exec(WarBrain b) {
    		WarHeavyBrainController self = (WarHeavyBrainController) b;
    		self.setDebugString("En attente d'instructions");
    		self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.WHERE_ARE_ENEMIES, "");
    		List<WarMessage> messages = self.getMessages();
    		for(WarMessage message : messages) {
    			if(message.getMessage().equals(WarUtil.DEFEND_ME_PLEASE)) {
    				if(message.getDistance() <= 200) {
    					String[] content = message.getContent();
    					double angle = Calculs.AngleObjectToSelf(Double.parseDouble(content[0]), Double.parseDouble(content[1]), message.getDistance(), message.getAngle());
    					self.setHeading(angle);
    					return ACTION_MOVE;
    				}
    			}
    			if(message.getMessage().equals(WarUtil.GO_TO_ENEMY) && message.getSenderType().equals(WarAgentType.WarBase)) {
    				self.ctask = AttackMode;
    				return ACTION_MOVE;
    			}
    		}
    		for (WarAgentPercept wp : self.getPerceptsEnemies()) {
    			if(wp.getType().equals(WarAgentType.WarBase))
    				self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.ENEMY_FUNDED, String.valueOf(wp.getDistance()), String.valueOf(wp.getAngle()));
	
	            if ((wp.getType().equals(WarAgentType.WarBase) || 
	            		wp.getType().equals(WarAgentType.WarRocketLauncher) || 
	            		wp.getType().equals(WarAgentType.WarHeavy)) && !wp.getType().equals(WarAgentType.WarFood)) {
	                self.setHeading(Calculs.CalculAnticipation(wp, WarAgentType.WarHeavy));
	
	                self.setDebugString("Attaque!");
	                self.ctask = KillNine;
	            }
	        }
    		if(self.isBlocked())
    			self.setRandomHeading();
    		self.setHeading(self.getEsquiveAngle());
			return ACTION_MOVE;
    	}
    };
    
    static WarTask AttackMode = new WarTask() {
    	String exec(WarBrain b) {
    		WarHeavyBrainController self = (WarHeavyBrainController) b;
    		self.setDebugString("IT'S MUFFIN TIME!");
    		self.broadcastMessageToAgentType(WarAgentType.WarBase, WarUtil.WHERE_ARE_ENEMIES, "");
    		if(self.isBlocked())
    			self.setRandomHeading();
    		List<WarMessage> messages = self.getMessages();
    		for(WarMessage message : messages) {
    			if(message.getMessage().equals(WarUtil.GO_TO_ENEMY)) {
    				String[] content = message.getContent();
					double angle = Calculs.AngleObjectToSelf(Double.parseDouble(content[0]), Double.parseDouble(content[1]), message.getDistance(), message.getAngle());
					self.setHeading(angle);
    			}
    		}
    		List<WarAgentPercept> percepts = self.getPercepts();
    		for(WarAgentPercept p : percepts) {
    			WarAgentType pType = p.getType();
    			if(self.isEnemy(p) && !pType.equals(WarAgentType.WarFood)) {
    				if(pType.equals(WarAgentType.WarExplorer) || pType.equals(WarAgentType.WarLight) || pType.equals(WarAgentType.WarHeavy)) {
    					self.setHeading(Calculs.CalculAnticipation(p, WarAgentType.WarHeavy));
        				if(self.isReloaded())
        					return ACTION_FIRE;
        				return ACTION_RELOAD;
        			}
    				if(pType.equals(WarAgentType.WarTurret)) {
    					self.setHeading(p.getAngle());
        				if(self.isReloaded())
        					return ACTION_FIRE;
        				return ACTION_RELOAD;
    				}
        			if(pType.equals(WarAgentType.WarBase)) {
        				self.ctask = KillNine;
        				return ACTION_FIRE;
        			}
    			}
    		}
    		if(self.isBlocked()) self.setRandomHeading();
    		return ACTION_MOVE;
    	}
    };
    
    static WarTask KillNine = new WarTask() {
    	String exec(WarBrain b) {
    		WarHeavyBrainController self = (WarHeavyBrainController) b;
    		self.setDebugString("Taste my muffins!");
    		List<WarAgentPercept> percepts = self.getPercepts();
    		for(WarAgentPercept p : percepts) {
    			if(self.isEnemy(p) && !p.getType().equals(WarAgentType.WarFood)) {
    				self.setHeading(p.getAngle());
    				if(self.isReloaded()) return ACTION_FIRE;
    				return ACTION_RELOAD;
    			}
    		}
    		// On ne perçoit plus d'ennemis
    		self.ctask = AwaitingDecision;
    		return ACTION_MOVE;
    	}
    };
    	
    public String action() {
    	// REFLEXES
    	// this.killEnemy();
    	// STATE
    	String state = ctask.exec(this);
    	if(state.equals(null)) {
    		if(this.isBlocked())
    			this.setRandomHeading();
    		return ACTION_MOVE;
    	}
    	return state;
    }

//    @Override
//    public String action() {
//
//        for (WarAgentPercept wp : getPerceptsEnemies()) {
//
//            if ((wp.getType().equals(WarAgentType.WarBase) || 
//            		wp.getType().equals(WarAgentType.WarRocketLauncher) || 
//            		wp.getType().equals(WarAgentType.WarHeavy)) && !wp.getType().equals(WarAgentType.WarFood)) {
//
//                setHeading(wp.getAngle());
//
//                this.setDebugString("Attaque!");
//                if (isReloaded())
//                    return ACTION_FIRE;
//                else if (isReloading())
//                    return ACTION_IDLE;
//                else
//                    return ACTION_RELOAD;
//            }
//        }
//        
//        for (WarMessage message : getMessages()) {
//            if (message.getSenderType().equals(WarAgentType.WarBase)){
//            	if(message.getMessage().equals("I'm under attack")) {
//            		this.setHeading(message.getAngle());
//            	}
//            }
//        }
//
//        if (isBlocked())
//            setRandomHeading();
//
//        return ACTION_MOVE;
//    }
    
    public void killEnemy() {
    	for(WarAgentPercept e : this.getPerceptsEnemies()) {
    		if(!e.getType().equals(WarAgentType.WarFood) && !this.ctask.equals(AttackMode)) {
    			this.setHeading(Calculs.CalculAnticipation(e, WarAgentType.WarHeavy));
    			this.ctask = KillNine;
    		}
    	}
    }
    
    public double getEsquiveAngle() {
    	double AngleToGo = this.getHeading();
    	List<WarAgentPercept> percepts = this.getPercepts();
    	
    	for(WarAgentPercept wp : percepts) {
    		if(wp.getType().getCategory().equals(WarAgentCategory.Projectile) || wp.getType().equals(WarAgentType.WarRocketLauncher) && this.isEnemy(wp) ) {    			
    			int randomDir = ThreadLocalRandom.current().nextInt(0, 2);
    			double randomAngle = ThreadLocalRandom.current().nextDouble(0, 45);
    			if(randomDir == 0) {
    				AngleToGo += randomAngle;
    			}else {
    				AngleToGo -= randomAngle;
    			}
    		}
    	}
		return AngleToGo;
    }
}
