package itsmuffintime;


import edu.warbot.agents.agents.*;
import edu.warbot.agents.enums.WarAgentCategory;
import edu.warbot.agents.enums.WarAgentType;
import edu.warbot.agents.percepts.WarAgentPercept;
import edu.warbot.brains.WarBrain;
import edu.warbot.brains.brains.WarBaseBrain;
import edu.warbot.communications.WarMessage;

import java.util.ArrayList;
import java.util.List;

public abstract class WarBaseBrainController extends WarBaseBrain {

	protected WarTask ctask;
	protected int tick;
	public ArrayList<WarAgentType> AgentStack;
	protected boolean _alreadyCreated;
	protected boolean _gotSentinelle;
	protected Double[] PosMuffinZone= {-1.0,-1.0};
	protected ArrayList<Double[]> PosTours;
	protected ArrayList<Double[]> PosBases;
	protected ArrayList<Double[]> PosMuffins;
	protected boolean _inDanger;
    

    public WarBaseBrainController() {
        super();
        this.ctask = Management;
        this.tick = 0;
        this.AgentStack = new ArrayList<WarAgentType>();
        this._alreadyCreated = false;
        this._gotSentinelle = false;
        this.PosTours = new ArrayList<Double[]>();
        this.PosBases = new ArrayList<Double[]>();
        this.PosMuffins = new ArrayList<Double[]>();
        this._inDanger = false;
        AddWarAgent(this,WarAgentType.WarExplorer,2);
		AddWarAgent(this,WarAgentType.WarHeavy,2);
		AddWarAgent(this,WarAgentType.WarLight,3);
    }
    
    static WarTask Management = new WarTask(){
		String exec(WarBrain b){
			WarBaseBrainController self = (WarBaseBrainController) b;
			self.setDebugString("PosMuffinsZone : " + self.PosMuffinZone[0] + " : " + self.PosMuffinZone[1]);
			self.HandleMessages();
			while(self.AgentStack.size() != 0 && self.getNbElementsInBag() >= 0 && self.getHealth() >= 0.3 * self.getMaxHealth()){
				self.setNextAgentToCreate(self.AgentStack.get(0));
				self.AgentStack.remove(0);
				return WarBase.ACTION_CREATE;
			}
			GetEngineer(self);
			PrepareDefense(self);
			PrepareAttack(self);
			
			if(self._alreadyCreated && self.PosMuffins.size() > 3 && (self.tick % 200 == 0 && self.tick != 0)) {
				self.broadcastMessageToAgentType(WarAgentType.WarEngineer, WarUtil.BUILD_TURRET, String.valueOf(self.PosMuffinZone[0]), String.valueOf(self.PosMuffinZone[1]));
			}
			if(self.getNbElementsInBag() >= 0 && self.getHealth() < 0.8 * self.getMaxHealth())
				return ACTION_EAT;
			return ACTION_IDLE;
		}
	};
	
	static void AddWarAgent(WarBaseBrainController self, WarAgentType wat, int n) {
		for(int i=0; i < n; i++)
			self.AgentStack.add(wat);
	};
    
    @Override
    public String action(){
    	this.tick += 1;
    	// REFLEXES
    	DefendMe();
    	// STATE
    	String state = ctask.exec(this);

    	if(state.equals(null)) {
    		return ACTION_IDLE;
    	}
    	return state;
    }
    
    protected void HandleMessages() {
    	List<WarMessage> messages = this.getMessages();
    	Integer explorers = 0;
    	// Integer warLight = 0;
    	for(WarMessage message : messages) {
    		if (message.getMessage().equals(WarUtil.WHERE_ARE_BASES))
              reply(message, "I'm here");
    		if(message.getMessage().equals(WarUtil.STILL_ALIVE) && message.getSenderType().equals(WarAgentType.WarExplorer)) {
    			explorers += 1;
    		}
    		if(message.getMessage().equals(WarUtil.WHERE_ARE_ENEMIES) && !this.PosBases.isEmpty()) {
    			double distBase = this.PosBases.get(0)[0];
    			double angleBase = this.PosBases.get(0)[1];
    			reply(message, WarUtil.GO_TO_ENEMY, Double.toString(distBase), Double.toString(angleBase));
    		}
    		if(message.getMessage().equals(WarUtil.ENEMY_FUNDED)){
            	String[] contents = message.getContent();
            	Double[] base = {Calculs.DistObjectToSelf(Double.parseDouble(contents[0]), Double.parseDouble(contents[1]), message.getDistance(), message.getAngle()), 
    					Calculs.AngleObjectToSelf(Double.parseDouble(contents[0]), Double.parseDouble(contents[1]), message.getDistance(), message.getAngle())};
            	// this.PosBases.add(base);
            	this.addBase(base);
    		}
    		if(message.getMessage().equals(WarUtil.WHERE_ARE_MUFFINS)) {
    			if(this.PosMuffinZone[0] != -1.0) {
    				reply(message, WarUtil.MUFFIN_ZONE, Double.toString(this.PosMuffinZone[0]), Double.toString(this.PosMuffinZone[1]));
    			}
    		}
    		if(message.getMessage().equals(WarUtil.FOUND_MUFFINS)) {
    			String[] contents = message.getContent();
    			Double[] muffin = {
    					Calculs.DistObjectToSelf(Double.parseDouble(contents[0]), Double.parseDouble(contents[1]), message.getDistance(), message.getAngle()), 
    					Calculs.AngleObjectToSelf(Double.parseDouble(contents[0]), Double.parseDouble(contents[1]), message.getDistance(), message.getAngle())			};
    			this.PosMuffins.add(muffin);
    			//this.setDebugString("Nombre de muffins trouvés : " + this.PosMuffins.size());
    			
    			this.PosMuffinZone[0] = 0.0; this.PosMuffinZone[1] = 0.0;
    			
    			// Mise à jour de la zone de nourriture
    			for(int i = 0; i < this.PosMuffins.size(); i++) {
    				this.PosMuffinZone[0] += this.PosMuffins.get(i)[0];
    				this.PosMuffinZone[1] += this.PosMuffins.get(i)[1];
    			}
    			this.PosMuffinZone[0] /= this.PosMuffins.size();
				this.PosMuffinZone[1] /= this.PosMuffins.size();
    		}
    		if(message.getMessage().equals(WarUtil.TURRET_FUNDED)){
    			String[] contents = message.getContent();
				Double[] tour = {Calculs.DistObjectToSelf(Double.parseDouble(contents[0]), Double.parseDouble(contents[1]), message.getDistance(), message.getAngle()), 
    					Calculs.AngleObjectToSelf(Double.parseDouble(contents[0]), Double.parseDouble(contents[1]), message.getDistance(), message.getAngle())};
				this.PosTours.add(tour);
			}		
    		
    	}
    	if(explorers < 5) {
    		AddWarAgent(this, WarAgentType.WarExplorer, 1);
    	}
    			
    }
    
    public void addBase(Double[] base) {
    	if(!this.PosBases.contains(base)) {
    		this.PosBases.add(base);
    	}
    }
    
    /*
     * @brief Ajoute un type d'agent a creer dans la pile de construction
     * @param wat Le type d'agent a creer
     * @param self Controleur courant
     * @param n Nombre d'agents a creer
     */
    static void AddWarAgent(WarAgentType wat, WarBaseBrainController self, int n) {
    	for(int i = 0; i < n; i++) {
    		self.AgentStack.add(0, wat);
    	}
    }  
    
    public boolean EnoughMuffinsForEngineer() {
    	return (this.PosMuffins.size() > 3);
    }
    
    static boolean HealMyself(WarBaseBrainController self) {
    	if(!self.isBagEmpty() && (self.getHealth() <= 0.8 * self.getMaxHealth())) {
    		return true;
    	}
    	return false;
    }
    
    static void GetEngineer(WarBaseBrainController self) {
    	if(!self._alreadyCreated && self.PosMuffins.size() > 3){
			self._alreadyCreated = true;
			AddWarAgent(self,WarAgentType.WarEngineer,1);
		}
    }
    
    static void PrepareDefense(WarBaseBrainController self) {
    	if(self.tick != 0 && (self.tick % 500 == 0)) {
    		AddWarAgent(WarAgentType.WarLight, self, 1);
    		AddWarAgent(WarAgentType.WarHeavy, self, 2);
    	}
    }
    
    static void PrepareAttack(WarBaseBrainController self) {
    	if(self.tick != 0 && (self.tick % 250 == 0)) {
			AddWarAgent(WarAgentType.WarHeavy,self, 2);
			AddWarAgent(WarAgentType.WarRocketLauncher, self, 1);
		}
    }
    
    /*
     * @brief Place le prochain agent a creer comme etant le premier de la file AgentStack
     */
    public void CreateWarAgent(WarBaseBrainController self) {
    	self.setNextAgentToCreate(self.AgentStack.remove(0));
    }
    
    public void DefendMe() {
    	for (WarAgentPercept percept : getPerceptsEnemies()) {
          if (isEnemy(percept) && percept.getType().getCategory().equals(WarAgentCategory.Soldier))
              broadcastMessageToAll(WarUtil.DEFEND_ME_PLEASE,
                      String.valueOf(percept.getAngle()),
                      String.valueOf(percept.getDistance()));
      }
    }
    
    public boolean CanCreate() {
    	return (!this.isBagEmpty() && this.getHealth() >= (0.3 * this.getMaxHealth()));
    }


//	  @Override
//    public String action() {
//
//        if (!_alreadyCreated) {
//            setNextAgentToCreate(WarAgentType.WarEngineer);
//            _alreadyCreated = true;
//            return WarBase.ACTION_CREATE;
//        }
//
//        if (getNbElementsInBag() >= 0 && getHealth() <= 0.8 * getMaxHealth())
//            return WarBase.ACTION_EAT;
//
//        if (getMaxHealth() == getHealth()) {
//            _alreadyCreated = true;
//        }
//
//        List<WarMessage> messages = getMessages();
//
//        for (WarMessage message : messages) {
//            if (message.getMessage().equals(WarUtil.WHERE_ARE_BASES))
//                reply(message, "I'm here");
//            if(message.getMessage().equals(WarUtil.ENEMY_FUNDED)) {
//            	
//            	String[] content = message.getContent();
//            	
//            	double DistEnemyBase = Calculs.DistObjectToSelf(Double.parseDouble(content[0]), Double.parseDouble(content[1]), message.getDistance(), message.getAngle());
//            	double AngleEnemyBase = Calculs.AngleObjectToSelf(Double.parseDouble(content[0]), Double.parseDouble(content[1]), message.getDistance(), message.getAngle());
//            	this.setDebugString("DistEnemyBase : " + DistEnemyBase + " AngleEnemyBase : " + AngleEnemyBase);
//            	this.broadcastMessageToAgentType(WarAgentType.WarRocketLauncher, "Enemy Base", String.valueOf(DistEnemyBase), String.valueOf(AngleEnemyBase));
//            	this.broadcastMessageToAgentType(WarAgentType.WarKamikaze, "Fire in the hole!", String.valueOf(DistEnemyBase), String.valueOf(AngleEnemyBase));
//            }
//        }
//
//        for (WarAgentPercept percept : getPerceptsEnemies()) {
//            if (isEnemy(percept) && percept.getType().getCategory().equals(WarAgentCategory.Soldier))
//                broadcastMessageToAll("I'm under attack",
//                        String.valueOf(percept.getAngle()),
//                        String.valueOf(percept.getDistance()));
//        }
//
//        for (WarAgentPercept percept : getPerceptsResources()) {
//            if (percept.getType().equals(WarAgentType.WarFood))
//                broadcastMessageToAgentType(WarAgentType.WarExplorer, "I detected food",
//                        String.valueOf(percept.getAngle()),
//                        String.valueOf(percept.getDistance()));
//        }
//
//        return WarBase.ACTION_IDLE;
//    }
    
    /*
     * @brief Retourne le prix de construction d'une unite
     * @param wat Type de l'unite dont on desire connaitre le prix
     * @return prix de construction d'une unite
     */
    public int getUnitCost(WarAgentType wat) {
    	switch(wat) {
    	case WarExplorer:
    		return WarExplorer.COST;
    	case WarLight:
    		return WarLight.COST;
    	case WarHeavy:
    		return WarHeavy.COST;
    	case WarKamikaze:
    		return WarKamikaze.COST;
    	case WarTurret:
    		return WarTurret.COST;
    	case WarRocketLauncher:
    		return WarRocketLauncher.COST;
    	case WarEngineer:
    		return WarEngineer.COST;
    	default:
    		break;
    	}
    	return 0;
    }

}
