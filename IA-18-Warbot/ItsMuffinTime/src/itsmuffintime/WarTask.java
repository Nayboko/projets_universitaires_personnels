package itsmuffintime;

import edu.warbot.brains.WarBrain;

public abstract class WarTask {
	WarBrain brain;
	
	// Execution d'une WarTask
	abstract String exec(WarBrain b);
}
