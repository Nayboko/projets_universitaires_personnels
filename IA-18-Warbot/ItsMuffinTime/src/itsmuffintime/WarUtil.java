package itsmuffintime;

public class WarUtil {
	public static String WHERE_ARE_MUFFINS = "Where are the muffins?";
	public static String FOUND_MUFFINS = "I found some muffins!";
	public static String MUFFIN_ZONE = "Somewhere here!";
	public static String WHERE_ARE_BASES = "Where are my bases?";
	public static String WHERE_ARE_ENEMIES = "Where are enemy bases?";
	public static String BACK_HOME = "Delivery of muffins!";
	public static String ENEMY_FUNDED = "Oh oh! They're here!";
	public static String GO_TO_ENEMY = "Go give them some Muffins!";
	public static String TURRET_FUNDED = "Turret funded";
	public static String BUILD_TURRET = "Go build a turret!";
	public static String STILL_ALIVE = "Hey! I'm a Muffin !";
	public static String DEFEND_ME_PLEASE = "I've some problems here!";
	public static String GO_THERE = "Go there";
}
