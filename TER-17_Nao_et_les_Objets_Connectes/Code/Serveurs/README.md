# Liste des fichiers utiles en fonction de la situation du composant dans l'architecture :

* Le dossier "BD" contient les Bases de Données à mettre sur le Serveur Central avec le dossier "historique_ordres"
* Le dossier "Serveur_doc" est une ébauche d'une documentation générée automatiquement avec l'outil Epydoc
* Le fichier "Iclient.py" permet de simuler l'émission de messages envoyés par Nao au Serveur Central.
* "InterpreteurBD.py", "Domotarduino.py" et "Serveur_Central.py" dans le *Serveur Central*
* "Serveur.py" défini la super-classe _Serveur_ des serveurs Serveur_Central et _ServeurRPI_. Il doit être présent sur chaque composant faisant tourner un serveur.
* "ServeurRPI.py" et "InterpreteurRPI.py" dans le composant qui va faire office de *Serveur IoT*
* Les images PNG "fenetre_ferme", "fenetre_ouvert", "porte_ferme", "porte_ouvert", "volet_ferme" et "volet_ouvert" doivent être sur le Serveur IoT chargé de la simulation de ceux-ci.
* "run_servRPI.sh" contient un script de démarrage pour le *Serveur IoT*.