#!/usr/bin/python
# coding=utf-8

import sys

#exemple argv : arduino2 chambre AouE
longueur = len(sys.argv)


# prend en paramètre un string correspondant à la ligne dont nous voulons l'ip/port et le renvoie
def ip_port(s):
	if s[0] == sys.argv[1] :
		return s[1]+" "+s[2]+ " "

# prend en paramètre un string correspondant à la ligne dont nous voulons le code d'exécution et le renvoie
def obj_co(s):
	if s[0] == sys.argv[2] :
		if sys.argv[3] == 'A' :
			return s[1]
		elif sys.argv[3] == 'E' :
			return s[2]
		elif sys.argv[3] == 'I' :
			return s[3]	

# prend en paramètre un string correspondant au nom du fichier de la BD et renvoie un string = <IP> <Port> <Code d'exécution>
def lire_bd(s):
	try:
		fd = open(s)
		lignes = fd.read()
		fd.close()
		#print "traitement des lignes"
		mots = []
		for mot in lignes.split('\n'):
			mots.append(mot)
		#après avoir séparé les lignes de la BD
		#on sépare les mots de chaque ligne que l'on redirige en fonction du contenu de s
		for i in range (len(mots)):
			line = mots[i]
			stg = line.split(',')
			if s == "./BD/ip_port_obj_co.txt":
				res = ip_port(stg)
				if res != None:
					return res
			else:
				res = obj_co(stg)
				if res != None:
					return res
	except IOError as e:
		print "I/O error({0}): {1}".format(e.errno, e.strerror)
		raise

if (longueur != 4) :
	print "Pas assez d'arguments"
	sys.exit()

else :
	try :
		res = ""
		#récupération ip/port correspondant à l'ordre à envoyer
		f_nom = "./BD/ip_port_obj_co.txt"
		res = lire_bd(f_nom)
		#print res
		#traitement des argv de l'ordre pour obtenir le code à envoyer
		f_nom = "./BD/"+sys.argv[1]+".txt"
		res += str(lire_bd(f_nom))
		sys.stdout.write(res) # Pour retirer le \n
		
	except:
		print "Unexpected error:", str(sys.exc_info()[0]), "N'est pas dans la BD"
		raise

#sys.exit(0)

