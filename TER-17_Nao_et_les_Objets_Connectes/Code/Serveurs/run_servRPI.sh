#!/bin/sh

# le nom du service
SERVICE_NAME=ServeurRPI
# le répertoire où se trouvent les exécutables du service
SERVICE_DIRECTORY=~/Serveur
# le nom du script de démarrage du service
SERVICE_STARTUP_SCRIPT=ServeurRPI.py
# le nom du script d'arrêt du service
SERVICE_SHUTDOWN_SCRIPT=client.py
 
usage()
{
        echo "-----------------------"
        echo "Usage: $0 (stop|start|restart)"
        echo "-----------------------"
}
 
if [ -z $1 ]; then
        usage
fi
 
service_start()
{
        echo "Démarrage du service '${SERVICE_NAME}'..."
        OWD=`pwd`
        cd ${SERVICE_DIRECTORY} &amp;&amp; ./${SERVICE_STARTUP_SCRIPT}
        cd $OWD
        echo "Le service '${SERVICE_NAME}' a démarré correctement"
}
 
service_stop()
{
        echo "Arrêt du service '${SERVICE_NAME}'..."
        OWD=`pwd`
        cd ${SERVICE_DIRECTORY} &amp;&amp; ./${SERVICE_SHUTDOWN_SCRIPT} 1 stop
        cd $OWD
        echo "Service '${SERVICE_NAME}' arrêté"
}
 
case $1 in
        stop)
                service_stop
        ;;
        start)
                service_start
        ;;
        restart)
                service_stop
                service_start
        ;;
        *)
                usage
esac
exit 0