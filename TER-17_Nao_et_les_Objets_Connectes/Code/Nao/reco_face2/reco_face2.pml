<?xml version="1.0" encoding="UTF-8" ?>
<Package name="reco_face2" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="Reco_face" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="unlearn_face" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="delete_all_faces" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="ExampleDialog" src="Reco_face/ExampleDialog/ExampleDialog.dlg" />
        <Dialog name="learn_reco" src="Reco_face/learn_reco/learn_reco.dlg" />
        <Dialog name="reset_learn" src="unlearn_face/reset_learn/reset_learn.dlg" />
        <Dialog name="effacer" src="effacer/effacer.dlg" />
    </Dialogs>
    <Resources />
    <Topics>
        <Topic name="ExampleDialog_enu" src="Reco_face/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
        <Topic name="learn_reco_frf" src="Reco_face/learn_reco/learn_reco_frf.top" topicName="learn_reco" language="fr_FR" />
        <Topic name="reset_learn_frf" src="unlearn_face/reset_learn/reset_learn_frf.top" topicName="reset_learn" language="fr_FR" />
        <Topic name="effacer_frf" src="effacer/effacer_frf.top" topicName="effacer" language="fr_FR" />
    </Topics>
    <IgnoredPaths />
</Package>
