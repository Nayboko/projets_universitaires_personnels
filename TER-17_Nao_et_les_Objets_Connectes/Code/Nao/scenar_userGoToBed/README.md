# Comportement scénario Coucher

## Comportement général

Clair et explicite via le diagramme et le dialogue. L'utilisateur signifie qu'il est fatigué ou qu'il va dormir, Nao va agir en conséquence sur le réseau domotique de la maison.


## TODO

* Il faut encore faire la jointure avec le serveur central en utilisant les intepréteurs propres à chaque objet connecté ;
* Modifier le comportement serveur central en conséquence
* Ajouter plus d'interactivité entre Nao et l'utilisateur
** Completer la base des concepts de Dialogue
