<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Reco_Learn_Face" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="Dial Learn" src="Dial Learn/Dial Learn.dlg" />
        <Dialog name="Dial Reco" src="Dial Reco/Dial Reco.dlg" />
    </Dialogs>
    <Resources />
    <Topics>
        <Topic name="Dial Learn_frf" src="Dial Learn/Dial Learn_frf.top" topicName="" language="C" />
        <Topic name="Dial Reco_frf" src="Dial Reco/Dial Reco_frf.top" topicName="" language="" />
    </Topics>
    <IgnoredPaths />
</Package>
