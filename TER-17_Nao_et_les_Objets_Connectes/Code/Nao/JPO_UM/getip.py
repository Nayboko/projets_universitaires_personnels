#!/usr/bin/python
import json

class IPlist():
    table = None
    def __init__(self, f="ip.json"):
        self.content = ""
        with open(f, 'r') as content_file:
            self.content += content_file.read()
        IPlist.table = json.loads(self.content)
        
if __name__ == '__main__':
    ips = IPlist()
    print("table complete :\t" + str(IPlist.table))
    print("IP NAO :\t" + IPlist.table['nao']['ip'])
