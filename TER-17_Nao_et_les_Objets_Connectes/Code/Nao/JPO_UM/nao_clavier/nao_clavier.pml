<?xml version="1.0" encoding="UTF-8" ?>
<Package name="nao_clavier" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="serveur_nao" src="serveur_nao.py" />
        <File name="serveurthread_0_1" src="serveurthread_0_1.py" />
        <File name="__init__" src="__init__.py" />
        <File name="client" src="client.py" />
        <File name="client_0_1" src="client_0_1.py" />
    </Resources>
    <Topics />
    <IgnoredPaths />
</Package>
