<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Nao_presentation_JPO" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="JPO_diagram" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="touchhead" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="Ask_Dance" src="Ask_Dance/Ask_Dance.dlg" />
        <Dialog name="JPO" src="JPO/JPO.dlg" />
        <Dialog name="ExampleDialog" src="JPO_diagram/ExampleDialog/ExampleDialog.dlg" />
        <Dialog name="Blagues" src="Blagues/Blagues.dlg" />
    </Dialogs>
    <Resources>
        <File name="" src=".metadata" />
        <File name="Game of Thrones - Générique, Saison 1" src="Game of Thrones - Générique, Saison 1.mp3" />
        <File name="swiftswords_ext" src="JPO_diagram/swiftswords_ext.mp3" />
        <File name="got" src="got.mp3" />
    </Resources>
    <Topics>
        <Topic name="Ask_Dance_frf" src="Ask_Dance/Ask_Dance_frf.top" topicName="Ask_Dance" language="fr_FR" />
        <Topic name="JPO_frf" src="JPO/JPO_frf.top" topicName="JPO" language="fr_FR" />
        <Topic name="ExampleDialog_enu" src="JPO_diagram/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
        <Topic name="Blagues_frf" src="Blagues/Blagues_frf.top" topicName="Blagues" language="fr_FR" />
    </Topics>
    <IgnoredPaths />
</Package>
