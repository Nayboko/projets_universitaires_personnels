#!/usr/bin/env python
# -*- coding: utf-8 -*-

#	Note : 	Ne fonctionne pas, block sur la loop 2 du run()
#

import os
import datetime
import time
import threading
from clock import Clock

class Alarme(threading.Thread):
	def __init__(self, annee, mois, jour, heure, minute, event):
		super(Alarme, self).__init__()
		self.time = [int(annee), int(mois), int(jour), int(heure), int(minute)]
		self.event = str(event)
		self.active = True
		self.update_interval = 10
		self.clock = Clock()

	def run(self):
		try:
			while self.active:
				self.clock.start()
				same = True
				current = self.clock.current()
				self.clock.join(timeout=0)

				print("Heure courante : ")
				print("Date  : " + str(current[2])+"/"+str(current[1])+ "/" + str(current[0]))
				print("Heure : " +str(current[3])+ " heure et " + str(current[4]) + " minutes")

				for i in range(5):
					if current[i] != self.time[i]:
						same = False

				if same:
					# TODO: Executer le réveil sur Nao
					print("Wake up ! Vous avez prévu l'évenement suivant : ")
					print(self.event)
					music = os.popen('vlc ./Reveil_en_douceur.mp3', 'w')
					time.sleep(60)
					music.close()
					self.active = False
				else:
					time.sleep(self.update_interval)
			return
		except:
			return

	def alarm_kill(self):
		self.active = False
		self.clock.quit()

if __name__ == '__main__':

	print("Alarme Programée : Y/M/D H/Mn E")
	alarm = []
	for i in range(6):
		In = raw_input()
		alarm.append(In)

	alarm = Alarme(alarm[0], alarm[1], alarm[2], alarm[3], alarm[4], alarm[5])
	alarm.start()

	try:
		while True:
			text = str(raw_input())
			if text == "stop":
				alarm.alarm_kill()
				break
	except:
		print("Erreur")
		alarm.alarm_kill()	



