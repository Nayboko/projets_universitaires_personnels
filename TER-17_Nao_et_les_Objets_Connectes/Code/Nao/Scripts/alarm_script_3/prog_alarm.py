#!/usr/bin/env python
# -*- coding: utf-8 -*-

from alarm import Alarme
import sys


alarm = Alarme(alarm_A, alarm_M, alarm_J, alarm_H, alarm_Mn, alarm_E)
alarm.start()

try:
	while True:
		text = str(raw_input())
		if text == "stop":
			alarm.alarm_kill()
			break
except:
	print("Erreur")
	alarm.alarm_kill()		