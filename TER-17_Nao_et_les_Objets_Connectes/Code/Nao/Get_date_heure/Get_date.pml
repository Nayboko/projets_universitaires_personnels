<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Get_date" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="Test" src="Test/Test.dlg" />
    </Dialogs>
    <Resources />
    <Topics>
        <Topic name="Test_frf" src="Test/Test_frf.top" topicName="Test" language="fr_FR" />
    </Topics>
    <IgnoredPaths />
</Package>
