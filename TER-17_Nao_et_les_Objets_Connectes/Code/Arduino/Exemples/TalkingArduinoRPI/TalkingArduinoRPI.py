#!/usr/bin/env python2
#-*- coding: utf-8 -*-

# Module de lecture du port série
# Port série ttyACM0
# Réception de messages depuis le Arduino

import serial

ser = serial.Serial('/dev/ttyACM0', 9600)

while 1:
	print ser.readline()