const int LED = 9;
const int BUTTON = 7;

int val = 0;
int old_val = 0;
int state = 0;
int lumi = 128; // Niveau de luminosité
unsigned long startTime = 0; // Temps en miiseconde où l'on presse le bouton

void setup() {
  pinMode(LED,OUTPUT);
  pinMode(BUTTON, INPUT);

}

void loop() {

  val = digitalRead(BUTTON);

  // Si on presse le bouton
  if((val == HIGH) && (old_val == LOW)){
    state = 1 - state;
    startTime = millis(); // retourne le nombre de miliseconde 
                          // depuis last reset
    delay(10);
  }

  // Si on continue de presser le bouton
  if((val == HIGH) && (old_val == HIGH)){
    if(state == 1 && (millis() - startTime) > 500){ // Vérifie si le bouton est pressé plus de 500 ms
      lumi++; // Augmente la luminosité
      delay(10);

      // Si lumi est au max, on la remet à 0
      if(lumi > 255){
        lumi = 0;
      }
    }
  }

  old_val = val;

  if(state == 1){
    analogWrite(LED, lumi); // Si on est en état allumé, on applique la lumi
  }else{
    analogWrite(LED, 0); // Sinon on éteint la LED
  }
}
