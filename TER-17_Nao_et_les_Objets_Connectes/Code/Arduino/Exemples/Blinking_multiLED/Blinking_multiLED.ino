// Blinking de plusieurs LED

const int LED_jaune = 2;
const int LED_rouge = 3;
const int LED_bleue = 4;
const int LED_verte = 5;

void setup() {
  // Initialisation des digital pins LED_x comme des output
  pinMode(LED_jaune, OUTPUT);
  pinMode(LED_rouge, OUTPUT);
  pinMode(LED_bleue, OUTPUT);
  pinMode(LED_verte, OUTPUT);
}

void loop(){
  // Alume et éteint les LEDs successivement
  // avec un délais de 1 seconde
  digitalWrite(LED_jaune, HIGH);
  delay(1000);
  digitalWrite(LED_jaune, LOW);
  digitalWrite(LED_rouge, HIGH);
  delay(1000);
  digitalWrite(LED_rouge, LOW);
  digitalWrite(LED_bleue, HIGH);
  delay(1000);
  digitalWrite(LED_bleue, LOW);
  digitalWrite(LED_verte, HIGH);
  delay(1000);
  digitalWrite(LED_verte, LOW);
  delay(1000);  
}

