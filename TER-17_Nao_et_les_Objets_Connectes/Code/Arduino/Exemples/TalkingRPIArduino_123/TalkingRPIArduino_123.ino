/*
 * 3 Leds sont reliées à l'Arduino
 * Les leds s'allument en fonction du message reçu
 */
 
// Déclaration des variables globales

const int LED1 = 2;
const int LED2 = 3;
const int LED3 = 4;

int msg = 0;

void setup() {
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  if(Serial.available()){
    msg = Serial.read() - '0'; // Soustraction de 0 qui vaut 48 en ASCII

    digitalWrite(LED1, LOW);
    digitalWrite(LED2, LOW);
    digitalWrite(LED3, LOW);

    switch(msg){
      case 1:
        digitalWrite(LED1, HIGH);
        break;
      case 2:
        digitalWrite(LED2, HIGH);
        break;
      case 3:
        digitalWrite(LED3, HIGH);
        break;
      case 4:
        digitalWrite(LED1, LOW);
        break;
      case 5:
        digitalWrite(LED2, LOW);
        break;
      case 6:
        digitalWrite(LED3, LOW);
        break;
    }
  }

}
