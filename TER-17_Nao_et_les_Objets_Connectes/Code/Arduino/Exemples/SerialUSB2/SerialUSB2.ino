/*
 * Arduino reçoit un nombre via le Serial
 * La LED clignote le nombre de fois que reçu par le Serial
 */


const int LED = 13;

void setup() {
  pinMode(LED, OUTPUT);
  Serial.begin(9600);

}

void loop() {

  Serial.println("Hi, it's me Arduino");
  
  while(Serial.available()){
    int r = atoi(Serial.read()); // Lecture du serial
    twinkle(r);
  }
  delay(1000);

}

/*
 * @name: twinkle
 * @param n: nombre de fois que la LED doit clignoter
 * @result Fait clignoter la LED n fois
 */
void twinkle(int n){
  for(int i = 0; i < n; i++){
    digitalWrite(LED, HIGH);
    delay(1000);
    digitalWrite(LED, LOW);
    delay(1000);
  }
}

