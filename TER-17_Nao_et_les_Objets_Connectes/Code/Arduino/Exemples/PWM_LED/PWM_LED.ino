/*  Utilisation de la technique PWM
    On va voir les variations de luminosité d'une LED
    La sortie Analog peut prendre une valeur entre 0 et 254*/


const int LED = 9;  // Le pin pour la LED
int i = 0;          // Compteur
void setup() {
  pinMode(LED, OUTPUT); // LED est un Output
}

void loop() {
  /* On boucle de 0 à 254 pour augmenter la luminosité */
  for(i = 0; i < 255; i++){
    analogWrite(LED, i);  // On définit la luminosité de la LED
    delay(10); // Pour avoir le temps de voir le changement (POV)
  }

  // Puis on la diminue progressivement
  for(i = 255; i > 0; i--){
    analogWrite(LED, i);
    delay(10);
  }

}
