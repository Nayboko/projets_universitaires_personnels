
/* Globales variables */
const int LedPin = 13;

void setup() {
  pinMode(LedPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  if(Serial.available()){
    light(Serial.read()-'0');
  }
  delay(500);
}

void light(int n){
  for(int i = 0; i <n; i++){
    digitalWrite(LedPin, HIGH);
    delay(500);
    digitalWrite(LedPin, LOW);
    delay(500);
  }
}

