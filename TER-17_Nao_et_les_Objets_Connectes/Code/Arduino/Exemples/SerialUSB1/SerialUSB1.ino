/*
 * Arduino envoie un message toutes les deux secondes
 * depuis le Serial au port 9600
 */

int cpt = 0;

void setup() {
  Serial.begin(9600);

}

void loop() {
  Serial.print("Message numero ");
  Serial.println(cpt);
  Serial.println("Ceci est un message de test !");
  cpt++;
  delay(2000);
}
