/*
 * Des Leds sont reliées à l'Arduino
 * Les leds s'allument en fonction du message reçu
 * Exemple: 21 -> Pin 2 à l'état 1
 */
 
// Déclaration des variables globales
#define N  13 // Nombre de pins

int state[N] = {0}; // 0 si Led éteinte, 1 sinon
int msg = 0;
int tmp[2] = {-1};

// Variables de vérification
int exec1 = 0;
int exec2 = 0;


// Initialisation des actuators LEDs
void setup() {
   for(int i = 2; i <= N; i++){
      pinMode(i, OUTPUT);
   }
  Serial.begin(9600);
}

// ##### MAIN #####
void loop() {
  if(Serial.available()){
    // Réception du message au port Serial
    while((msg = Serial.read()) >= 0){
      Serial.print("Contenu du message initial:");
      Serial.println(msg - 48);
      delay(500);
      msg -= 48; // Conversion ASCII -> DEC
      if((msg >= 2) && (msg <= 13)){
        tmp[0] = msg;
        exec1 = 10;
      }
      else if(msg == 0 || msg == 1){
        tmp[1] = msg;
        exec2 = 1;
      }
      else{
        exec1 = 0;
        exec2 = 0;
      }
    }

    //Serial.print("tmp[0] = "); Serial.println(tmp[0]);
    //Serial.print("tmp[1] = "); Serial.println(tmp[1]);

    
    
    // On change le statut de la Led désirée
    if((tmp[0] >= 2) && (tmp[0] <= 13) && (tmp[1] == 0 || tmp[1] == 1) && ((exec1 + exec2) == 11)){
      state[tmp[0]] = tmp[1];
    }

    // On allume ou éteint les LEDS en fonction de leur état
    for(int i = 2; i < N; i++){
      digitalWrite( i, state[i]);
    }

    delay(100);
    // On retourne sur le Serial la confirmation
    confirmation(tmp, exec1 + exec2);

  }
}

// Fonction à changer en fonction de l'ajout de LEDs
void confirmation(int d[2], int verif){
  /* Si erreur dans la réception */
  if(verif != 11){
    Serial.print("Arduino : Erreur dans la réception du code.");
    Serial.print("Message reçu : ");
    Serial.print(tmp[0]); Serial.println(tmp[1]);
    Serial.println("Arduino : Veuillez relancer l'ordre.");
  }

  /* En fonction de la Led, puis de l'état */
  if(verif == 11 && (d[1] == 0 || d[1] == 1)){
    Serial.print("Arduino : ");
    switch(d[0]){
      case 2:
        Serial.print("Le salon est");
        Serial.print(" ");
        if(d[1] == 0){
          Serial.println("eteint");
        } else if(d[1] == 1){
          Serial.println("allume");
        }
        break;
      case 3:
        Serial.print("La cuisine est");
        Serial.print(" ");
        if(d[1] == 0){
          Serial.println("eteinte");
        } else if(d[1] == 1){
          Serial.println("allumee");
        }
        break;
      case 4:
        Serial.print("La chambre est");
        Serial.print(" ");
        if(d[1] == 0){
          Serial.println("eteinte");
        } else if(d[1] == 1){
          Serial.println("allumee");
        }
        break;
    }
  }
  Serial.println("end");
}

