#!/usr/bin/env python2
#-*- coding: utf-8 -*-

# Module de lecture/écriture du port série
# Port série ttyACM0

import serial
import time

myPort = serial.Serial('/dev/ttyACM0', 9600)

while 1:
	n = raw_input("Entre un nombre [NumLed|Etat] : ")
	myPort.write(str(n))
	time.sleep(0.1)