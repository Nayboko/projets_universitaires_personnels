/*
 * Des Leds sont reliées à l'Arduino
 * Les leds s'allument en fonction du message reçu
 * Exemple: 21 -> Pin 2 à l'état 1
 */
 
// Déclaration des variables globales
#define N  13 // Nombre de pins
int state[N] = {0}; // 0 si Led éteinte, 1 sinon
int msg = 0;
int tmp[2] = {0};
void setup() {
   for(int i = 2; i <= N; i++){
      pinMode(i, OUTPUT);
   }
  Serial.begin(9600);
}
void loop() {
  if(Serial.available()){
    // Réception du message au port Serial
    while((msg = Serial.read()) >= 0){
      //Serial.print("Contenu du message initial:");
      //Serial.println(msg);
      delay(500);
      msg -= 48; // Conversion ASCII -> DEC
      if(msg >= 2){
        tmp[0] = msg;
      }
      else if(msg == 0 || msg == 1){
        tmp[1] = msg;
      }
    }  
    
    // On change le statut de la Led désirée
    state[tmp[0]] = tmp[1];
    // On allume ou éteint les LEDS en fonction de leur état
    for(int i = 2; i < N; i++){
      digitalWrite( i, state[i]);
    }
    delay(100);
    // On retourne sur le Serial le tableau d'état des LEDs
    Serial.println("ok");
    delay(10);
  }
}
