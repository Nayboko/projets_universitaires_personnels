# Feuille de route du Rapport de projet

## 1. Synopsis

Rapport final du projet de fin d'étude de **Licence 3 mention Informatique** de l'_Université de Montpellier_, en collaboration avec le _LIRMM_ et le _Département Informatique de la Faculté des Sciences_. Le travail est centré sur l'élaboration de scénarios d'assistance aux personnes âgées à l'aide du robot Nao de la société Aldebarran/SoftBank et d'autres objets connectés.

## 2. Plan
Voici le **plan général** pour le rapport de projet sur _Nao et les objets connectés_. Il prend en considération les différentes facettes de ce dernier, permettant une lecture fluide et cohérente, et autorise l'ajout de nouvelles sous-rubriques pour affiner le rapport.

---

### I/ Introduction
Description générale du déroulement du projet, du cadre dans lequel il est réalisé, les objectifs de l'unité d'enseignement et une brève description du plan. Cette partie contiendra également les remerciements.

### II/ Etat de l'art
Après avoir introduit l'objet principal du projet de recherche, on passe en revue ce qui a d'ores et déjà été réalisé dans le secteur public et privé. L'objectif est de dégager les idées principales de chaque projet, de peser le pour et le contre et de mettre en exergue ceux qui sont les plus notables. 

Puis, on introduit une idée générale, l'utilisation possible du même concept central avec des éléments nouveaux.

### III/ L'Internet des objets
Cette partie est consacrée à une revue des différentes technologies rencontrées durant le travail de recherche, principalement pendant la veille technologique et informationnelle.

### IV/ Notre solution
On va donner explicitement la définition client, c'est à dire la commande faite par le client concernant ce projet, puis les spécifications du livrable, où nous donnerons ce qui a été définit au début du projet, ce qui est donc attendu à la fin de celui-ci. 

Une partie importante est consacrée à l'élaboration du projet et à sa mise en oeuvre, à savoir une rapide description des différentes phases de travail, sa répartition entre les membres (éventuellement un diagramme de gantt), puis une explication plus approfondie de chaque élément constituant le projet.

Une sous-section sera également consacrée à la présentation d'un point technique et central : 
* Ce qui a été retenu pour son implémentation
* Comment nous l'avons implémenté et quelles techniques ont été utilisées
* Ce qui a posé problème
* Perspectives, ce qui pourrait être amélioré


### V/ Bug Report
Les possibilités d'amélioration, on répertorie les éventuels bugs dans les programmes, et les solutions qui nous semblent concevables.

### VI/ Conclusion
Si on devait refaire le projet, quelles sont les points que nous changerions dans son déroulement, les choses qui pourraient être améliorée, en somme, prendre du recul sur ce qui a été réalisé avant d'émettre des perspectives.

### Bibliogaphie / Webographie
Ensemble de la documentation qui a permit à chaque membre du groupe d'avancer dans son travail, d'améliorer, de consolider ou d'acquérir de nouvelles compétences. Le lecteur pourra, s'il en ressent l'envie, se documenter à son tour afin de mieux apprécier le contenu du présent rapport.

---

## 3. Changelog
* _19/04/17 :_ Mise en place du .tex et .bib. Rédaction du README.md avec explication des différentes parties du plan.

## 4. Contributeurs

* Elodie TRIBOUT
* Cédric PLUTA		    [nayboko.fr](http://www.nayboko.fr)
* Marin JULIEN
* Michael OHAYON-ORDAZ