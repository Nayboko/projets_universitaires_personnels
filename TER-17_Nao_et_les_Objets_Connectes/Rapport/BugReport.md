# BUG REPORT

Le script run_ServeurRPI.sh d'initialisation du programme ServeurRPI.py permettant de démarrer le service directement depuis le second niveau d'initialisation de l'OS Raspbian sur notre carte Raspberry Pi ne se lance pas. C'est une erreur connue de la communauté Debian qui rencontre de nombreuses difficultés pour l'execution de script au boot. 

La carte Arduino UNO R3 avec le programme Domotarduino2 ignore toujours le premier ordre en provenance du serveur central et n'allume pas l'actuator correspondant à l'ordre.