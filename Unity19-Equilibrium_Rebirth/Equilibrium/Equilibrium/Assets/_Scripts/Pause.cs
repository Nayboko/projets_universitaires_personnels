﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    [SerializeField] private GameObject pausePanel;
    void Start()
    {
        pausePanel.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!pausePanel.activeInHierarchy)
            {
                PauseGame();
                Debug.LogError("Pause");
            }
            else if (pausePanel.activeInHierarchy)
            {
                ContinueGame();
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    private void PauseGame()
    {
        Time.timeScale = 0;
        pausePanel.gameObject.SetActive(true);
    }
    private void ContinueGame()
    {
        Time.timeScale = 1;
        pausePanel.gameObject.SetActive(false);
    }

    public void ReturnMainMenu()
    {
        Resources.UnloadUnusedAssets();
        SceneManager.LoadScene("MenuGame");
    }
}