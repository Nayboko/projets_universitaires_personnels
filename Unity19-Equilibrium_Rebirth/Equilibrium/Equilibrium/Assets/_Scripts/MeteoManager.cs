﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeteoManager : MonoBehaviour
{
    [SerializeField]
    protected Sprite s_SunnyWeather;
    [SerializeField]
    protected Sprite s_ColdWeather;
    [SerializeField]
    protected Sprite s_WetWeather;
    [SerializeField]
    protected Sprite s_TemperedWeather;

    public string CurrentWeatherString;

    private Image _img;

    // Start is called before the first frame update
    void Start()
    {
        _img = this.gameObject.GetComponent<Image>();
        _img.sprite = s_SunnyWeather;
        CurrentWeatherString = "Sunny";

        StartCoroutine(WeatherManager(this));
    }


    private IEnumerator WeatherManager(MeteoManager _meteo)
    {
        while (true){
            yield return new WaitForSeconds(GameSettings.WeatherChangeLaps);
            int random = UnityEngine.Random.Range(0, 4);
            switch (random)
            {
                case 0:
                    _meteo._img.sprite = s_SunnyWeather;
                    CurrentWeatherString = "Sunny";
                    break;
                case 1:
                    _meteo._img.sprite = s_ColdWeather;
                    CurrentWeatherString = "Frezzy";
                    break;
                case 2:
                    _meteo._img.sprite = s_WetWeather;
                    CurrentWeatherString = "Wet";
                    break;
                case 3:
                    _meteo._img.sprite = s_TemperedWeather;
                    CurrentWeatherString = "Tempered";
                    break;

            }
        }
    }

}
