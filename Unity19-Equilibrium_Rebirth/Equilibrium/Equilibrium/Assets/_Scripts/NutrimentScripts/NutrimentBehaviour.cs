﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NutrimentBehaviour : MonoBehaviour
{
    public GameObject fx;
    public GameObject worldObject;

    void Start()
    {
        worldObject = GameObject.Find("World");
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Organism>() != null)
        {
            worldObject.SendMessage("AddEnergie");
            Instantiate(fx, transform.position, Quaternion.identity);
            Destroy(gameObject);
            GameVariables.CurrentNbNutriments -= 1;
        }
    }
}
