﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoadingScript : MonoBehaviour
{
    [SerializeField]
    private Image _progressBar;
    [SerializeField]
    private Image _illustration;
    private Text _funFact;

    public Sprite Bacillus;
    public Sprite Virus;

    private int random;

    // Start is called before the first frame update
    void Start()
    {
        _funFact = this.transform.Find("FunFacts").GetComponent<Text>();

        _funFact.text = RandomFunFact();


        random = Random.Range(0, 2);
        switch (random)
        {
            case 0:
                _illustration.GetComponent<Image>().sprite = Bacillus;
                break;
            case 1:
                _illustration.GetComponent<Image>().sprite = Virus;
                break;
        }

         

        // start async operation
        StartCoroutine(LoadAsyncOperation());
    }

    private IEnumerator LoadAsyncOperation()
    {
        yield return new WaitForSeconds(5.0f);
        // Create an async operation
        AsyncOperation level = SceneManager.LoadSceneAsync("MainGame");

        while (level.progress < 1)
        {
            _progressBar.fillAmount = level.progress;
            yield return new WaitForEndOfFrame();
        }

        // take the progress bar fill
        
    }

    private string RandomFunFact()
    {
        var dico = new Dictionary<int, string>{
            {0, "Ceci est une représentation 3D de la Bacillus Subtilis, une bactérie pouvant être trouvée dans le sol ou l'appareil digestif des ruminants ou des humains." },
            {1, "D'abord connue sous le nom Vibrio subtilis après avoir été découverte en 1835 par Christian G. Ehrenberg, elle fut renommée en 1872 par Ferdinand Cohn en Bacillus Subtilis." },
            {2, "Bacillus Subtilis : Bacillus fait référence à la forme de la bactérie (tige) et Subtilis signifie fin." },
            {3, "La Bacillus Subtilis détient le record de longévité dans l'espace : 6 ans sur un satellite de la NASA." },
            {4, "La Bacillus Subtilis est également utilisée dans des préparations pro-biotiques pour les problèmes intestinaux. Elle est aussi utilisée dans l'élaboration d'antibiotiques ou comme fongicide." },
            {5, "Le premier virus humain a avoir été découvert est celui de la fièvre jaune, le Flaviviridae Flavivirus, en 1901 par Walter Reed" },
            {6, "Il y a des millions de particules virales par millilitre d'eau de mer, pour un total de 10^30 virions. Les mettre bout à bout créerait une ligne de 200 millions d'année lumière dans l'espace." },
            {7, "Le génome du HIV-1 est long de 10 000 nucléotides, il y a donc 10^6020 combinaisons de séquences possible. Par comparaison, il y a 10^11 étoiles dans la Voie Lactée et 10^80 protons dans l'Univers observable." }
        };
        if (random == 0)
            return dico[UnityEngine.Random.Range(0, 5)];
        else if (random == 1)
            return dico[UnityEngine.Random.Range(5, dico.Count)];
        else return null;
    }   
}
