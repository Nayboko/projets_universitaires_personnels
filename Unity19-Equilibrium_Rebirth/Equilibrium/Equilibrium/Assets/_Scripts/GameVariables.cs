﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameVariables
{
    public static int ColonieEnergie = 0;

    public static int CurrentConsoEnergie = 0;

    public static int CurrentDNA = 0;

    public static int TailleColonie = 0;

    public static int CurrentNbNutriments = 0;

    public static float ColdResistance = 0f;

    public static float HeatResistance = 0f;

    public static float HumidityResistance = 0f;

    public static float AridityResistance = 0f;

    public static float VirusResistance = 0f;

    public static float BacteriaResistance = 0f;

    public static float ParasiteResistance = 0f;

    public static int NbVirus = 0;

    public static int NbParasites = 0;

    public static int NbBacteries = 0;

    public static List<Organism> ColonieMates = new List<Organism>();

    public static int GameScore = 0;

}
