﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class PlayerMouvment : MonoBehaviour
{
    NavMeshAgent agent;
    Mesh surface;
    

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        RaycastHit groundhit;

        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {   
                Debug.Log("Clicked on the UI");
            }
            else
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    agent.SetDestination(hit.point);
                }
            }
            

        }

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(horizontal, 0, vertical);

        gameObject.transform.Translate(direction.normalized * Time.deltaTime * GameSettings.OrganismMoveSpeed);


        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out groundhit, GameSettings.AggroRadius))
        {
            MeshCollider meshCollider = groundhit.collider as MeshCollider;
            
            if (meshCollider == null || meshCollider.sharedMesh == null)
                return;

            var tchunk = EndlessTerrain.terrainChunksVisibleLastUpdate.Find(x => x.meshObject.Equals(groundhit.collider.gameObject));

            MapGenerator mapGenerator = GameObject.Find("Map Generator").GetComponent<MapGenerator>();

            // var tchunk = EndlessTerrain.terrainChunksVisibleLastUpdate[meshCollider.];
            

            Mesh mesh = meshCollider.sharedMesh;
            Vector3[] vertices = mesh.vertices;
            int[] triangles = mesh.triangles;
            
            Vector3 p0 = vertices[triangles[groundhit.triangleIndex * 3 + 0]];
            Vector3 p1 = vertices[triangles[groundhit.triangleIndex * 3 + 1]];
            Vector3 p2 = vertices[triangles[groundhit.triangleIndex * 3 + 2]];


            Transform hitTransform = groundhit.collider.transform;
            p0 = hitTransform.TransformPoint(p0);
            p1 = hitTransform.TransformPoint(p1);
            p2 = hitTransform.TransformPoint(p2);

            Debug.DrawLine(p0, p1);
            Debug.DrawLine(p1, p2);
            Debug.DrawLine(p2, p0);

            Transform tChunk = tchunk.meshObject.transform;
            Vector3 localPos = tChunk.InverseTransformPoint(transform.position);


            var localCoord = (int)(localPos.z + (MapGenerator.mapChunkSize / 2)) * MapGenerator.mapChunkSize + ((int)localPos.x + (MapGenerator.mapChunkSize / 2));

            Debug.Log("(x,y) : (" + (localPos.x + (MapGenerator.mapChunkSize / 2)) + ", " + (localPos.z + (MapGenerator.mapChunkSize / 2)) + ") \n" +
                "localCoord : " + localCoord);

            var color = tchunk.mapData.colourMap[localCoord];


            if (color == mapGenerator.regions[0].colour)
            {
                Debug.Log("("+ localPos.x + " - " + localPos.z + ") : " + mapGenerator.regions[0].name);
            }else if (color == mapGenerator.regions[1].colour)
            {
                Debug.Log("(" + localPos.x + " - " + localPos.z + ") : " + mapGenerator.regions[1].name);
            }
            else if (color == mapGenerator.regions[2].colour)
            {
                Debug.Log("(" + localPos.x + " - " + localPos.z + ") : " + mapGenerator.regions[2].name);
            }
            else if (color == mapGenerator.regions[3].colour)
            {
                Debug.Log("(" + localPos.x + " - " + localPos.z + ") : " + mapGenerator.regions[3].name);
            }
            else if (color == mapGenerator.regions[4].colour)
            {
                Debug.Log("(" + localPos.x + " - " + localPos.z + ") : " + mapGenerator.regions[4].name);
            }
            else if (color == mapGenerator.regions[5].colour)
            {
                Debug.Log("(" + localPos.x + " - " + localPos.z + ") : " + mapGenerator.regions[5].name);
            }
            else if (color == mapGenerator.regions[6].colour)
            {
                Debug.Log("(" + localPos.x + " - " + localPos.z + ") : " + mapGenerator.regions[6].name);
            }
            else if (color == mapGenerator.regions[7].colour)
            {
                Debug.Log("(" + localPos.x + " - " + localPos.z + ") : " + mapGenerator.regions[7].name);
            }


            Debug.Log(color);
        }
    }
}
