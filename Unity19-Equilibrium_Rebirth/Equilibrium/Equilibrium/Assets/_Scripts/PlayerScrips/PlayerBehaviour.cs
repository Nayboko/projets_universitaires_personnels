﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour
{
    [SerializeField]
    private Image _progressBar_Heat;
    [SerializeField]
    private Image _progressBar_Cold;
    [SerializeField]
    private Image _progressBar_Wet;
    [SerializeField]
    private Image _progressBar_Tempered;

    public float HP;
    public float HeatResistance;
    public float ColdResistance;
    public float WetResistance;
    public float TemperedResistance;

    private float MaxResistanceToReach_Heat;
    private float MaxResistanceToReach_Cold;
    private float MaxResistanceToReach_Wet;
    private float MaxResistanceToReach_Tempered;

    // Start is called before the first frame update
    void Start()
    {
        HP = GameSettings.PlayerMaxHP;

        MaxResistanceToReach_Heat = UnityEngine.Random.Range(0f, 100f);
        MaxResistanceToReach_Cold = UnityEngine.Random.Range(0f, 100f);
        MaxResistanceToReach_Wet = UnityEngine.Random.Range(0f, 100f);
        MaxResistanceToReach_Tempered = UnityEngine.Random.Range(0f, 100f);

        HeatResistance = UnityEngine.Random.Range(0f, MaxResistanceToReach_Heat/3f);
        ColdResistance = UnityEngine.Random.Range(0f, MaxResistanceToReach_Cold / 3f);
        WetResistance = UnityEngine.Random.Range(0f, MaxResistanceToReach_Wet / 3f);
        TemperedResistance = UnityEngine.Random.Range(0f, MaxResistanceToReach_Tempered / 3f);
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(HealthManager(this));
        StartCoroutine(ResistancesBarStates(this));
    }

    private IEnumerator ResistancesBarStates(PlayerBehaviour player)
    { 

        while (true)
        {
            yield return new WaitForSeconds(1f);
            _progressBar_Heat.fillAmount = HeatResistance / 100f;
            _progressBar_Cold.fillAmount = ColdResistance / 100f;
            _progressBar_Wet.fillAmount = WetResistance / 100f;
            _progressBar_Tempered.fillAmount = TemperedResistance / 100f;

        }

        // take the progress bar fill

    }

    protected virtual IEnumerator HealthManager(PlayerBehaviour player)
    {
        while (true)
        {
            yield return new WaitForSeconds(GameSettings.OrganismTimeToCheckHealth);
            if(player.HP <= 0)
            {
                Destroy(player.gameObject);
                // SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
                Resources.UnloadUnusedAssets();
                SceneManager.LoadScene("MenuGame");
            }
        }
    }

    protected virtual IEnumerator ScoreManager(PlayerBehaviour player)
    {
        while (true)
        {
            yield return new WaitForSeconds(5f);
            GameVariables.GameScore += 1;
        }
    }
}
