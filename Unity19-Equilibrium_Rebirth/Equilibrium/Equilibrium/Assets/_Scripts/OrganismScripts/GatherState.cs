﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatherState : BaseState
{
    private float _gatherReadyTime = 1f;
    private Organism _organism;

    public GatherState(Organism organism) : base(organism.gameObject)
    {
        _organism = organism;
    }

    public override Type Tick()
    {
        if (_organism.Target == null)
        {
            return typeof(WanderState);
        }

        _gatherReadyTime -= Time.deltaTime;

        if(_gatherReadyTime <= 0f)
        {
            Debug.Log("Gather some nutriments");
            _organism.LetsGather();
        }

        return null;
    }

}
