﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class InstantiateOrganism : MonoBehaviour
{
    [SerializeField] 
    protected GameObject _prefabNormal;
    [SerializeField]
    protected GameObject _prefabAdvanced;
    [SerializeField]
    protected GameObject _prefabExpert;

    [SerializeField]
    protected GameObject _prefabVirus;
    [SerializeField]
    protected GameObject _prefabBacterie;



    [SerializeField]
    protected int ySpawn = 10;
    [SerializeField]
    protected int minX = -100;
    [SerializeField]
    protected int maxX = 100;
    [SerializeField]
    protected int minZ = -100;
    [SerializeField]
    protected int maxZ = 100;
    [SerializeField]
    protected int MaxVirus = 20;
    [SerializeField]
    public int MaxBacteries = 20;

    public float tVirus = 2f; // Laps de temps entre chaque spawn
    public float tBacteries = 3f;


    protected Organism[] _organismList;
    protected Organism _organism;

    private int _minX;
    private int _maxX;
    private int _minZ;
    private int _maxZ;


    void Start()
    {
        UpdateSpawn();
        StartCoroutine(VirusCreation());
        StartCoroutine(BacteriaCreation());
    }


    // Update is called once per frame
    void Update()
    {
        _organism = null;

        if((Input.GetKeyDown(KeyCode.N)) && GameVariables.TailleColonie == 0)
        {
            InvokeNormalMate();
        }

        // Duplication d'un mate "normal"
        if (Input.GetKeyDown(KeyCode.W))
        {
            DuplicateMate();
            
        }

        // Transformation d'un mate normal => advanced
        if (Input.GetKeyDown(KeyCode.X) )
        {
           
            EvolveMateAdvanced();
        }

        // Transformation d'un mate advanced => expert
        if (Input.GetKeyDown(KeyCode.C))
        {

            EvolveMateExpert();
        }

    }

    public void EvolveMateExpert()
    {
        if (GameVariables.ColonieMates.Count != 0 && GameVariables.CurrentDNA >= GameSettings.ExpertDNACost)
        {
            foreach (Organism o in GameVariables.ColonieMates)
            {
                if (o.Level == Level.Advanced)
                {
                    _organism = o;
                }
            }

            if (_organism == null)
                Debug.Log("Not enought DNA to create Expert Mate");
            else
            {
                Vector3 randomPos = new Vector3(_organism.transform.position.x, ySpawn, _organism.transform.position.z);

                GameObject newOrga = Instantiate(_prefabExpert, randomPos, Quaternion.identity);

                GameVariables.ColonieEnergie -= GameSettings.OrganismExpertCost;

                GameVariables.CurrentDNA -= GameSettings.ExpertDNACost;

                Destroy(_organism.gameObject);
            }
        }
    }

    public void EvolveMateAdvanced()
    {
        if (GameVariables.ColonieMates.Count != 0 && GameVariables.CurrentDNA >= GameSettings.AdvancedDNACost)
        {
            foreach (Organism o in GameVariables.ColonieMates)
            {
                if (o.Level == Level.Normal)
                {
                    _organism = o;
                }
            }

            if (_organism == null)
                Debug.Log("Not enought DNA to create Advanced Mate");
            else
            {
                Vector3 randomPos = new Vector3(_organism.transform.position.x, ySpawn, _organism.transform.position.z);

                GameObject newOrga = Instantiate(_prefabAdvanced, randomPos, Quaternion.identity);

                GameVariables.ColonieEnergie -= GameSettings.OrganismAdvancedCost;

                GameVariables.CurrentDNA -= GameSettings.AdvancedDNACost;

                Destroy(_organism.gameObject);
            }
        }
    }

    public void DuplicateMate()
    {
        if (GameVariables.ColonieMates.Count != 0 && GameVariables.TailleColonie < GameSettings.MaxColonieTaille && GameVariables.ColonieEnergie > GameSettings.OrganismNormalCost && GameVariables.CurrentDNA >= 5)
        {
            int rand = UnityEngine.Random.Range(0, GameVariables.ColonieMates.Count);
            _organism = GameVariables.ColonieMates[rand];
            Vector3 randomPos = new Vector3(_organism.transform.position.x, ySpawn, _organism.transform.position.z);

            GameObject newOrga = Instantiate(_prefabNormal, randomPos, Quaternion.identity);
            GameVariables.ColonieEnergie -= GameSettings.OrganismNormalCost;

        }
    }

    public void InvokeNormalMate()
    {
        if(GameVariables.TailleColonie == 0)
        {
            Vector3 playerPosition = GameObject.Find("Player").gameObject.transform.position;

            Vector3 randomPos = new Vector3(UnityEngine.Random.Range(playerPosition.x - 10, playerPosition.x + 10), ySpawn, UnityEngine.Random.Range(playerPosition.z - 10, playerPosition.z + 10));

            GameObject newOrga = Instantiate(_prefabNormal, randomPos, Quaternion.identity);
            GameVariables.ColonieEnergie -= GameSettings.OrganismNormalCost;
        }

    }

    private void UpdateSpawn()
    {
        _minX = (int)GameObject.FindGameObjectWithTag("Player").gameObject.transform.position.x + minX;
        _maxX = (int)GameObject.FindGameObjectWithTag("Player").gameObject.transform.position.x + maxX;
        _minZ = (int)GameObject.FindGameObjectWithTag("Player").gameObject.transform.position.x + minZ;
        _minZ = (int)GameObject.FindGameObjectWithTag("Player").gameObject.transform.position.x + minZ;
    }

    IEnumerator VirusCreation()
    {
        Debug.Log("Création virus enclenchés !");
        while (true)
        {
            // Crée une coroutine attendant t secondes avant d'exécuter la suite
            yield return new WaitForSeconds(tVirus);
            UpdateSpawn();
            if (GameVariables.NbVirus < MaxVirus)
            {
                Vector3 randomPos = new Vector3(UnityEngine.Random.Range(_minX, _maxX), ySpawn, UnityEngine.Random.Range(_minZ, _maxZ));
                Instantiate(_prefabVirus, randomPos, Quaternion.identity);
                GameVariables.NbVirus += 1;
            }
        }
    }

    IEnumerator BacteriaCreation()
    {
        while (true)
        {
            // Crée une coroutine attendant t secondes avant d'exécuter la suite
            yield return new WaitForSeconds(tBacteries);
            UpdateSpawn();
            if (GameVariables.NbBacteries < MaxBacteries)
            {
                Vector3 randomPos = new Vector3(UnityEngine.Random.Range(_minX, _maxX), ySpawn, UnityEngine.Random.Range(_minZ, _maxZ));
                Instantiate(_prefabBacterie, randomPos, Quaternion.identity);
                GameVariables.NbBacteries += 1;
            }
        }
    }
}
