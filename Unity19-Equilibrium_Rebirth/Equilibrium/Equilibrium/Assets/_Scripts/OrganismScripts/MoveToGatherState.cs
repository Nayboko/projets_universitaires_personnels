﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToGatherState : BaseState
{
    private Organism _organism;

    public MoveToGatherState(Organism organism) : base(organism.gameObject)
    {
        _organism = organism;
    }

    public override Type Tick()
    {
        if(_organism.Target == null)
        {
            return typeof(WanderState);
        }

        // Move to Nutriment

        transform.LookAt(_organism.Target);
        transform.Translate(Vector3.forward * Time.deltaTime * GameSettings.OrganismMoveSpeed);

        var distance = Vector3.Distance(transform.position, _organism.Target.transform.position);
        if(distance <= GameSettings.GatherRange)
        {
            return typeof(GatherState);
        }

        return null;
    }
}
