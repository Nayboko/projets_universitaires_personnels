﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseState : BaseState
{
    private Organism _organism;

    public ChaseState(Organism organism) : base(organism.gameObject)
    {
        _organism = organism;
    }

    public override Type Tick()
    {
        if(_organism.Target == null)
        {
            if (_organism.Team == Team.Colonie)
                return typeof(WanderState);
            else return typeof(WanderStateOthers);
        }

        transform.LookAt(_organism.Target);
        transform.Translate(Vector3.forward * Time.deltaTime * GameSettings.OrganismMoveSpeed);

        var distance = Vector3.Distance(transform.position, _organism.Target.transform.position);
        if(distance <= GameSettings.AttackRange)
        {
            return typeof(DefendState);
        }

        return null;
    }
}
