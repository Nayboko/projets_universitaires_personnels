﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldBehaviour : MonoBehaviour
{
    
    GameObject canvasObj;
    Transform childEnergie;
    Transform childConso;
    Transform childPop;
    Transform childDNA;
    Transform childScore;

    Text energieText;
    Text consoText;
    Text popText;
    Text DNAText;
    Text ScoreText;

    // Start is called before the first frame update
    void Start()
    {
        canvasObj = GameObject.Find("Canvas");
        Init();
    }

   

    private void Update()
    {
        // Mise à jour de l'affichage
        energieText.text = "" + GameVariables.ColonieEnergie;

        consoText.text = "- " + GameVariables.CurrentConsoEnergie;

        DNAText.text = "" + GameVariables.CurrentDNA;

        popText.text = GameVariables.TailleColonie + " / " + GameSettings.MaxColonieTaille;

        ScoreText.text = "" + GameVariables.GameScore;
    }

    private void Init()
    {
        // Init de l'energie totale
        childEnergie = canvasObj.transform.Find("lblEnergie");
        energieText = childEnergie.GetComponent<Text>();
        GameVariables.ColonieEnergie = GameSettings.BeginEnergy;

        // Init de la conso de la colonie
        childConso = canvasObj.transform.Find("lblConso");
        consoText = childConso.GetComponent<Text>();

        // Init de la population
        childPop = canvasObj.transform.Find("lblPop");
        popText = childPop.GetComponent<Text>();

        // Init du DNA
        childDNA = canvasObj.transform.Find("lblDNA");
        DNAText = childDNA.GetComponent<Text>();
        GameVariables.CurrentDNA = GameSettings.BeginDNA;

        // Init Score
        childScore = canvasObj.transform.Find("lblScore");
        ScoreText = childScore.GetComponent<Text>();
    }

    public void AddEnergie()
    {
        GameVariables.ColonieEnergie += UnityEngine.Random.Range(GameSettings.MinEnergieGain, GameSettings.MaxEnergieGain);
    }

    public void LossEnergie(int energie)
    {
        GameVariables.ColonieEnergie -= energie;
        if (GameVariables.ColonieEnergie < 0) GameVariables.ColonieEnergie = 0;
    }

    public void AddDNA()
    {
        GameVariables.CurrentDNA += UnityEngine.Random.Range(GameSettings.MinDNAGain, GameSettings.MaxDNAGain);

    }


    public void LossDNA(int dna)
    {
        GameVariables.CurrentDNA -= dna;
    }

    public void AddScore(int score)
    {
        GameVariables.GameScore += score;
    }

    public void LossScore(int score)
    {
        GameVariables.GameScore -= score;
    }

}
