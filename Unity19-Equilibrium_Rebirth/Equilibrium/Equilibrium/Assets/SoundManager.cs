﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{
    public FMOD.Studio.EventInstance EffectInstance;
    public FMOD.Studio.EventInstance MusicInstance;

    private Button PlayButton;
    private Button OptionsButton;
    private Button CreditButton;
    private Button BackButton;
    private Button QuitButton;

    private bool musicOn;

    // Start is called before the first frame update
    void Start()
    {
        musicOn = false;
    }

    private void Update()
    {
        var currentSelection = EventSystem.current.currentSelectedGameObject;

        // Check all interactions
        if (Input.GetMouseButtonDown(0) && currentSelection.GetComponent<IPointerDownHandler>() != null)
        {
            if (CheckEnableButtonsMenu() && (currentSelection == PlayButton.gameObject || currentSelection == OptionsButton.gameObject || currentSelection == QuitButton.gameObject))
            {
                EffectInstance = FMODUnity.RuntimeManager.CreateInstance("event:/UI_MENU");
                EffectInstance.setVolume(GameSettings.EffectVolumeLevel);
                EffectInstance.start();
                EffectInstance.release();
            }

            if (CheckEnableButtonsMenuOptions() && (currentSelection == CreditButton.gameObject || currentSelection == BackButton.gameObject))
            {
                EffectInstance = FMODUnity.RuntimeManager.CreateInstance("event:/UI_MENU");
                EffectInstance.setVolume(GameSettings.EffectVolumeLevel);
                EffectInstance.start();
                EffectInstance.release();
            }
        }

        if (!musicOn)
        {
            if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("MenuGame"))
            {
                MusicInstance = FMODUnity.RuntimeManager.CreateInstance("event:/EquilibriumOST");
                MusicInstance.setVolume(GameSettings.MusicVolumeLevel);
                MusicInstance.start();
                musicOn = true;
            }
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("LoadingScreen"))
            {
                if (IsPlaying(MusicInstance))
                {
                    MusicInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                    MusicInstance.release();
                }
                MusicInstance = FMODUnity.RuntimeManager.CreateInstance("event:/EquilibriumOST");
                MusicInstance.setVolume(GameSettings.MusicVolumeLevel);
                MusicInstance.start();
                musicOn = true;
            }
        }
        else
        {
            if (IsPlaying(MusicInstance))
            {
                MusicInstance.setVolume(GameSettings.MusicVolumeLevel);
            } 
        }

        EffectInstance.setVolume(GameSettings.EffectVolumeLevel);
    }

    private bool CheckEnableButtonsMenu()
    {
        GameObject buttonObject = null;
        if ((buttonObject = GameObject.Find("PlayButton")) == null) return false;
        else PlayButton = buttonObject.GetComponent<Button>();
        if ((buttonObject = GameObject.Find("OptionsButton")) == null) return false;
        else OptionsButton = buttonObject.GetComponent<Button>();
        if ((buttonObject = GameObject.Find("QuitButton")) == null) return false;
        else QuitButton = buttonObject.GetComponent<Button>();

        return true;
    }

    private bool CheckEnableButtonsMenuOptions()
    {
        GameObject buttonObject = null;
        if ((buttonObject = GameObject.Find("BackButton")) == null) return false;
        else BackButton = buttonObject.GetComponent<Button>();
        if ((buttonObject = GameObject.Find("CreditButton")) == null) return false;
        else CreditButton = buttonObject.GetComponent<Button>();
        
        return true;
    }

    bool IsPlaying(FMOD.Studio.EventInstance instance)
    {
        FMOD.Studio.PLAYBACK_STATE state;
        instance.getPlaybackState(out state);
        return state != FMOD.Studio.PLAYBACK_STATE.STOPPED;
    }

}
