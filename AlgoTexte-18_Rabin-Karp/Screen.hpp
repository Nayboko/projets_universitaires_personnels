#ifndef SCREEN_HPP
#define SCREEN_HPP

#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "IScreen.hpp"

#define COLOR_GRIS sf::Color(96,96,96, getAlpha()/alpha_div)
#define COLOR_NOIR sf::Color(0,0,0, getAlpha()/alpha_div)
#define COLOR_BLUE sf::Color(0,105,255, getAlpha()/alpha_div)
#define COLOR_RED sf::Color(255,65,95, getAlpha()/alpha_div)
#define COLOR_GREEN sf::Color(0,230,140, getAlpha()/alpha_div)
#define COLOR_BLANC sf::Color(255,255,255, getAlpha()/alpha_div)


class Screen : public virtual IScreen{

private:
	bool		Running;
	int 		etape;
	int 		alpha;

	std::string screenTitle;
	

public:
	sf::Event	Event;
	sf::Font	Font;
	sf::Texture	T_bg; 	// Texture du Background
	sf::Sprite	S_bg;	// Sprite du Background
	int 		alpha_max;
	int			alpha_div;

	Screen();
	Screen(std::string title, std::string fPath, std::string tPath);
	virtual ~Screen();

	virtual int		run(sf::RenderWindow *&App);
	
	virtual void	draw(sf::RenderWindow &App);

	virtual int		processEvent(sf::RenderWindow *&App);

	virtual bool	TextureLoad(sf::Texture &t, const std::string path);

	virtual	bool	FontLoad(sf::Font *f, const std::string path);

	virtual bool	isButtonClicked(sf::Sprite *spr, sf::RenderWindow *&App);

	virtual void	setText(sf::Text &t, std::wstring s, sf::Font f, sf::Color c, unsigned int size);

	virtual void	positionText(sf::Text &t, float _X, float _Y);

	virtual void	TextureToSprite(sf::Sprite &s, sf::Texture &t, sf::Color c, std::string path, float _X, float _Y);

	virtual bool	getRunning()const;
	virtual void	setRunning(const bool b);

	virtual std::string getTitle() const;

	virtual int		getAlpha()const;
	virtual void	setAlpha(int _alpha);

	virtual int getEtape()const;

	virtual void setEtape(const int e);

	virtual sf::Font getFont()const;

};


#endif