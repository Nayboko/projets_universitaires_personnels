#include <iostream>

#include "Credits.hpp"

Credits::Credits():Screen(){}

Credits::Credits(std::string title, std::string fPath, std::string tPath):Screen(title, fPath, tPath){}

Credits::~Credits(){}

int Credits::run(sf::RenderWindow *&App){
	setRunning(true);
	unsigned int 		menu_font_size = App->getSize().y * 0.05;   // Taille des fonts Menu
	float 		X_pos_button = App->getSize().x; // Position des bouttons en X
	float		Y_pos_button = App->getSize().y;

	Button Precedent((X_pos_button * 0.475f), (Y_pos_button*0.05f), 0.2f, 0.2f, menu_font_size, L"Precedent", sf::Color(255,255,255,this->getAlpha()), sf::Color(255,255,255,this->getAlpha()), "./Contents/BebasNeue-Regular.ttf", "./graphismes/logo/logo.png");

	ListButton.push_back(Precedent);

	while(this->getRunning()){
		int error = processEvent(App);
		if(error == -1){
			return -1;
		}else if(error == -2){
			return 0;
		}

		App->clear();

		App->draw(S_bg);

		for(size_t i = 0; i < ListButton.size(); i++){
			App->draw(ListButton[i].S_button);
		}

		App->display();
	}
	return 0;

}

int Credits::processEvent(sf::RenderWindow *&App){
	sf::Event Event;
	while(App->pollEvent(Event)){
		// L'user a fermé la fenêtre
		if(Event.type == sf::Event::Closed){
			App->close();
			this->setRunning(false);
			return -1;
		}

		// Si on a fait un clic de souris left
		if (Event.mouseButton.button == sf::Mouse::Left)
		{
			// Bouton Tester
			if(isButtonClicked(&ListButton[0].S_button, App)){
		    	std::cout << "L'utilisateur est retourné au menu" << std::endl;
		    	setRunning(false);
		    	return -2;
		    }
		}

		// Si on a fait un clic de souris droit
		if (Event.mouseButton.button == sf::Mouse::Right)
		{
		    std::cout << "the right button was pressed" << std::endl;
		    std::cout << "mouse x: " << Event.mouseButton.x << std::endl;
		    std::cout << "mouse y: " << Event.mouseButton.y << std::endl;
		}

	}
	return 0;
}