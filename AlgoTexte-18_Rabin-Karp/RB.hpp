#ifndef RB_H
#define RB_H

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <iostream>
#include <cstdio>
#include <string>
#include <cstdlib>
#include <utility>
#include <string>
#include <utility>

class RB{
private:
	std::string				sequence;	// Chaîne à analyser
	int						seq_len;	// Taille de la séquence
	std::string				motif;		// Motif à trouver
	int						motif_len;	// Taille du motif;
	int						premier;	// Nombre premier pour hashage
	int						charset;	// Charset
	int						occ;		// Nombre d'occurence du motif

	int 					hashPat; 	// Valeur de hashage du pattern
	int 					hashSeq; 	// Valeur de hashage de la Sequence
	int 					hashVal; 	// Valeur unique pour calcul des hashages

public:

	std::vector<std::pair<std::string, std::string>> tabSeq;	// Etat de la recherche, les indices de
										// sont le numéro d'étape
	std::vector<std::string>		positions;	// Va contenir les positions des sous-séquences
	
	//	#####################################
	//	#	Constructeurs et Destructeur	#
	//	#####################################

	// Constructeur par défaut
	RB();

	// Constructeur paramétré
	RB(std::string seq, std::string pat, int p);

	// Destructeur
	virtual ~RB();

	//	#####################################
	//	#		Méthodes principales		#
	//	#####################################

	// Handleur principal de la classe
	// Implémente l'algorithme de Rabin-Karp
	// Et fonction de hashage optimisée (!= empreinte de Rabin)
	virtual int	run();

	// Remet à zéro tous les éléments de la classe
	virtual void reset();

	//	#####################################
	//	#		Getters et setters			#
	//	#####################################

	// Retourne la séquence à analyser
	virtual std::string getSequence() const;

	// Permet de modifier la séquence à analyser
	virtual void setSequence(const std::string seq);

	// Retourne la longueur de la séquence
	virtual int	getSeqLen() const;

	// Retourne le motif à chercher
	virtual std::string getMotif() const;

	// Permet de modifier le motif
	virtual void setMotif(const std::string pat);

	// Retourne la longueur du motif
	virtual int getMotifLen()const;
	
	// Retourne le nombre premier utilisé pour la fonction de hashage
	virtual int getPremier() const;

	// Permet de modifier le nombre premier
	virtual void setPremier(const int p);

	// Retourne la base utilisée pour le hashage
	virtual int getCharset() const;

	// Permet de modifier la base
	virtual void setCharset(const int c);

	// Retourne le nombre d'occurences trouvées
	// à la fin de l'Algorithme de Rabin-Karp
	virtual int getOcc() const;

	// Permet de modifier le nombre d'occurences
	virtual void setOcc(const int _occ);

	// Retourne la valeur de hashage du motif
	virtual int getHashPat()const;

	// Permet de modifier la valeur de hashage du motif
	virtual void setHashPat(const int p);

	// Retourne la valeur de hashage de la sous-séquence 
	// analysée
	virtual int getHashSeq()const;

	// Permet de modifier la valeur de hashage de la sous
	// séquence
	virtual void setHashSeq(const int s);

	// Retourne la valeur de hashage générale
	virtual int getHashVal()const;

	// Permet de modifier la valeur de hashage générale
	virtual void setHashVal(const int h);

	
	
};

#endif