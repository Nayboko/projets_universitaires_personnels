/* 	Implémentation de l'algorithme Rabin-Karp
 *	Entrée : txt, un flux de données, motif, une chaîne de caractère
 *  Sortie : Retourne toutes les occurences du motif
 */

#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

#define d 256 /* Taille max du flux de données */

void rabin_karp(char* seq, char* motif, int q);

int main(int argc, char** argv, char** env){

	char *seq = "MISSISSIPI";			// Sequence de recherche
	char *motif = "SIP";				// Motif recherché
	int prime = 3; 					// Nombre premier pour le hashage
	rabin_karp(seq, motif, prime);

	return 0;
}

void rabin_karp(char* seq, char* motif, int prime){
	int motif_len = strlen(motif); 	// Taille du motif
	int seq_len = strlen(seq);		// Taille de la séquence
	int i, j, cpt;				

	int hashPat = 0; 	// Valeur de hashage du pattern
	int hashSeq = 0; 	// Valeur de hashage de la Sequence
	int hashVal = 1; 	// Valeur unique pour calcul des hashages
	cpt = 0;	// Compteur du nombre d'occurence

	// Calcul de la valeur de hashage de la recherche
	for(i = 0; i < motif_len - 1; i++){
		hashVal = (hashVal * d) % prime;
	}

	// Calcul des valeurs de hashage (pattern, fenêtre de recherche)
	for(i = 0; i < motif_len; i++){
		hashPat = (hashPat * d + motif[i]) % prime;
		hashSeq = (hashSeq * d + seq[i]) % prime;
		cout << hashSeq << endl;
	}


	// Chevauchement du motif sur la séquence
	for(i = 0; i <= seq_len - motif_len; i++){

		/* Compare les valeurs de hash entre celle du motif et
			la sous-séquence */
		if(hashPat == hashSeq){
			// Vérification char par char
			for(j = 0; j < motif_len; j++){
				if(seq[i + j] != motif[j]){
					break;
				}
			}

			// Si motif[0..motif_len - 1] == seq[i.. i + motif_len -1]
			if(j == motif_len){
				cout << "Le motif a été trouvé à l'index " << i << endl;
				cpt++;
			}
		}

		// Calcul de la valeur de hashage de la prochaine 
		// fenêtre de recherche
		if(i < seq_len - motif_len){
			hashSeq = (d * (hashSeq - seq[i] * hashVal) + seq[i + motif_len]) % prime;
			// Si le calcul tombe sur une valeur négative
			if(hashSeq < 0){
				hashSeq = (hashSeq + prime);
			}
		}
	}

	cout << "Nombre d'occurences du motif : " << cpt << " fois." << endl;
}