#include <iostream>

#include "App.hpp"

App::App(){
	std::cout << "Initialisation de l'application avec les valeurs par défaut." << std::endl;
	this->screen = 0;
	this->windowWidth = 1280;
	this->windowHeight = 720;
	this->pixelPerPoint = 32;
	this->framerate = 60;
	this->windowTitle = "Rabin-Karp C++/SFML";

	this->Application = new sf::RenderWindow(sf::VideoMode(this->windowWidth, this->windowHeight, this->pixelPerPoint), this->windowTitle, sf::Style::Close | sf::Style::Titlebar);

	Application->setFramerateLimit(this->framerate);
}

App::App(int screen, int windowWidth, int windowHeight, int pixelPerPoint, unsigned int framerate , std::string windowTitle){
	std::cout << "Initialisation de l'application avec les valeurs paramétrées." << std::endl;
	this->screen = screen;
	this->windowWidth = windowWidth;
	this->windowHeight = windowHeight;
	this->pixelPerPoint = pixelPerPoint;
	this->framerate = framerate;
	this->windowTitle = windowTitle;

	this->Application = new sf::RenderWindow(sf::VideoMode(this->windowWidth, this->windowHeight, this->pixelPerPoint), this->windowTitle, sf::Style::Close | sf::Style::Titlebar);

	Application->setFramerateLimit(this->framerate);

}



	App::~App(){
		std::cerr << "Fin de l'application " << windowTitle << "." << std::endl;
		delete Application;
	}

	int App::run(){

		// On ajoute les différents écrans
		Menu* menu = new Menu("Menu", "./Contents/BebasNeue-Regular.ttf", "./graphismes/titre/menu.png");
		Screen.push_back(menu);
		Tutoriel* tuto =  new Tutoriel("Tutoriel", "./Contents/arial.ttf", "./graphismes/fond/background.png");
		Screen.push_back(tuto);
		Tester* tester = new Tester("Tester", "./Contents/arial.ttf", "./graphismes/fond/background.png");
		Screen.push_back(tester);
		Credits* credits =  new Credits("Crédits", "./Contents/BebasNeue-Regular.ttf", "./graphismes/credits/credits.png");
		Screen.push_back(credits);
		
		while(this->getScreen() >= 0){
			std::cout << " ### Lancement de l'écran " << Screen[screen]->getTitle() << " ###" << std::endl;
			this->screen = Screen[screen]->run(Application);
		}

		return EXIT_SUCCESS;
	}

	int App::getScreen() const{ return this->screen;}
	void App::setScreen(const int screen){
		this->screen = screen;
	}

	int App::getWindowWidth() const{
		return this->windowWidth;
	}
	void App::setWindowWidth(const int width){
		this->windowWidth = width;
	}

	int App::getWindowHeight() const{
		return this->windowHeight;
	}
	void App::setWindowHeight(const int height){
		this->windowHeight = height;
	}

	int App::getPixelPerPoint() const{
		return this->pixelPerPoint;
	}
	void App::setPixelPerPoint(const int ppp){
		this->pixelPerPoint = ppp;
	}

	unsigned int App::getFramerate() const{
		return this->framerate;
	}