#include <iostream>
#include <unistd.h>

#include "Screen.hpp"
#include "Tutoriel.hpp"
#include "RB.hpp"

Tutoriel::Tutoriel():Screen(){
	this->etape_algo_naif = 0;
	this->found_algo_naif = false;
	this->etape = 1;
	this->rb = new RB();
	setAlpha(0);
	this->isCharged = false;
}

Tutoriel::Tutoriel(std::string title, std::string fPath, std::string tPath):Screen(title, fPath, tPath){
	this->etape_algo_naif = 0;
	this->found_algo_naif = false;
	this->etape = 1;
	this->rb = new RB();
	setAlpha(0);
	this->isCharged = false;
}

Tutoriel::~Tutoriel(){ delete[] rb;}

int		Tutoriel::run(sf::RenderWindow *&App){
	setRunning(true);
	unsigned int 		menu_font_size = App->getSize().y * 0.05;   // Taille des fonts Menu
	float 		X_pos_button = App->getSize().x; // Position des bouttons en X
	float		Y_pos_button = App->getSize().y;

	Button Precedent((X_pos_button * 0.60f), (Y_pos_button*0.85f), 0.8f, 0.6f, menu_font_size, L"Precedent", sf::Color(255,255,255,255), sf::Color(255,255,255,this->getAlpha()), "./Contents/BebasNeue-Regular.ttf", "./graphismes/bouton/precedent.png");

	Button Suivant((X_pos_button * 0.95f), (Y_pos_button*0.85f), 0.8f, 0.6f, menu_font_size, L"Suivant", sf::Color(255,255,255,255), sf::Color(255,255,255,this->getAlpha()), "./Contents/BebasNeue-Regular.ttf", "./graphismes/bouton/suivant.png");
	Button Menu((X_pos_button * 0.45f), (Y_pos_button*0.05f), 0.2f, 0.2f, menu_font_size, L"Menu", sf::Color(255,255,255,255), sf::Color(255,255,255,this->getAlpha()), "./Contents/BebasNeue-Regular.ttf", "./graphismes/logo/logo.png");

	ListButton.push_back(Precedent);
	ListButton.push_back(Suivant);
	ListButton.push_back(Menu);

	int occ = rb->run();
	std::cout 	<< "Nombre d'occurence de " << rb->getMotif()
				<< " dans le mot " << rb->getSequence()
				<< " : " << occ << std::endl;


    if(!getRunning())
    	setAlpha(alpha_max);

	while(getRunning()){
		int error = processEvent(App);
		switch(error){
			case -1:
				App->close();
		    	setRunning(false);
				return -1;
			break;
			case 1:
				std::cout << "=> \033[1;31mRetour au menu principal\033[0m"<< std::endl;
				setRunning(false);
				setEtape(1);
				rb->setOcc(0);
				rb->reset();
				return 0;
			break;
		}

		if(getEtape() <= 0)
			setEtape(1);

		if(getAlpha() < alpha_max)
			setAlpha(getAlpha() + 5);

		// Gestion affichage
		App->clear();
		App->draw(S_bg);
		for(size_t i = 0; i < ListButton.size(); i++){
			App->draw(ListButton[i].S_button);
		}
		afficheIntro(App);
		AlgoNaif(App);
		afficheResultats(App);
		afficheCompare(App);
		App->display();
	}

	return 0;

}

int		Tutoriel::processEvent(sf::RenderWindow *&App){
	sf::Event Event;
	while(App->pollEvent(Event)){
		// L'user a fermé la fenêtre
		if(Event.type == sf::Event::Closed){
			App->close();
			this->setRunning(false);
			return -1;
		}

		// Si on a fait un clic de souris left
		if (Event.mouseButton.button == sf::Mouse::Left)
		{
			// Bouton Precedent
			if(isButtonClicked(&ListButton[0].S_button, App)){
		    	setEtape(getEtape() - 1);
		    	setAlpha(0);
		    	std::cout << "Passage à l'étape " << getEtape() << std::endl;
		    }
		    // Bouton Suivant
		    if(isButtonClicked(&ListButton[1].S_button, App)){
		    	setEtape(getEtape() + 1);
		    	setAlpha(0);
		    	std::cout << "Passage à l'étape " << getEtape() << std::endl;
		    }

		    if(isButtonClicked(&ListButton[1].S_button, App) && getEtape() == 14){
		    	return 1;
		    }
		    if(isButtonClicked(&ListButton[2].S_button, App)){
		    	return 1;
		    }
		}

		// Si on a fait un clic de souris droit
		if (Event.mouseButton.button == sf::Mouse::Right)
		{
		    std::cout << "the right button was pressed" << std::endl;
		    std::cout << "mouse x: " << Event.mouseButton.x << std::endl;
		    std::cout << "mouse y: " << Event.mouseButton.y << std::endl;
		}

	}
	return 0;
}

void	Tutoriel::afficheIntro(sf::RenderWindow *&App){
	resetContenu();
	resetTextElem();

	// On crée les phrases qui vont être affichées
	switch(getEtape()){
		// Etape 1 : Généralités
		case 1:
			addContenu(L"La recherche de sous-chaînes");
			addContenu(L"C'est un important problème en informatique correspondant à la\nrecherche d'un motif donné dans une séquence.");
			addContenu(L"Il existe plusieurs façon d'y répondre, soit par force brute lorsqu'on\nvérifie chaque entrée une par une, soit par heuristique si le contex-\nte le permet, ce qui sera plus efficace.");
			addContenu(L"Chaque algorithme developpé aura donc une complexité en temps\net en espace mémoire en fonction de la structure de données,\ndu domaine et de la méthode utilisée.");
		break;
		case 2:
			addContenu(L"L'algorithme naïf");
			addContenu(L"Cet algorithme va comparer le motif donné à l'ensemble des \nsous-chaînes et ce caractère par caractère.");
			addContenu(L"Il va parcourir chaque caractère de la séquence jusqu'à ce qu'il\nen trouve un identique au premier caractère du motif, va parcourir les\nsuivants de la sous-chaîne et afficher l'indice de ce dernier dans la \nchaîne initiale.");
			addContenu(L"Complexité en temps: O(n * m)");
		break;
		case 3:
			addContenu(L"L'algorithme naïf : Exemple");
			addContenu(L"Text = ");
				addContenu(L"A");
				addContenu(L"T");
				addContenu(L"T");
				addContenu(L"G");
				addContenu(L"C");
				addContenu(L"A");
				addContenu(L"T");
			addContenu(L"Motif = ");
				addContenu(L"T");
				addContenu(L"G");
				addContenu(L"C");
			addContenu(L"Position Occurence :");
				addContenu(L"3");
		break;
		case 4:
			addContenu(L"L'algorithme de Rabin-Karp");
			addContenu(L"Cet algorithme, proposé par Michael O. Rabin et Richard M. Karp en 1987,\n\
va faire glisser le motif sur la séquence, de la même manière que l'algo naïf.");
			addContenu(L"La différence réside dans le fait qu'il va faire correspondre les\nsous-chaînes et le motif à des valeurs de hashage (avec pour clé\nun nombre premier suffisament grand) et qu'il va comparer\navant de vérifier caractère par caractère.");
			addContenu(L"Complexité en temps: moy -> O(n+m)\t\tpire -> O(nm) (quadratique).");
		break;
		case 5:
			addContenu(L"Fonction de Hashage");
			addContenu(L"Une fonction de hashage va associer une donnée à une empreinte \n\
calculée. Cette empreinte va permettre de comparer des données en\n\
tre-elles plus rapidement sans avoir besoin d'accéder aux détails\n\
des objets.");
			addContenu(L"Une fonction de hashage doit éviter le plus possible les collisions.\n\
Une collision intervient lorsqu'au moins deux données sont associées\n\
à la même empreinte. Il devient alors nécessaire de vérifier plus en\n\
détail ces objets.");
			addContenu(L"Le Hashage permet donc de comparer plus rapidement des données et les \n\
empreintes associées peuvent être facilement stockées dans un tableau \n\
clé - valeur à accès direct (en O(1)), plutôt qu'avec un arbre de recher-\n\
che ou une liste chaînée.");

		break;
		case 6:
			addContenu(L"Empreinte de Rabin");
			addContenu(L"Michael O. Rabin, prix Turing de 1979, proposa une fonction de\n\
hashage se basant sur un polynôme de degré m-1.");
			addContenu(L"Soit b une base correspondant au cardinal de l'ensemble de\n\
départ et p un polynôme irréductible ou un nombre premier et T un mot\n\
de longueur m.");
			addContenu(L"hash(T[0..m-1]) = (t0 + t1 * b + .. + tm-1 x b^m-1) mod p");
			addContenu(L"On obtient ainsi une empreinte pseudo-aléatoire et déroulante\n\
(une donnée est hashée sous-séquence par sous-séquence).");
		break;
		case 7:
			addContenu(L"Mise en place du hashage");
			addContenu(L"Soit b une base, ici on utilisera 256 vis-à-vis de l'encodage\ndes caractères qui se fait sur 8 bits.");
			addContenu(L"Soit q un nombre premier, ici 113, et m la taille du motif.");
			addContenu(L"Pour la valeur de hashage clé permettant de déterminer les sous-chaînes :");
			addContenu(L"hash_value = (hash_value * b) mod q");
			addContenu(L"Pour la valeur numérique liée au motif recherché :");
			addContenu(L"hash_motif = (hash_motif * b + motif[0..m - 1]) mod q");
			addContenu(L"Pour la première sous-séquence que l'on va traiter :");
			addContenu(L"hash_txt = (hash_txt * b + txt[i..i + m - 1]) mod q");
		break;
		default:
		break;

	}

	// À partir des phrases au dessus, on crée des éléments sf::Text
	for(unsigned int i = 0; i < sizeContenu(); i++){
		addTextElem(sf::Text());
		this->Text_Elements[i].setFont(Font);
		this->Text_Elements[i].setColor(sf::Color(45,45,45, getAlpha()/ alpha_div));
		this->Text_Elements[i].setString(this->contenu[i]);
	}

	// Initialisation du titre
	this->Text_Elements[0].setPosition(sf::Vector2f(App->getSize().x / 3, App->getSize().y * 0.32f - 25));
	this->Text_Elements[0].setCharacterSize(36);
	this->Text_Elements[0].setStyle(sf::Text::Bold);

	// Positionnement des éléments en fonction de l'étape
	switch(getEtape()){
		case 1:
			for(unsigned int i = 1, deca = 25; i < sizeTextElem(); i++, deca += 100){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
				this->Text_Elements[i].setCharacterSize(22);
			}
			
		break;
		case 2:
			for(unsigned int i = 1, deca = 25; i < sizeTextElem(); i++, deca += 125){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
				this->Text_Elements[i].setCharacterSize(22);
			}
			this->Text_Elements[3].setColor(COLOR_RED);
		break;
		case 3:
			this->Text_Elements[1].setPosition(sf::Vector2f(App->getSize().x / 4, App->getSize().y / 2));
			this->Text_Elements[1].setCharacterSize(48);

			// Lettres Séquence
			for(int i = 2, deca = 48; i < 9; i++, deca +=48){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 + 100 + deca, App->getSize().y / 2));
				this->Text_Elements[i].setCharacterSize(48);
			}


			this->Text_Elements[9].setPosition(sf::Vector2f(App->getSize().x / 4, App->getSize().y / 2 + App->getSize().y * 0.1));
			this->Text_Elements[9].setCharacterSize(48);

			// Lettres motif
			for(int i = 10, deca = 48; i < 13; i++, deca +=48){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 + 100 + deca, App->getSize().y / 2 + App->getSize().y * 0.1));
				this->Text_Elements[i].setCharacterSize(48);
			}

			// Position Occurence
			this->Text_Elements[13].setPosition(sf::Vector2f(App->getSize().x / 4, App->getSize().y / 2 + App->getSize().y * 0.2));
				this->Text_Elements[13].setCharacterSize(48);
				this->Text_Elements[13].setColor(sf::Color(0,0,0,0));

				this->Text_Elements[14].setPosition(sf::Vector2f(App->getSize().x * 0.7f , App->getSize().y / 2 + App->getSize().y * 0.2));
				this->Text_Elements[14].setCharacterSize(48);
				this->Text_Elements[14].setColor(sf::Color(0,0,0,0));

			if(!this->found_algo_naif){
			this->Text_Elements[2].setColor(sf::Color(0, 105, 255, getAlpha() / alpha_div));
			this->Text_Elements[10].setColor(sf::Color(0, 105, 255, getAlpha() / alpha_div));
			}else{
				this->Text_Elements[4].setColor(COLOR_GREEN);
				this->Text_Elements[5].setColor(COLOR_GREEN);
				this->Text_Elements[6].setColor(COLOR_GREEN);
				this->Text_Elements[10].setColor(COLOR_GREEN);
				this->Text_Elements[11].setColor(COLOR_GREEN);
				this->Text_Elements[12].setColor(COLOR_GREEN);

				// Affichage de la position de l'occurence
				this->Text_Elements[13].setColor(COLOR_NOIR);
				this->Text_Elements[14].setColor(COLOR_GREEN);
			}
		break;
		case 4:
			for(unsigned int i = 1, deca = 25; i < this->Text_Elements.size(); i++, deca += 125){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
				this->Text_Elements[i].setCharacterSize(22);
			}
			this->Text_Elements[3].setColor(COLOR_RED);	
		break;
		case 5:
			for(unsigned int i = 1, deca = 25; i < this->Text_Elements.size(); i++, deca += 125){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
				this->Text_Elements[i].setCharacterSize(21);
			}
		break;
		case 6:
			for(unsigned int i = 1, deca = 25; i < this->Text_Elements.size(); i++, deca += 80){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
				this->Text_Elements[i].setCharacterSize(22);
			}
			this->Text_Elements[3].setCharacterSize(26);
			this->Text_Elements[3].setColor(COLOR_BLUE);
			
		break;
		case 7:
			for(unsigned int i = 1, deca = 25; i < this->Text_Elements.size(); i++, deca += 45){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
				this->Text_Elements[i].setCharacterSize(21);
			}
			this->Text_Elements[4].setColor(COLOR_BLUE);
			this->Text_Elements[6].setColor(COLOR_BLUE);
			this->Text_Elements[8].setColor(COLOR_BLUE);
		break;
		default:
		break;
	}

	for(unsigned int i = 0; i < sizeTextElem(); i++){
		App->draw(this->Text_Elements[i]);
	}
}

void	Tutoriel::afficheResultats(sf::RenderWindow *&App){
	resetContenu();
	resetTextElem();

	std::string sequence = rb->getSequence();
	std::string motif = rb->getMotif();
	std::string hashVal = std::to_string(rb->getHashVal());
	std::string hashPat = std::to_string(rb->getHashPat());
	std::string hashSeq = std::to_string(rb->getHashSeq());
	std::string occ = std::to_string(rb->getOcc());
	std::vector<std::pair<std::string, std::string>> tabSeq = rb->tabSeq;
	std::vector<std::string> positions = rb->positions;

	std::string s1("string");
	std::string s2("string");
	std::string s3("string");
	
	std::wstring ws1;
	std::wstring ws2;
	std::wstring ws3;

	size_t tmp, _tmp;

	switch(getEtape()){
		case 8:
			addContenu(L"Hashage");
			s1 = rb->getSequence();
			ws1.assign(s1.begin(), s1.end());
			
			addContenu(L"Séquence à analyser:\t\t" + ws1);
			s1 = rb->getMotif();
			ws1.assign(s1.begin(), s1.end());
			addContenu(L"Motif à rechercher :\t\t\t" + ws1);
			
			s1 = std::to_string(rb->getCharset());
			ws1.assign(s1.begin(), s1.end());
			s2 = std::to_string(rb->getPremier());
			ws2.assign(s2.begin(), s2.end());
			addContenu(L"hash_value = (hash_value * " + ws1 + L") % " + ws2);
			s1 = std::to_string(rb->getHashVal());
			ws1.assign(s1.begin(), s1.end());
			addContenu(L"=> hash_value = " + ws1);

			s1 = std::to_string(rb->getCharset());
			ws1.assign(s1.begin(), s1.end());
			s2 = std::to_string(rb->getPremier());
			ws2.assign(s2.begin(), s2.end());
			s3 = motif;
			ws3.assign(s3.begin(), s3.end());
			addContenu(L"hash_motif = (hash_motif * " + ws1 + L" + motif[i..m - 1]) % " + ws2);
			s1 = std::to_string(rb->getHashPat());
			ws1.assign(s1.begin(), s1.end());
			addContenu(L"=> hash("+ws3+ L") = " + ws1);

			s1 = std::to_string(rb->getCharset());
			ws1.assign(s1.begin(), s1.end());
			s2 = std::to_string(rb->getPremier());
			ws2.assign(s2.begin(), s2.end());
			s3 = rb->tabSeq[0].first;
			ws3.assign(s3.begin(), s3.end());
			addContenu(L"hash_txt = (hash_txt * " + ws1 + L" + txt[i..i + m - 1]) % " + ws2);
			s1 = rb->tabSeq[0].second;
			ws1.assign(s1.begin(), s1.end());
			addContenu(L"=> hash("+ws3+L") = " + ws1);
		break;
		case 9:
			addContenu(L"Overlap");
			s1 = rb->getMotif();
			ws1.assign(s1.begin(), s1.end());
			s2 = std::to_string(rb->getHashPat());
			ws2.assign(s2.begin(), s2.end());
			addContenu(L"hash(" + ws1 + L") = " + ws2);
			for(size_t i = 0; i < tabSeq.size() - 1; i++){
				s1 = tabSeq[i].first;
				ws1.assign(s1.begin(), s1.end());
				s2 = tabSeq[i].second;
				ws2.assign(s2.begin(), s2.end());
				addContenu(L"hash(" + ws1 + L") = " + ws2);
			}
		break;
		case 10:
			addContenu(L"Conclusion");
			for(size_t i = 0; i < sequence.length(); i++){
				s1 = sequence[i];
				ws1.assign(s1.begin(), s1.end());
				addContenu(ws1);
			}

			addContenu(L"Position des motifs dans la séquence : ");
			for(size_t i = 0; i < positions.size(); i++){
				s1 = positions[i];
				ws1.assign(s1.begin(), s1.end());
				addContenu(ws1);
			}

			s1 = occ;
			ws1.assign(s1.begin(), s1.end());
			addContenu(L"Nombre d'occurences du motif : ");
			addContenu(ws1);
	}

	// À partir des phrases au dessus, on crée des éléments sf::Text
	for(size_t i = 0; i < sizeContenu(); i++){
		addTextElem(sf::Text());
		this->Text_Elements[i].setFont(Font);
		this->Text_Elements[i].setColor(sf::Color(45,45,45, getAlpha()/ alpha_div));
		this->Text_Elements[i].setString(this->contenu[i]);
	}

	// Initialisation du titre
	this->Text_Elements[0].setPosition(sf::Vector2f(App->getSize().x *0.45f- this->Text_Elements[0].getCharacterSize(), App->getSize().y *0.32f - 25));
	this->Text_Elements[0].setCharacterSize(36);
	this->Text_Elements[0].setStyle(sf::Text::Bold);

	// Positionnement des éléments en fonction de l'étape
	switch(getEtape()){

		case 8:
			for(size_t i = 1, deca = 25; i < this->Text_Elements.size(); i++, deca += 45){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x / 4 - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
					if(i == 4 || i == 6 || i == 8)
						this->Text_Elements[i].setColor(sf::Color(200,45,45, getAlpha()/ alpha_div));
					this->Text_Elements[i].setCharacterSize(26);
			}
		break;
		case 9:
			// Mise en évidence de la valeur du motif
			this->Text_Elements[1].setPosition(sf::Vector2f(App->getSize().x *0.45f- this->Text_Elements[0].getCharacterSize(), App->getSize().y / 3 - 75));
			this->Text_Elements[1].setColor(sf::Color(200,45,45, getAlpha()/ alpha_div));
			this->Text_Elements[1].setCharacterSize(22);
			this->Text_Elements[1].setStyle(sf::Text::Bold);

			tmp = this->Text_Elements.size() / 2;
			if(tmp % 2 != 0) tmp++;

			// Affichage des valeurs de chaque sous-séquence:
			for(size_t i = 2, deca = 25; i < tmp; i++, deca += 40){
					this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x *0.35f - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
					this->Text_Elements[i].setCharacterSize(20);
			}
			for(size_t i = tmp, deca = 25; i < this->Text_Elements.size(); i++, deca += 40){
					this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x * 0.55f - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
					this->Text_Elements[i].setCharacterSize(20);
			}

			// On colorie en vert les matchs 
			for(size_t i = 0; i < tabSeq.size(); i++){
				if(tabSeq[i].first == motif){
					this->Text_Elements[i+2].setColor(sf::Color(45,200,45, getAlpha()/ alpha_div));
				}
			}
		break;
		case 10:
			// On place les lettres de la séquence
			for(size_t i = 1, deca = 25; i <= sequence.length(); i++, deca += 45){
					this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x *0.25f - this->Text_Elements[i].getCharacterSize()  + deca, App->getSize().y * 0.4f));
					this->Text_Elements[i].setCharacterSize(30);
			}			

			//if(getAlpha() == alpha_max){
				// Marquage en vert des sous-séquence qui sont égales au motif
				for (size_t i = 1; i <= sequence.length(); ++i)
				{	
					for(size_t j = 0; j <= positions.size(); j++){
						if(positions[j] == std::to_string(i - 1)){
							for(size_t k = i; k < i + rb->getMotifLen(); k++)
							this->Text_Elements[k].setColor(sf::Color(45,200,45,getAlpha()/alpha_div));
						}
						tmp = i;
					}
				}

				// Position des motifs dans la séquence
				this->Text_Elements[tmp + 1].setPosition(sf::Vector2f(App->getSize().x *0.3f - this->Text_Elements[tmp].getCharacterSize()  + 25, App->getSize().y * 0.5f));
				this->Text_Elements[tmp + 1].setCharacterSize(26);
				this->Text_Elements[tmp + 1].setColor(sf::Color(86,216,239, getAlpha()/ alpha_div));

				for(size_t i = tmp + 2, deca = 25; i < tmp + 2 + positions.size(); i++, deca+= 50){
					this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x *0.3f - this->Text_Elements[i].getCharacterSize()  + deca, App->getSize().y * 0.55f));
					this->Text_Elements[i].setCharacterSize(30);
					this->Text_Elements[i].setColor(sf::Color(45,200,45, getAlpha()/ alpha_div));
					_tmp = i + 1;
				}

				// Nombre d'occurences
				this->Text_Elements[_tmp].setPosition(sf::Vector2f(App->getSize().x *0.3f - this->Text_Elements[_tmp].getCharacterSize() + 25, App->getSize().y * 0.65f));
				this->Text_Elements[_tmp].setCharacterSize(26);
				this->Text_Elements[_tmp].setColor(sf::Color(86,216,239, getAlpha()/ alpha_div));

				this->Text_Elements[_tmp + 1].setPosition(sf::Vector2f(App->getSize().x *0.7f - this->Text_Elements[_tmp].getCharacterSize() + 25, App->getSize().y * 0.65f));
				this->Text_Elements[_tmp + 1].setCharacterSize(30);
				this->Text_Elements[_tmp + 1].setColor(sf::Color(45,200,45, getAlpha()/ alpha_div));
				
			//}
		break;
	}

	// On draw tous les éléments textuels à l'écran
	for(unsigned int i = 0; i < sizeTextElem(); i++){
		App->draw(this->Text_Elements[i]);
	}
}

void Tutoriel::afficheCompare(sf::RenderWindow *&App){
	resetContenu();
	resetTextElem();
	switch(getEtape()){
		case 11:
			addContenu(L"Algorithme KMP");
			addContenu(L"L'algorithme de Knuth-Morris-Pratt permet d'éliminer les\n\
comparaisons qui peuvent être évitées grâce aux comparaisons précédentes et décale\n\
la fenêtre de recherche dans la séquence intelligemment.");
			addContenu(L"Il fonctionne de manière complètement similaire à l'algo\n\
naïf tant que la recherche réussie, et c'est en cas d'échec qu'il calcule le décalage\n\
nécessaire en fonction de la position du caractère ayant provoqué le dismatch.");
			addContenu(L"Complexité en temps: \tmeilleur -> O(m)\t\tpire -> O(m+n)");
		break;
		case 12:
			addContenu(L"Algorithme Boyer-Moore");
			addContenu(L"L'objectif de cet algorithme est de faire glisser une\n\
fenêtre de recherche en comparant caractère par caractère de la droite vers la\n\
gauche en commançant par la lettre la plus à droite.");
			addContenu(L"Il utilise également les décalages en cas d'echec, soit\n\
en se positionnant en fonction de la lettre ayant causé l'échec, soit du meilleur\n\
suffixe, c'est à dire que le suffixe qui match de la fenêtre de recherche devient le\n\
préfixe de la fenêtre suivante.");
			addContenu(L"Complexité en temps: \tmeilleur -> O(n/m)\t\tpire -> O(m+n)");
		break;
		case 13:
			addContenu(L"Récapitulatif des complexités");
			if(!isCharged && !TextureLoad(this->T_Image, "./Contents/compare_algo.png")){
				std::cerr << "\033[1.31mErreur dans le chargement de l'image!\033[0m" << std::endl;}
			else{
				S_Image.setTexture(T_Image);
				S_Image.setScale(1, 1);
				S_Image.setColor(COLOR_BLANC);
				S_Image.setPosition(App->getSize().x *0.2f,App->getSize().y * 0.4f);
				this->isCharged = true;
			}
		break;
		default:
		break;
	}

	// À partir des phrases au dessus, on crée des éléments sf::Text
	for(unsigned int i = 0; i < sizeContenu(); i++){
		addTextElem(sf::Text());
		this->Text_Elements[i].setFont(Font);
		this->Text_Elements[i].setColor(sf::Color(45,45,45, getAlpha()/ alpha_div));
		this->Text_Elements[i].setString(this->contenu[i]);
	}

	// Initialisation du titre
	this->Text_Elements[0].setPosition(sf::Vector2f(App->getSize().x *0.35f , App->getSize().y * 0.32f - 25));
	this->Text_Elements[0].setCharacterSize(36);
	this->Text_Elements[0].setStyle(sf::Text::Bold);

	switch(getEtape()){
		case 11:
			for(unsigned int i = 1, deca = 25; i < sizeTextElem(); i++, deca += 150){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x *0.2f - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
				this->Text_Elements[i].setCharacterSize(22);
			}
			this->Text_Elements[3].setColor(COLOR_RED);			
		break;
		case 12:
			for(unsigned int i = 1, deca = 25; i < sizeTextElem(); i++, deca += 150){
				this->Text_Elements[i].setPosition(sf::Vector2f(App->getSize().x *0.2f - this->Text_Elements[i].getCharacterSize(), App->getSize().y / 3 + deca));
				this->Text_Elements[i].setCharacterSize(22);
			}
			this->Text_Elements[3].setColor(COLOR_RED);	
		break;
		case 13:
			App->draw(S_Image);
		break;
		default:
		break;
	}
	// On draw tous les éléments textuels
	for(unsigned int i = 0; i < sizeTextElem(); i++){
		App->draw(this->Text_Elements[i]);
	}
}

void	Tutoriel::addContenu(std::wstring c){
	this->contenu.push_back(c);
}

void	Tutoriel::setContenu(int index, std::wstring newContenu){
	this->contenu.at(index) = newContenu;
}

void	Tutoriel::rmContenu(int index){
	this->contenu[index].erase();
}

void	Tutoriel::resetContenu(){
	this->contenu.clear();
}

unsigned int	Tutoriel::sizeContenu()const{
	return this->contenu.size();
}

void	Tutoriel::addTextElem(sf::Text t){
	this->Text_Elements.push_back(t);
}

void	Tutoriel::setTextElem(int index, sf::Text t){
	this->Text_Elements.at(index) = t;
}

void	Tutoriel::resetTextElem(){
	this->Text_Elements.clear();
}

unsigned int	Tutoriel::sizeTextElem()const{
	return this->Text_Elements.size();
}

void Tutoriel::AlgoNaif(sf::RenderWindow *&App){
	if(getEtape() == 3 && getAlpha() == alpha_max && etape_algo_naif != 7){
		etape_algo_naif++;

		if(etape_algo_naif == 2){
			sleep(1);
			this->Text_Elements[2].setColor(COLOR_NOIR);
			this->Text_Elements[3].setColor(COLOR_BLUE);
		}
		else if(etape_algo_naif == 3){
			sleep(1);
			this->Text_Elements[2].setColor(COLOR_NOIR);
			this->Text_Elements[4].setColor(COLOR_BLUE);
			this->Text_Elements[11].setColor(COLOR_RED);
		}
		else if(etape_algo_naif == 4){
			sleep(1);
			this->Text_Elements[2].setColor(COLOR_NOIR);
			this->Text_Elements[10].setColor(COLOR_BLUE);
			this->Text_Elements[11].setColor(COLOR_NOIR);
			this->Text_Elements[3].setColor(COLOR_NOIR);
			this->Text_Elements[4].setColor(COLOR_BLUE);
		}
		else if(etape_algo_naif == 5){
			sleep(1);
			this->Text_Elements[2].setColor(COLOR_NOIR);
			this->Text_Elements[10].setColor(COLOR_BLUE);
			this->Text_Elements[11].setColor(COLOR_BLUE);
			this->Text_Elements[4].setColor(COLOR_BLUE);
			this->Text_Elements[5].setColor(COLOR_BLUE);
		}
		else if(etape_algo_naif == 6){
			sleep(1);
			this->Text_Elements[2].setColor(COLOR_NOIR);
			this->Text_Elements[10].setColor(COLOR_BLUE);
			this->Text_Elements[11].setColor(COLOR_BLUE);
			this->Text_Elements[12].setColor(COLOR_BLUE);
			this->Text_Elements[4].setColor(COLOR_BLUE);
			this->Text_Elements[5].setColor(COLOR_BLUE);
			this->Text_Elements[6].setColor(COLOR_BLUE);
			this->found_algo_naif = true;
		}
	}
	for(unsigned int i = 0; i < sizeTextElem(); i++){
		App->draw(this->Text_Elements[i]);
	}
}