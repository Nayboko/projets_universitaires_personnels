#ifndef APP_HPP
#define APP_HPP

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "IScreen.hpp"
#include "Button.hpp"
#include "Menu.hpp"
#include "Tutoriel.hpp"
#include "Tester.hpp"
#include "Credits.hpp"


class App
{
private:
	std::vector<IScreen*> Screen;

	int screen;
	int windowWidth;
	int windowHeight;
	int pixelPerPoint;
	unsigned int framerate;
	std::string windowTitle;
	sf::RenderWindow *Application;

public:

	//	#####################################
	//	#	Constructeurs et Destructeur	#
	//	#####################################

	// Constructeur par défaut
	App();

	// Constructeur paramétré
	App(int screen, int windowWidth, int windowHeight, int pixelPerPoint, unsigned int framerate, std::string windowTitle);

	// Destructeur
	virtual ~App();

	//	#####################################
	//	#		Méthodes principales		#
	//	#####################################

	// Handler de la classe
	// Retourne un entier correspondant au n° de screen suivant
	virtual int run();

	//	#####################################
	//	#		Getters et setters			#
	//	#####################################

	// Retourne le n° de screen actuel
	virtual int getScreen() const;

	// Permet de modifier la valeur du screen actuel
	virtual void setScreen(const int screen);

	// Retourne la largeur de la fenêtre de l'application
	virtual int getWindowWidth() const;

	// Permet de modifier la largeur de la fenêtre
	virtual void setWindowWidth(const int width);

	 // Retourne la hauteur de la fenêtre de l'application
	virtual int getWindowHeight() const;

	// Permet de modifier la hauteur de la fenêtre
	virtual void setWindowHeight(const int height);

	// Retourne le nombre de pixels par points
	virtual int getPixelPerPoint() const;

	// Permet de modifier le nombre de pixels par points
	virtual void setPixelPerPoint(const int ppp);

	// Retourne le taux de rafraichissement des images
	virtual unsigned int getFramerate() const;
};

#endif