﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class buttons : MonoBehaviour {


	public AudioClip m_ButtonOnSound;

	public void playGame() {
	//	AudioSource.PlayClipAtPoint(m_ButtonOnSound, Camera.main.transform.position);
		SceneManager.LoadScene ("mainGame");
	}
		
	public void credits() {
		SceneManager.LoadScene("credits", LoadSceneMode.Single);
	}

	public void menu() {
		SceneManager.LoadScene("Menu", LoadSceneMode.Single);
	}

	public void quit() {
		Application.Quit();
	}
}
