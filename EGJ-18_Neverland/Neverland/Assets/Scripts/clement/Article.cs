﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Article : MonoBehaviour {

	public string titre;
	public string contexte;

	public int fiabilite;
	public int etat;
	public int lobby;
	public int notoriete;

	public int etatNonPublie;
	public int lobbyNonPublie;

	public int viewers; 

	public int tempsPublication;
	public bool isPublished;

	public float alpha;
    public Image rend;

	// Use this for initialization
	void Start () {
        rend = GetComponent<Image>();
		viewers = 100;
		tempsPublication = 5;
		isPublished = false;
        alpha = rend.color.a;
		Debug.Log (alpha);
	}
	
	// Update is called once per frame
	void Update () {
		if (isPublished) {
			Invoke ("autodestroy", tempsPublication);
			Material myMat = new Material(rend.material);
            myMat.color = new Color(1, 1, 1, alpha - ((Time.deltaTime/tempsPublication)*2));
	    	alpha = rend.material.color.a;
	    	rend.material = myMat;


			Debug.Log(alpha);
		}
	}

	void autodestroy() {
		Destroy (this.gameObject);
	}
}
