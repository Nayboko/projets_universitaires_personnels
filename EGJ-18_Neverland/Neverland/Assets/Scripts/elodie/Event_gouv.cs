﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_gouv : Event {

    // Use this for initialization
    void Start () {
        this.InitMsg();
        this.InitScore();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void InitMsg()
    {
        this.message = new List<string>();
        this.AddMessage("Evénement dans le Sport: un changement très positif pour la nation!\nSaurez-vous mettre notre sport national en valeur? ");
        this.AddMessage("Nouveau sondage sur le bien-être de nos habitants:\nVoulez-vous publier cette information?");
        this.SetNbEvents(this.message.Count);
        this.InitAlreadySeen();
    }

    public override void InitScore()
    {
        // NEFL
        this.scoreJauge = new int[2, 4] { { 0, 5, -10, 0 }, { 5, 0, 0, 5 } };
    }
}
