﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_info : Event {

    // public List<Article>;
    private void Awake()
    {
        this.InitMsg();
    }
    // Use this for initialization
    void Start () {
        this.InitScore();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void InitMsg()
    {
        this.message = new List<string>();
        this.AddMessage("Un pays allié a fait le bon choix!\nAllez-vous en parler?");
        this.AddMessage("ATTENTION! Vous venez de recevoir une information urgente, réagissez!");
        this.SetNbEvents(this.message.Count);
        this.InitAlreadySeen();
    }

    public override void InitScore()
    {
        this.scoreJauge = new int[2, 4] { { 5, 0, 0, 5 }, { -5, 0, -5, 0 } };
    }
}
