﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_osef : Event
{

    // Use this for initialization
    void Start()
    {
        this.InitMsg();
        this.InitScore();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void InitMsg()
    {
        this.message = new List<string>();
        this.AddMessage("J’ai accumulé environ 3000 ans de malheur et je suis mort 67 fois à cause de toutes les chaînes que je n’ai pas renvoyées! Ne fais pas la même erreur !");
        this.AddMessage("J’ai lu au moins 25 tomes de tous les préceptes du DALAI LAMA et j’ai accumulé du bonheur pour au moins les 4690 prochaines années!!\nFais de même!");
        this.SetNbEvents(this.message.Count);
        this.InitAlreadySeen();
    }

    public override void InitScore()
    {
        this.scoreJauge = new int[2, 4] { { 0, 0, 0, 0 }, { 0, 0, 0, 0 } };
    }





}
