﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_lobby : Event {

    // Use this for initialization
    void Start () {

        this.InitMsg();
        this.InitScore();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void InitMsg()
    {
        this.message = new List<string>();
        this.AddMessage("Un pacte sacré entre deux grandes sociétés du pays:\nSouhaitez vous les mettre en valeur en faisant remonter l'information?");
        this.AddMessage("Bourse: la société ARMA grimpe en bourse!");
        this.SetNbEvents(this.message.Count);
        this.InitAlreadySeen();
    }

    public override void InitScore() {
        this.scoreJauge = new int[2, 4] { { 5, 0, 0, 5 }, { -10, 0, 0, -10 } };
    }



}
