-- Tables virtuelles liées à Vente

CREATE OR REPLACE VIEW VENTE_PRODUIT
AS SELECT * FROM Produit;

CREATE OR REPLACE VIEW VENTE_LIEU
AS SELECT * FROM Lieu;

CREATE OR REPLACE VIEW VENTE_DATE
AS SELECT * FROM CurrentDate;

CREATE OR REPLACE VIEW VENTE_MAGASIN
AS SELECT * FROM Magasin;

-- Tables virtuelles liées à Stock

CREATE OR REPLACE VIEW STOCK_PRODUIT
AS SELECT * FROM Produit;

CREATE OR REPLACE VIEW STOCK_LIEU
AS SELECT * FROM Lieu;

CREATE OR REPLACE VIEW STOCK_DATE
AS SELECT * FROM CurrentDate;

CREATE OR REPLACE VIEW STOCK_MAGASIN
AS SELECT * FROM Magasin;