ALTER SESSION SET nls_date_format='YYYY/MM/DD';

-- Magasin - clear

INSERT INTO Magasin (idMagasin, nomMagasin, numeroMagasin, nbEmployees, gerantMagasin, SurfaceDuMagasin, SurfaceDeStockage, SurfaceDeVente, datePremiereOuverture, dateFermetureMagasin) VALUES (1, 'Montpellier-Odysseum', 157, 199, 'Jean-Michel Amoitier', 3900, 1000, 2600, TO_DATE('2008/09/01', 'YYYY/MM/DD'), NULL);

INSERT INTO Magasin (idMagasin, nomMagasin, numeroMagasin, nbEmployees, gerantMagasin, SurfaceDuMagasin, SurfaceDeStockage, SurfaceDeVente, datePremiereOuverture, dateFermetureMagasin) VALUES (2, 'Quetigny-Dijon', 82, 186, 'Jean-Michel Apeupres', 3500, 1000, 2200, TO_DATE('2009/02/11', 'YYYY/MM/DD'), NULL);

INSERT INTO Magasin (idMagasin, nomMagasin, numeroMagasin, nbEmployees, gerantMagasin, SurfaceDuMagasin, SurfaceDeStockage, SurfaceDeVente, datePremiereOuverture, dateFermetureMagasin) VALUES (3, 'Orange', 329, 198, 'Jean-Michel Padevoix', 3900, 1200, 2600, TO_DATE('2007/09/01', 'YYYY/MM/DD'), NULL);


-- CurrentDate - clear

INSERT INTO CurrentDate (idDate, jour, jourSemaine, jourAnnee, semaine, semaineDuMois, mois, trimestre, annee, saison, vacances) VALUES (1, 29, 1, 302, 43, 5, 10, 4, 2018, 'Automne', 1);

INSERT INTO CurrentDate (idDate, jour, jourSemaine, jourAnnee, semaine, semaineDuMois, mois, trimestre, annee, saison, vacances) VALUES (2, 30, 2, 303, 43, 5, 10, 4, 2018, 'Automne', 1);

INSERT INTO CurrentDate (idDate, jour, jourSemaine, jourAnnee, semaine, semaineDuMois, mois, trimestre, annee, saison, vacances) VALUES (3, 31, 3, 304, 43, 5, 10, 4, 2018, 'Automne', 1);

INSERT INTO CurrentDate (idDate, jour, jourSemaine, jourAnnee, semaine, semaineDuMois, mois, trimestre, annee, saison, vacances) VALUES (4, 1, 4, 305, 43, 1, 11, 4, 2018, 'Automne', 1);

INSERT INTO CurrentDate (idDate, jour, jourSemaine, jourAnnee, semaine, semaineDuMois, mois, trimestre, annee, saison, vacances) VALUES (5, 2, 5, 306, 43, 1, 11, 4, 2018, 'Automne', 1);

INSERT INTO CurrentDate (idDate, jour, jourSemaine, jourAnnee, semaine, semaineDuMois, mois, trimestre, annee, saison, vacances) VALUES (6, 3, 6, 307, 43, 1, 11, 4, 2018, 'Automne', 1);

INSERT INTO CurrentDate (idDate, jour, jourSemaine, jourAnnee, semaine, semaineDuMois, mois, trimestre, annee, saison, vacances) VALUES (7, 14, 4, 73, 11, 3, 3, 1, 2019, 'Printemps', 0);

INSERT INTO CurrentDate (idDate, jour, jourSemaine, jourAnnee, semaine, semaineDuMois, mois, trimestre, annee, saison, vacances) VALUES (8, 15, 5, 74, 11, 3, 3, 1, 2019, 'Printemps', 0);


-- Lieu - clear 

INSERT INTO Lieu (idLieu, pays, departement, region, ville, quartier, rue, codeCommune, zipCode, cedex) VALUES (1, 'France', 'Herault', 'Occitanie', 'Montpellier', 'Millenaire', '1072 Avenue Georges Melies', 34172, 34000, 4);

INSERT INTO Lieu (idLieu, pays, departement, region, ville, quartier, rue, codeCommune, zipCode, cedex) VALUES (2, 'France', 'Cote d-Or', 'Bourgogne-Franche-Comte', 'Quetigny', 'ZI Quetigny', 'Rue Champeau', 21515, 21800, NULL);

INSERT INTO Lieu (idLieu, pays, departement, region, ville, quartier, rue, codeCommune, zipCode, cedex) VALUES (3, 'France', 'Vaucluse', 'Provence-Alpes-Cote-D-Azur', 'Orange', 'ZA Porte Sud', 'ZA Porte Sud', 84087, 84100, NULL);


-- Temps - clear

INSERT INTO Temps (idTemps, heure, minute, seconde, fuseauHoraire, indicateurAMPM, momentJournee) VALUES (1, 2, 40, 29, '+01', 'PM', 'Apres-Midi');

INSERT INTO Temps (idTemps, heure, minute, seconde, fuseauHoraire, indicateurAMPM, momentJournee) VALUES (2, 3, 16, 47, '+01', 'PM', 'Apres-Midi');

INSERT INTO Temps (idTemps, heure, minute, seconde, fuseauHoraire, indicateurAMPM, momentJournee) VALUES (3, 11, 48, 56, '+01', 'AM', 'Matin');

INSERT INTO Temps (idTemps, heure, minute, seconde, fuseauHoraire, indicateurAMPM, momentJournee) VALUES (4, 3, 33, 9, '+01', 'PM', 'Apres-Midi');


-- Produit - clear

INSERT INTO Produit (idProduit, description, prixHT, prixAchatFournisseur, tauxTaxe, prixTTC, marge, marque, categorie, sousCategorie, typeConditionnement, tailleConditionnement, poidsNet, hauteur, largeur, profondeur) VALUES (1, 'Expert TR 960', 80.00, 55.59, 20, 96, 24.41, 'ARTENGO', 'Tennis', 'Raquette Tennis Adulte', 'libre', NULL, 0.360, 86.5, 35, 2.8);

INSERT INTO Produit (idProduit, description, prixHT, prixAchatFournisseur, tauxTaxe, prixTTC, marge, marque, categorie, sousCategorie, typeConditionnement, tailleConditionnement, poidsNet, hauteur, largeur, profondeur) VALUES (2, 'Kiprun Trail Jaune taille 40', 50.00, 29.9, 20, 60, 20.10, 'KALENJI', 'Trail', 'Chaussure de Trail Homme', 'libre', NULL, 0.390, 7.5, 9.8, 26.5);

INSERT INTO Produit (idProduit, description, prixHT, prixAchatFournisseur, tauxTaxe, prixTTC, marge, marque, categorie, sousCategorie, typeConditionnement, tailleConditionnement, poidsNet, hauteur, largeur, profondeur) VALUES (3, 'Kiprun Trail Grise taille 39', 50, 28.55, 20, 60, 21.45, 'KALENJI', 'Trail', 'Chaussure de Trail Femme', 'libre', NULL, 0.38, 7.5, 9.8, 25.5);

INSERT INTO Produit (idProduit, description, prixHT, prixAchatFournisseur, tauxTaxe, prixTTC, marge, marque, categorie, sousCategorie, typeConditionnement, tailleConditionnement, poidsNet, hauteur, largeur, profondeur) VALUES (4, 'Survetement 560 Gym Bleu 10 ans', 4.8, 3.5, 20, 6, 1.3, 'DOMYOS', 'Gym', 'Survetement Fille', 'libre', NULL, 0.49, 56, 36, 15);


-- Client - clear

INSERT INTO Client (idClient, prenom, nom, adresse, ville, mail, dateProgrammeFidelite) VALUES (1, 'Michel', 'Bergeait', '55 Rue du Parvis', 'Grabels', 'michel.berger@mail.fr', '2009/08/11');

INSERT INTO Client (idClient, prenom, nom, adresse, ville, mail, dateProgrammeFidelite) VALUES (2, NULL, NULL, NULL, 'Quetigny', 'david.goodenough@mail.fr', NULL);

INSERT INTO Client (idClient, prenom, nom, adresse, ville, mail, dateProgrammeFidelite) VALUES (3, 'Henri', 'Lechanteur', '256 rue des oiseaux', 'Orange', 'henri.cestmoi@mail.fr', '2014/03/15');

INSERT INTO Client (idClient, prenom, nom, adresse, ville, mail, dateProgrammeFidelite) VALUES (4, 'David', 'Goodenought', '40 quai des Carrosses', 'Montpellier', 'matuture34@mail.fr', '2018/10/30');


-- Stock - clear

INSERT INTO Stock (idProduit, idLieu, idDate, idMagasin, quantiteEnRayon, quantiteEntrepot, quantiteTotale) VALUES (1, 1, 4, 1, 15, 40, 55);

INSERT INTO Stock (idProduit, idLieu, idDate, idMagasin, quantiteEnRayon, quantiteEntrepot, quantiteTotale) VALUES (2, 1, 4, 1, 6, 50, 56);

INSERT INTO Stock (idProduit, idLieu, idDate, idMagasin, quantiteEnRayon, quantiteEntrepot, quantiteTotale) VALUES (3, 1, 4, 1, 4, 30, 34);

INSERT INTO Stock (idProduit, idLieu, idDate, idMagasin, quantiteEnRayon, quantiteEntrepot, quantiteTotale) VALUES (1, 2, 5, 2, 8, 52, 60);

INSERT INTO Stock (idProduit, idLieu, idDate, idMagasin, quantiteEnRayon, quantiteEntrepot, quantiteTotale) VALUES (3, 2, 5, 2, 8, 30, 38);

INSERT INTO Stock (idProduit, idLieu, idDate, idMagasin, quantiteEnRayon, quantiteEntrepot, quantiteTotale) VALUES (4, 2, 5, 2, 26, 150, 176);

INSERT INTO Stock (idProduit, idLieu, idDate, idMagasin, quantiteEnRayon, quantiteEntrepot, quantiteTotale) VALUES (2, 3, 6, 3, 2, 18, 20);

INSERT INTO Stock (idProduit, idLieu, idDate, idMagasin, quantiteEnRayon, quantiteEntrepot, quantiteTotale) VALUES (3, 3, 6, 3, 8, 36, 44);

INSERT INTO Stock (idProduit, idLieu, idDate, idMagasin, quantiteEnRayon, quantiteEntrepot, quantiteTotale) VALUES (4, 3, 6, 3, 15, 0, 15);


-- Vente - clear

INSERT INTO Vente (idClient, idProduit, idTemps, idLieu, idDate, idMagasin, numeroTransaction, quantiteVendue, prixVente) VALUES (1, 1, 1, 1, 1, 1, 2, 2, 184.00);

INSERT INTO Vente (idClient, idProduit, idTemps, idLieu, idDate, idMagasin, numeroTransaction, quantiteVendue, prixVente) VALUES (2, 2, 2, 2, 1, 2, 2, 1, 60.00);

INSERT INTO Vente (idClient, idProduit, idTemps, idLieu, idDate, idMagasin, numeroTransaction, quantiteVendue, prixVente) VALUES (3, 3, 3, 3, 2, 3, 3, 1, 60.00);

INSERT INTO Vente (idClient, idProduit, idTemps, idLieu, idDate, idMagasin, numeroTransaction, quantiteVendue, prixVente) VALUES (4, 4, 4, 1, 3, 1, 4, 3, 18.00);

INSERT INTO Vente (idClient, idProduit, idTemps, idLieu, idDate, idMagasin, numeroTransaction, quantiteVendue, prixVente) VALUES (1, 2, 2, 1, 7, 1, 5, 1, 60.00);

INSERT INTO Vente (idClient, idProduit, idTemps, idLieu, idDate, idMagasin, numeroTransaction, quantiteVendue, prixVente) VALUES (2, 1, 3, 2, 8, 2, 6, 1, 92.00);