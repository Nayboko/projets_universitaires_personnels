-- Magasin
ALTER TABLE Magasin ADD PRIMARY KEY(idMagasin);

-- CurrentDate
ALTER TABLE CurrentDate ADD PRIMARY KEY(idDate);

-- Lieu
ALTER TABLE Lieu ADD PRIMARY KEY(idLieu);

-- Temps
ALTER TABLE Temps ADD PRIMARY KEY(idTemps);

-- Produit
ALTER TABLE Produit ADD PRIMARY KEY(idProduit);

-- Client
ALTER TABLE Client ADD PRIMARY KEY(idClient);

-- Stock
ALTER TABLE Stock ADD FOREIGN KEY(idMagasin) REFERENCES Magasin(idMagasin);
ALTER TABLE Stock ADD FOREIGN KEY(idDate) REFERENCES CurrentDate(idDate);
ALTER TABLE Stock ADD FOREIGN KEY(idLieu) REFERENCES Lieu(idLieu);
ALTER TABLE Stock ADD FOREIGN KEY(idProduit) REFERENCES Produit(idProduit);
ALTER TABLE Stock ADD PRIMARY KEY(idMagasin, idDate, idLieu, idProduit);

-- Vente
ALTER TABLE Vente ADD FOREIGN KEY(idMagasin) REFERENCES Magasin(idMagasin);
ALTER TABLE Vente ADD FOREIGN KEY(idDate) REFERENCES CurrentDate(idDate);
ALTER TABLE Vente ADD FOREIGN KEY(idLieu) REFERENCES Lieu(idLieu);
ALTER TABLE Vente ADD FOREIGN KEY(idTemps) REFERENCES Temps(idTemps);
ALTER TABLE Vente ADD FOREIGN KEY(idProduit) REFERENCES Produit(idProduit);
ALTER TABLE Vente ADD FOREIGN KEY(idClient) REFERENCES Client(idClient);
ALTER TABLE Vente ADD PRIMARY KEY(idMagasin, idDate, idLieu, idTemps, idProduit, idClient);
