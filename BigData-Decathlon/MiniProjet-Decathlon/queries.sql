SET PAGESIZE 1000
SET LINESIZE 1000
SET TIMING ON

SELECT cd.saison, m.idMagasin, AVG(v.prixVente)
FROM Vente v, Magasin m, CurrentDate cd
WHERE v.idDate = cd.idDate
AND v.idMagasin = m.idMagasin
GROUP BY(cd.saison, m.idMagasin)
ORDER BY cd.saison;

SELECT cd.saison, m.idMagasin, AVG(v.prixVente)
FROM Vente v, VENTE_MAGASIN m, VENTE_DATE cd
WHERE v.idDate = cd.idDate
AND v.idMagasin = m.idMagasin
GROUP BY(cd.saison, m.idMagasin)
ORDER BY cd.saison;

SELECT m.idMagasin, p.categorie, v.quantiteVendue AS "Quantite"
FROM Vente v, VENTE_MAGASIN m, VENTE_PRODUIT p
WHERE v.idProduit = p.idProduit 
AND v.idMagasin = m.idMagasin
GROUP BY (m.idMagasin, p.categorie, v.quantiteVendue)
ORDER BY m.idMagasin;

SELECT p.categorie, SUM(v.prixVente)
FROM Vente v, VENTE_PRODUIT p
WHERE v.idProduit = p.idProduit 
GROUP BY (p.categorie, v.prixVente)
ORDER BY p.categorie;

SELECT t.indicateurAMPM, COUNT(v.prixVente)
FROM Vente v, Temps t
WHERE v.idTemps = t.idTemps
GROUP BY t.indicateurAMPM
ORDER BY COUNT(v.prixVente);

SELECT COUNT(p.idProduit) , v.idMagasin
FROM VENTE_PRODUIT p, Vente v, VENTE_DATE d
WHERE p.idProduit = v.idProduit
AND v.idDate = d.idDate
AND d.annee = 2018
GROUP BY v.idMagasin
ORDER BY COUNT(p.idProduit) DESC;

SELECT m.idMagasin, p.description, SUM(s.quantiteTotale)
FROM STOCK_MAGASIN m, STOCK_PRODUIT p, Stock s
WHERE m.idMagasin = s.idMagasin
AND p.idProduit = s.idProduit
GROUP BY m.idMagasin, p.description;

SELECT m.idMagasin, p.idProduit, p.description, s.quantiteEntrepot
FROM STOCK_PRODUIT p, STOCK_MAGASIN m , Stock s
WHERE p.idProduit = s.idProduit
AND m.idMagasin = s.idMagasin
AND s.quantiteEntrepot < 50
GROUP BY m.idMagasin, p.idProduit, p.description, s.quantiteEntrepot;


SELECT m.idMagasin, p.idProduit, p.description, s.quantiteEntrepot
FROM STOCK_PRODUIT p, STOCK_MAGASIN m , Stock s
WHERE p.idProduit = s.idProduit
AND m.idMagasin = s.idMagasin
AND s.quantiteEntrepot > 50
GROUP BY m.idMagasin, p.idProduit, p.description, s.quantiteEntrepot;

SELECT m.nomMagasin, SUM(s.quantiteTotale)
FROM STOCK_MAGASIN m, STOCK_DATE d, Stock s
WHERE m.idMagasin = s.idMagasin
AND d.idDate = s.idDate
AND d.jour <= 5
AND d.mois <= 11
AND d.annee <= 2018
GROUP BY m.nomMagasin
ORDER BY SUM(s.quantiteTotale) DESC;


SELECT  m.idMagasin, p.idProduit, p.description, ((p.hauteur * p.largeur * p.profondeur) * s.quantiteEntrepot) / 1000000 AS "Volume"
FROM STOCK_PRODUIT p, STOCK_MAGASIN m, Stock s
WHERE m.idMagasin = s.idMagasin
AND p.idProduit = s.idProduit
GROUP BY m.idMagasin, p.idProduit, p.description, p.hauteur,  p.largeur,  p.profondeur,  s.quantiteEntrepot
ORDER BY ((p.hauteur * p.largeur * p.profondeur) * s.quantiteEntrepot) DESC;

-- BUILD IMMEDIATE
-- REFRESH FAST START WITH SYSDATE NEXT SYSDATE + 1

CREATE MATERIALIZED VIEW MV_SPDM
AS
	SELECT m.idMagasin, m.nomMagasin, p.idProduit, p.description, p.hauteur, p.largeur, p.profondeur, s.quantiteEntrepot, s.quantiteTotale, d.idDate, d.jour, d.mois, d.annee
	FROM Magasin m, Produit p, CurrentDate d, Stock s 
	WHERE s.idMagasin = m.idMagasin
	AND s.idProduit = p.idProduit
	AND s.idDate = d.idDate;

CREATE MATERIALIZED VIEW MV_SPDM
AS
	SELECT m.idMagasin, p.catergorie, t.idTemps, t.indicateurAMPM, d.idDate, d.annee, d.saison, v.prixVente, v.quantiteVendue
	FROM Produit p, CurrentDate d, Magasin m, Temps t
	WHERE v.idMagasin = m.idMagasin
	AND v.idProduit = p.idProduit
	AND v.idDate = d.idDate
	AND v.idTemps = t.idTemps;
