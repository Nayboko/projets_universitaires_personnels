# Développement d'un Datawarehouse pour l'entreprise Decathlon

## Contexte
<!--Expliciter les objectifs du mini-projet et présentation succincte de l'entreprise Decathlon-->

## Analyse des besoins

### 1. Opérations importantes du sujet
* Développement et amélioration les marques distributeurs Decathlon
* Amélioration du ciblage marketing
* Optimisation du rayonnage des magasins
* Augmentation du volume des ventes
* Optimisation de la gestion des stocks
* Amélioration du système de livraison 
* Optimisation de la gestion des ressources humaines

### 2. Traitements possibles
* Développement et amélioration les marques distributeurs Decathlon
	* Quelles sont les produits de marque distributeur Decathlon qui se vendent moins bien que les autres marques proposées par les magasins?
	* Quels sont les types de produits que les marques distributeurs Decathlon ne possèdent pas et qui se vendent le plus?
	* Quels sont les produits des marques distributeurs "Decathlon" avec le plus de retours au Service Après-Vente?

* Amélioration du ciblage marketing
	* Quels sont les types d'articles les plus vendus sur une période donnée par des clients enregistrés au programme fidélité ?
	* Quels sont les sports les plus pratiqués par magasin en fonction des achats ?
	* Quel est le montant total d'achats par membre du programme fidélité ?

* Optimisation du rayonnage des magasins
	* Quels sont les équipements de sport qui rapportent le moins ?
	* Quels sont les types de produits les plus souvent achetés ensembles ?
	* Quels sont les produits les plus vendus par saison et par magasin ?

* Augmentation du volume des ventes
	* Chiffre d'affaire moyen en fonction de la saison par magasin ?
	* Quel est, en pourcentage, la proportion de type de produits vendus et par magasin ?	
	* Quels sont les types de produits vendus par saison et par magasin ?

* Optimisation de la gestion des stocks
	* Quels sont les séries de produits les moins vendues par magasin ?
	* Quelle est la quantité restante pour chaque produit par magasin ?
	* Quels sont les produits dont la quantité est inférieure à un certain seuil ?

* Amélioration du système de livraison 
	* Quelles sont les zones géographiques où il y a le plus de livraison ?
	* Quelle est la moyenne de livraison par jour et par ville ?
	* Quel est le nombre de retrait en magasin par semaine ?

* Optimisation de la gestion des ressources humaines
	* Quelle est la proportion de "retrait en magasin" suite à une commande en ligne par magasin sur une période donnée ?
	* Quel est le nombre de produits vendu par sport, par tranche horaire et par magasin ?
	* Quels sont les types de produits vendus par saison et par magasin ?


### 3. Ordonnancement des actions par ordre de priorité

1. Augmentation du volume des ventes
2. Optimisation de la gestion des stocks
3. Amélioration du ciblage marketing
4. Développement et amélioration les marques distributeurs Decathlon
5. Optimisation du rayonnage des magasins
6. Amélioration du système de livraison 
7. Optimisation de la gestion des ressources humaines

Nous pensons, hypothétiquement, qu'à travers la conception d'un Datawarehouse, l'entreprise Decathlon ne va pas se focaliser uniquement sur la réduction éventuelle des coûts mais sur l'optimisation des dépenses en incluant leurs fournisseurs. Elle va ainsi chercher à agir de telle sorte qu'elle puisse continuer son développement stratégique.

## Conception de l'Entrepôt de Données

### 4. Opérations les plus importantes à analyser

Les opérations les plus importantes à analyser seront donc l'augmentation du volume des ventes et donc à priori augmentation du chiffre d'affaire ainsi que l'optimisation de la gestion des stocks.

### 5. Conception des Data-Marts

#### a. Augmentation du volume des ventes

La base de faits pour ce Data-Mart se nomme 'Vente' et sera de type transactionnelle avec des mesures additives.


<!-- Voir diagram-ventes -->

#### b. Optimisation de la gestion des stocks

La base de fait de ce Data-Mart, issu d'un snapshot périodique, se nomme 'Stock' et la mesure sera semi-additive.
Elle est semi-additive car l'on va additionner selon certaines dimensions seulement et pas toutes.

<!-- Voir diagram-stocks -->

### 6. Définition des dimensions nécessaires pour chaque opération

**Augmentation du volume des ventes**
Vente(idDate {FK}, idTemps {FK}, idProduit {FK}, idMagasin {FK}, idClient {FK}, numeroTransaction #, quantiteVendue, prixVente)

Client(idClient, prenom, nom, adresse, ville, mail, dateProgrammeFidelité)
Date(idDate, jour, jourSemaine, jourAnnee, semaine, semaineDuMois, mois, trimestre, annee, saison, vacances?)
Temps(idTemps, heure, minute, seconde, fuseauHoraire, indicateurAM/PM, momentJournee)
Magasin(idMagasin, nomMagasin, numeroMagasin, villeMagasin, departementMagasin, regionMagasin, gerantMagasin)
Produit(idProduit, description, prixHT, prixAchatFournisseur, tauxTVA, marque, categorie, sousCategorie, typeConditionnement, tailleConditionnement, poidsNet, hauteur, largeur, profondeur)

**Optimisation de la gestion des stocks**
Stock(idDate{FK}, idProduit{FK}, idMagasin{FK}, idLivraison{FK}, quantitéReçue, quantitéEnRayon, quantitéTotale, quantitéAvantLivraison, seuilMinimal)

Date(idDate, jour, jourSemaine, jourAnnee, semaine, semaineDuMois, mois, trimestre, annee, saison, vacances?)
Temps(idTemps, heure, minute, seconde, fuseauHoraire, indicateurAM/PM, momentJournee)
Magasin(idMagasin, nomMagasin, numeroMagasin, villeMagasin, departementMagasin, regionMagasin, gerantMagasin)
Produit(idProduit, description, prixHT, prixAchatFournisseur, tauxTVA, marque, categorie, sousCategorie, typeConditionnement, tailleConditionnement, poidsNet, hauteur, largeur, profondeur)
Livraison(idLivraison, nomCompagnie, identifiantLivreur, nomLivreur, numeroDeCommande, capacitéVehicule, typeVehicule, immatVehicule, coûtLivraison)

### 7. Le modèle est-il adapté aux traitements de ces actions?

* Augmentation du volume des ventes
	* Chiffre d'affaire moyen en fonction de la saison par magasin ? OUI

	* Quel est, en pourcentage, la proportion de type de produits vendus et par magasin ? OUI

	* Quel est le total des ventes par magasin et par type de produit ? OUI

* Optimisation de la gestion des stocks
	* Quels sont les séries de produits les moins vendues par magasin? OUI

	* Quelle est la quantité restante pour chaque produit par magasin ? OUI

	* Quels sont les produits dont la quantité est inférieure à un certain seuil ? OUI

### 8. Instanciation du modèle

#### a. Augmentation du volume des ventes

Date:
(1, 1, 'lundi', 275, 1, 40, 'octobre', 4, 2018, 'automne', false)
(2, 2, 'mardi', 276, 1, 40, 'octobre', 4, 2018, 'automne', false)
(3, 3, 'mercredi', 277, 1, 40, 'octobre', 4, 2018, 'automne', false)

Temps:
(1, 9, 45, 17, '+01', 'AM', 'matin')
(2, 4, 12, 01, '+01', 'PM', 'après-midi')
(3, 1, 08, 34, '+01', 'PM', 'midi')

Magasin:
(1, 'Decathlon Odysseum', 157, 'Montpellier', 'Herault', 'Occitanie', 'Jean-Michel Amoitier')
(2, 'Decathlon Dijon-Quetigny', 82, 'Quétigny', 'Cote-d-Or', 'Bourgogne-Franche-Comte', 'Jean-Michel Padevoix')
(3, 'Decathlon Orange', 329, 'Orange', 'Vaucluse', 'Provence-Alpes-Cote-D'Azur', 'Jean-Michel Apeuprès')

Produit:
(1, 'Expert TR 960', 80.0, 55.0, 0.2, 'ARTENGO', 'Raquette de tennis', 'Raquette de tennis adulte', 'libre', null, 0.3,  96.5, 35.0, 2.8)
(2, 'Kiprun Trail Jaune taille 41', 50.0, 25.0, 0.2, 'KALENJI', 'Chaussure de Trail', 'Trail femme', 'libre', null, 0.3, 17.0, 15.0, 38.0)


Client:
(1, 'Cédric', 'PLUTA', '27 rue Pierre Antonini', 'Montpellier', 'cedric.pluta@mail.fr', '08-07-2017')
(2, 'Elodie', 'TRIBOUT', '27 rue Pierre Antonini', 'Montpellier', 'elodie.tribout@mail.fr', '18-04-2018')
(1, 'Natdanaï', 'BESSON', '256 rue des pixels noirs', 'Quétigny', 'cedric.pluta@mail.fr', '08-07-2017')

Vente:
Vente(idDate {FK}, idTemps {FK}, idProduit {FK}, idMagasin {FK}, idClient {FK}, numeroTransaction #, quantiteVendue, prixVente)

(1, 1, 1, 1, 2, 1, 2, 192)




#### b.Optimisation de la gestion des stocks