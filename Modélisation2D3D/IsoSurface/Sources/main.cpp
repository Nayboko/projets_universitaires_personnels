#include <iostream>
#include "utils.hpp"

using namespace std;

int main(int argc, char** argv, char** env){
	if(argc != 10){
		cerr << "Usage : " << argv[0] << "<imageFile> <dimX> <dimY> <dimZ> <sizeXVoxel> <sizeYVoxel> <sizeZVoxel> <resultFile> <threshold>" << endl;
		exit(EXIT_FAILURE);
	}

	unsigned int dimX, dimY, dimZ, taille;
	float sizeXVoxel, sizeYVoxel, sizeZVoxel;
	char imageFile[250], resultFile[250];
	unsigned short threshold;

	sscanf (argv[1],"%s",imageFile) ;
   	sscanf (argv[2],"%u",&dimX);
   	sscanf (argv[3],"%u",&dimY);
   	sscanf (argv[4],"%u",&dimZ);
   	sscanf (argv[5],"%f",&sizeXVoxel);
   	sscanf (argv[6],"%f",&sizeYVoxel);
   	sscanf (argv[7],"%f",&sizeZVoxel);
   	sscanf (argv[8],"%s",resultFile);
   	sscanf (argv[9],"%hu",&threshold);
   	taille = dimX * dimY * dimZ;

   	

   	unsigned short*** ImgOut = InitMatrice(dimX, dimY, dimZ);
   	if(ImgOut == NULL) exit(EXIT_FAILURE);

	cout << "Chargement de l'image " << imageFile << " de dimension " 
		 << dimX << " x " << dimY << " x " << dimZ << endl;
	cout << "Nombre de voxels : " << taille << endl;

	if(SetMatrice(ImgOut, imageFile, dimX, dimY, dimZ) == ERROR){
		exit(EXIT_FAILURE);
	}

	cout << "minValue : " << minValue(ImgOut, dimX, dimY, dimZ) << endl;
	cout << "maxValue : " << maxValue(ImgOut, dimX, dimY, dimZ) << endl;

	ComputeIsoSurface(ImgOut, dimX, dimY, dimZ, sizeXVoxel, sizeYVoxel, sizeZVoxel, threshold, resultFile);
	
	FreeTab(ImgOut, dimY, dimZ);
	
	return 0;
}