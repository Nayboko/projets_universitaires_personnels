#include "utils.hpp"

using namespace std;

unsigned int ID_VERTICE = 0;

unsigned short*** InitMatrice(unsigned int dimX, unsigned int dimY, unsigned int dimZ){
	if(dimX <= 0 || dimY <= 0 | dimZ <= 0){
   		std::cerr << "ERREUR : Les dimensions de l'images doivent être strictement positives" << std::endl;
   		return NULL;
   	}

	unsigned short*** Matrice = new unsigned short**[dimZ];
   	for(unsigned int z = 0; z < dimZ; z++){
   		Matrice[z] = new unsigned short*[dimY];
   		for(unsigned int y = 0; y < dimY; y++){
   			Matrice[z][y] =  new unsigned short[dimX];
   		}
   	}
   	return Matrice;
}


int SetMatrice(unsigned short*** Matrice, char* NomImgLue, unsigned int dimX, unsigned int dimY, unsigned int dimZ){
	FILE* file;
	if((file = fopen(NomImgLue, "rb")) != NULL){
		std::cout << "# Ouverture du fichier réussie." << std::endl;
		size_t res = 0;
		unsigned short value = 0;
		unsigned int x, y, z; x = y = z = 0;

		while(true){
			if((res = fread(&value, sizeof(unsigned short), 1, file)) == 1){
				value = (value >> 8) | (value << 8);
				Matrice[z][y][x] = value;
				x++;
				// Si on a atteint le x max de la ligne y, on change de ligne
				if(x == dimX) {
					x = 0;
					y++;
					// Si on a atteint le y max de la couche z, on change de couche
					if(y == dimY) {
						y = 0;
						z++;
						// Si on a lu l'ensemble des couches z de l'image, on a terminé la lecture
						if(z == dimZ) {
							std::cout << "# Fin de la lecture du fichier." << std::endl;
							break;
						}
					}
				}
			}else{
				std::cerr << "Erreur lors de la lecture du fichier." << std::endl;
				fclose(file);
				return ERROR;
			}
		}
	}else{
		std::cerr << "Pas d'accès en lecture sur l'image " << NomImgLue << "." << std::endl;
		return ERROR;
	}
}


const unsigned short getValue(unsigned short*** ImgOut, unsigned int dimX, unsigned int dimY, unsigned int dimZ, unsigned int x, unsigned int y, unsigned int z){
	if(x > dimX || x < 0){
		std::cerr << "Erreur : x doit être compris entre 0 et " << dimX << "." << std::endl;
		return ERROR;
	}else if(y > dimY || y < 0){
		std::cerr << "Erreur : y doit être compris entre 0 et " << dimY << "." << std::endl;	
		return ERROR;
	}else if(z > dimZ || z < 0){
		std::cerr << "Erreur : z doit être compris entre 0 et " << dimZ << "." << std::endl;	
		return ERROR;
	}
	return ImgOut[z][dimY - y - 1][x];
}

const unsigned short minValue(unsigned short *** ImgOut, unsigned int dimX, unsigned int dimY, unsigned int dimZ){
  unsigned short minValue = ImgOut[0][0][0];
  for(unsigned int x = 0; x < dimX; ++x) {
	for(unsigned int y = 0; y < dimY; ++y) {
	  for(unsigned int z = 0; z < dimZ; ++z) {
		unsigned short value = getValue(ImgOut,dimX, dimY, dimZ, x, y, z);
		if(value < minValue) {
		  minValue = value;
		}
	  }
	}
  }
  return minValue;
}

const unsigned short maxValue(unsigned short *** ImgOut, unsigned int dimX, unsigned int dimY, unsigned int dimZ){
  unsigned short maxValue = ImgOut[0][0][0];
  for(unsigned int x = 0; x < dimX; ++x) {
	for(unsigned int y = 0; y < dimY; ++y) {
	  for(unsigned int z = 0; z < dimZ; ++z) {
		unsigned short value = getValue(ImgOut,dimX, dimY, dimZ, x, y, z);
		if(value > maxValue) {
		  maxValue = value;
		}
	  }
	}
  }
  return maxValue;
}

unsigned short*** VolumeRendering(unsigned short*** ImgIn,unsigned int dimX, unsigned int dimY, unsigned int dimZ, unsigned int visuAxis, unsigned int visuMode){
	if(dimX <= 0){
		std::cerr << "Erreur : dimX doit être strictement supérieur à 0." << std::endl;
		return NULL;
	}else if(dimY <= 0){
		std::cerr << "Erreur : dimY doit être strictement supérieur à 0." << std::endl;	
		return NULL;
	}else if(dimZ <= 0){
		std::cerr << "Erreur : dimZ doit être strictement supérieur à 0." << std::endl;	
		return NULL;
	}

	if(visuAxis == 0){
		std::vector<unsigned short> pixelsValues;
		unsigned short p = 0; long int somme = 0;
		unsigned short*** ImgOut = new unsigned short**[1];
		for(unsigned int z = 0; z < dimZ; z++){
			ImgOut[z] = new unsigned short*[dimY];
			for(unsigned int y = 0; y < dimY; y++){
				ImgOut[z][y] =  new unsigned short[dimZ];
			}
		}

		for(unsigned int y = 0; y < dimY; y++){
			for(unsigned int z = 0; z < dimZ; z++){
				pixelsValues.clear();
				for(unsigned int x = 0; x < dimX; x++){
					pixelsValues.push_back(ImgIn[z][y][x]);
				}

				// Maximum Intensity Projection
				if(visuMode == 0){
					p = *std::max(pixelsValues.begin(), pixelsValues.end());
					break;
				}
				// Average Intensity Projection
				else if(visuMode == 1){
					for(unsigned short x : pixelsValues) somme += x;
					somme /= dimX; p = somme;
					break;
				}
				// Minimum Intensity Projection
				else if(visuMode == 2){
					p = *std::min(pixelsValues.begin(), pixelsValues.end());
					break;

				}else{
					std::cerr << "Erreur : Valeur de visuMode inconnue." << std::endl << "Usage : visuMode : 0 -> MIP, 1 -> AIP, 2 -> MinIP" << std::endl;
					return NULL;	
				}
				ImgOut[0][y][z] = p;
			}
		}
		return ImgOut;

	}else if(visuAxis == 1){
		std::vector<unsigned short> pixelsValues;
		unsigned short p = 0; long int somme = 0;
		unsigned short*** ImgOut = new unsigned short**[1];
		for(unsigned int z = 0; z < dimZ; z++){
			ImgOut[z] = new unsigned short*[dimZ];
			for(unsigned int y = 0; y < dimY; y++){
				ImgOut[z][y] =  new unsigned short[dimX];
			}
		}

		for(unsigned int z = 0; z < dimZ; z++){
			for(unsigned int x = 0; x < dimX; x++){
				pixelsValues.clear();
				for(unsigned int y = 0; y < dimY; x++){
					pixelsValues.push_back(ImgIn[z][y][x]);
				}

				// Maximum Intensity Projection
				if(visuMode == 0){
					p = *std::max(pixelsValues.begin(), pixelsValues.end());
					break;
				}
				// Average Intensity Projection
				else if(visuMode == 1){
					for(unsigned short x : pixelsValues) somme += x;
					somme /= dimX; p = somme;
					break;
				}
				// Minimum Intensity Projection
				else if(visuMode == 2){
					p = *std::min(pixelsValues.begin(), pixelsValues.end());
					break;

				}else{
					std::cerr << "Erreur : Valeur de visuMode inconnue." << std::endl << "Usage : visuMode : 0 -> MIP, 1 -> AIP, 2 -> MinIP" << std::endl;
					return NULL;	
				}
				ImgOut[0][z][x] = p;
			}
		}
		return ImgOut;
	}else if(visuAxis == 2){
		std::vector<unsigned short> pixelsValues;
		unsigned short p = 0; long int somme = 0;
		unsigned short*** ImgOut = new unsigned short**[1];
		for(unsigned int z = 0; z < dimZ; z++){
			ImgOut[z] = new unsigned short*[dimY];
			for(unsigned int y = 0; y < dimY; y++){
				ImgOut[z][y] =  new unsigned short[dimX];
			}
		}

		for(unsigned int y = 0; y < dimY; y++){
			for(unsigned int x = 0; x < dimX; x++){
				pixelsValues.clear();
				for(unsigned int z = 0; z < dimZ; x++){
					pixelsValues.push_back(ImgIn[z][y][x]);
				}

				// Maximum Intensity Projection
				if(visuMode == 0){
					p = *std::max(pixelsValues.begin(), pixelsValues.end());
					break;
				}
				// Average Intensity Projection
				else if(visuMode == 1){
					for(unsigned short x : pixelsValues) somme += x;
					somme /= dimX; p = somme;
					break;
				}
				// Minimum Intensity Projection
				else if(visuMode == 2){
					p = *std::min(pixelsValues.begin(), pixelsValues.end());
					break;

				}else{
					std::cerr << "Erreur : Valeur de visuMode inconnue." << std::endl << "Usage : visuMode : 0 -> MIP, 1 -> AIP, 2 -> MinIP" << std::endl;
					return NULL;	
				}
				ImgOut[0][y][x] = p;
			}
		}
		return ImgOut;
	}else{
		std::cerr << "Erreur : Valeur de visuAxis inconnue." << std::endl << "Usage : visuAxis : 0 -> x, 1 -> y, 2 -> z" << std::endl;
		return NULL;
	}
}


Vertice* CreateVertice(float x, float y, float z){
	Vertice* v = new Vertice;
	v->x = x;
	v->y = y;
	v->z = z;
	v->iD = ID_VERTICE;
	ID_VERTICE++;
	return v;
}

void PrintVertice(Vertice* v){
	std::cout << "Vertice " << v->iD << " : (" << v->x << ", " << v->y << ", " << v->z << ")" << std::endl;
}


void ComputeIsoSurface(unsigned short*** ImgOut, unsigned int dimX, unsigned int dimY, unsigned int dimZ, float sizeXVoxel, float sizeYVoxel, float sizeZVoxel, unsigned short threshold, char* resultFile){
	std::ofstream file(resultFile);
	if(file){
		file << "solid" << std::endl;
		// Parcours de tous les Voxels
		for(unsigned int z = 1; z < dimZ -1; z++){
			std::cout << " Génération de la couche # " << z << std::endl;
			for(unsigned int y = 1; y < dimY - 1; y++){
				for(unsigned int x = 1; x < dimX -1; x++){
					Vertice* v0 = CreateVertice((x-0.5) * sizeXVoxel, (y-0.5) * sizeYVoxel, (z-0.5) * sizeZVoxel);
					Vertice* v1 = CreateVertice((x+0.5) * sizeXVoxel, (y-0.5) * sizeYVoxel, (z-0.5) * sizeZVoxel);
					Vertice* v2 = CreateVertice((x+0.5) * sizeXVoxel, (y+0.5) * sizeYVoxel, (z-0.5) * sizeZVoxel);
					Vertice* v3 = CreateVertice((x-0.5) * sizeXVoxel, (y+0.5) * sizeYVoxel, (z-0.5) * sizeZVoxel);
					Vertice* v4 = CreateVertice((x-0.5) * sizeXVoxel, (y-0.5) * sizeYVoxel, (z+0.5) * sizeZVoxel);
					Vertice* v5 = CreateVertice((x+0.5) * sizeXVoxel, (y-0.5) * sizeYVoxel, (z+0.5) * sizeZVoxel);
					Vertice* v6 = CreateVertice((x+0.5) * sizeXVoxel, (y+0.5) * sizeYVoxel, (z+0.5) * sizeZVoxel);
					Vertice* v7 = CreateVertice((x-0.5) * sizeXVoxel, (y+0.5) * sizeYVoxel, (z+0.5) * sizeZVoxel);

					unsigned short VoxelValue = getValue(ImgOut, dimX, dimY, dimZ, x, y, z);
					if(VoxelValue >= threshold){
						// On réfléchit ici en 6-connexité, soit par face
						unsigned short Voxel0 = getValue(ImgOut, dimX, dimY, dimZ, x, y, z - 1); // Face basse
						unsigned short Voxel1 = getValue(ImgOut, dimX, dimY, dimZ, x, y, z + 1); // Face haute
						unsigned short Voxel2 = getValue(ImgOut, dimX, dimY, dimZ, x - 1, y, z); // Face gauche
						unsigned short Voxel3 = getValue(ImgOut, dimX, dimY, dimZ, x + 1, y, z); // Face droite
						unsigned short Voxel4 = getValue(ImgOut, dimX, dimY, dimZ, x, y - 1, z); // Face avant
						unsigned short Voxel5 = getValue(ImgOut, dimX, dimY, dimZ, x, y + 1, z); // Face arrière

						if(Voxel0 < threshold){
							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v0->x << " " << v0->y << " " << v0->z << std::endl;
							file << "vertex " << v1->x << " " << v1->y << " " << v1->z << std::endl;
							file << "vertex " << v3->x << " " << v3->y << " " << v3->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;

							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v3->x << " " << v3->y << " " << v3->z << std::endl;
							file << "vertex " << v1->x << " " << v1->y << " " << v1->z << std::endl;
							file << "vertex " << v2->x << " " << v2->y << " " << v2->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;
						}
						if(Voxel1 < threshold){
							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v4->x << " " << v4->y << " " << v4->z << std::endl;
							file << "vertex " << v5->x << " " << v5->y << " " << v5->z << std::endl;
							file << "vertex " << v7->x << " " << v7->y << " " << v7->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;

							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v7->x << " " << v7->y << " " << v7->z << std::endl;
							file << "vertex " << v5->x << " " << v5->y << " " << v5->z << std::endl;
							file << "vertex " << v6->x << " " << v6->y << " " << v6->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;
						}
						if(Voxel2 < threshold){
							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v3->x << " " << v3->y << " " << v3->z << std::endl;
							file << "vertex " << v0->x << " " << v0->y << " " << v0->z << std::endl;
							file << "vertex " << v7->x << " " << v7->y << " " << v7->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;

							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v7->x << " " << v7->y << " " << v7->z << std::endl;
							file << "vertex " << v3->x << " " << v3->y << " " << v3->z << std::endl;
							file << "vertex " << v4->x << " " << v4->y << " " << v4->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;
						}
						if(Voxel3 < threshold){
							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v1->x << " " << v1->y << " " << v1->z << std::endl;
							file << "vertex " << v2->x << " " << v2->y << " " << v2->z << std::endl;
							file << "vertex " << v5->x << " " << v5->y << " " << v5->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;

							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v5->x << " " << v5->y << " " << v5->z << std::endl;
							file << "vertex " << v2->x << " " << v2->y << " " << v2->z << std::endl;
							file << "vertex " << v6->x << " " << v6->y << " " << v6->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;
						}
						if(Voxel4 < threshold){
							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v0->x << " " << v0->y << " " << v0->z << std::endl;
							file << "vertex " << v1->x << " " << v1->y << " " << v1->z << std::endl;
							file << "vertex " << v4->x << " " << v4->y << " " << v4->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;

							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v4->x << " " << v4->y << " " << v4->z << std::endl;
							file << "vertex " << v1->x << " " << v1->y << " " << v1->z << std::endl;
							file << "vertex " << v5->x << " " << v5->y << " " << v5->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;
						}
						if(Voxel5 < threshold){
							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v2->x << " " << v2->y << " " << v2->z << std::endl;
							file << "vertex " << v3->x << " " << v3->y << " " << v3->z << std::endl;
							file << "vertex " << v6->x << " " << v6->y << " " << v6->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;

							file << "facet normal 0 0 0" << std::endl;
							file << "outer loop" << std::endl;
							file << "vertex " << v6->x << " " << v6->y << " " << v6->z << std::endl;
							file << "vertex " << v3->x << " " << v3->y << " " << v3->z << std::endl;
							file << "vertex " << v7->x << " " << v7->y << " " << v7->z << std::endl;
							file << "endloop" << std::endl;
							file << "endfacet" << std::endl;
						}
					}
				}
			}
		}
		file << "endsolid" << std::endl;
		file.close();
	}else{
		std::cerr << "Erreur dans l'ouverture du fichier " << resultFile << std::endl;
	}
	


}


void FreeTab (unsigned short*** ImgOut, int dimY, int dimZ){
	fprintf(stdout, "Libération de l'espace mémoire\n");
	for(int i = 0; i < dimZ; ++i){
		if(ImgOut[i] != NULL){
			for(int j = 0; j < dimY; ++j){
				free(ImgOut[i][j]);
			}
			free(ImgOut[i]);
		}
	}
	free(ImgOut);
}