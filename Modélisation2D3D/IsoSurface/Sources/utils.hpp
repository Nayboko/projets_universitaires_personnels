#ifndef UTILS_HPP
#define UTILS_HPP

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <fstream>

#define ERROR -1

typedef struct Vertice{
	unsigned int iD;
	float x;
	float y;
	float z;
}Vertice;

// Création d'un Vertice en initialisant ses coordonnées et en lui attribuant un indice
Vertice* CreateVertice(float x, float y, float z);

// Pour le DEBUG : Affichage des coordonnées et ID d'un vertice donné en paramètre
void PrintVertice(Vertice* v);


// Initialisation de la matrice 3D
unsigned short*** InitMatrice(unsigned int dimX, unsigned int dimY, unsigned int dimZ);

int SetMatrice(unsigned short*** Matrice, char* NomImgLue, unsigned int dimX, unsigned int dimY, unsigned int dimZ);

// Permet de récupérer la valeur d'un voxel de coordonnées (x,y,z)
const unsigned short getValue(unsigned short*** ImgOut, unsigned int dimX, unsigned int dimY, unsigned int dimZ, unsigned int x, unsigned int y, unsigned int z);

// Retourne la plus petite valeur présente dans l'image 3D
const unsigned short minValue(unsigned short *** ImgOut, unsigned int dimX, unsigned int dimY, unsigned int dimZ);

// Retourne la plus grande valeur présente dans l'image 3D
const unsigned short maxValue(unsigned short *** ImgOut, unsigned int dimX, unsigned int dimY, unsigned int dimZ);

/** 
 * @brief Retourne une image correspondant à un axe de coupe visuAxis et un mode de visualisation visuMode
 * @param visuAxis : 0 -> x, 1 -> y, 2 -> z
 * @param visuMode : 0 -> MIP, 1 -> AIP, 2 -> MinIP) 
 */
unsigned short*** VolumeRendering(unsigned short*** ImgIn,unsigned int dimX, unsigned int dimY, unsigned int dimZ, unsigned int visuAxis, unsigned int visuMode);

// Calcul l'iso-surface d'un fichier img donné en entrée et crée un fichier au format STL contenant les triangles du maillage
void ComputeIsoSurface(unsigned short*** ImgOut, unsigned int dimX, unsigned int dimY, unsigned int dimZ, float sizeXVoxel, float sizeYVoxel, float sizeZVoxel, unsigned short threshold, char* resultFile);


void FreeTab (unsigned short*** ImgOut, int dimY, int dimZ);

#endif