﻿# Nous voulons un cmake "récent" pour utiliser les dernières fonctionnalités
cmake_minimum_required(VERSION 3.0)

# Notre projet est étiqueté TP3_Fiorio
project(TP3_Fiorio)

# Crée des variables avec les fichiers à compiler
set(SRCS
    ../Sources/main.cpp
    ../Sources/Point.cpp
    ../Sources/Ply.cpp
    ../Sources/Edge.cpp
    ../Sources/pgm_basic_io.c
    )
    
set(HEADERS
    ../Sources/Point.hpp
    ../Sources.Ply.hpp
    ../Sources/Edge.hpp
    ../Sources/pgm_basic_io.h
    )

# Notre exécutable
add_executable(App ${SRCS} ${HEARDERS})

# Ajout des features lié à c++11
target_compile_features(App PUBLIC cxx_nullptr)