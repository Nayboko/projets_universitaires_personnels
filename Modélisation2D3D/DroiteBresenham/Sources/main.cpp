#include <iostream>
#include <cstdlib>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>

#include "Point.hpp"
#include "Edge.hpp"
#include "pgm_basic_io.c"

#define WIDTH 	1024
#define HEIGHT 	1024
#define ORIGINE Point(WIDTH/2 - 1, WIDTH/2 - 1)

using namespace std;

/* ## FONCTIONS ## */

// Initialise un tableau de char correspondant à une image
void initImg(unsigned char* img, unsigned int width, unsigned int height, unsigned int val);

// Convertit les valeurs de l'image en niveau de gris
void ConvertToGrayScale(unsigned char* img, unsigned int height, unsigned int width, unsigned int rayon);

// Ecriture du résultat dans l'image de sortie en nuance de gris
short int ResToPGM(char* ImgOutPath, unsigned char* ImgOut, unsigned int width, unsigned int height);

// Libère la mémoire du contenu d'un tableau t
template<typename T> void FreeTab(T& t, unsigned int size);

int CreateResultsDir();

unsigned char* DroiteDeBresenham(unsigned char* ImgOut, unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1, unsigned int color);


int main(int argc, char** argv, char** env){
	if(argc != 4){
		cerr << "Usage : " << argv[0] << " <n> <rayon> <fileOut>" << endl;
		exit(EXIT_FAILURE);
	}

	unsigned int n = atoi(argv[1]);
	unsigned int rayon = atoi(argv[2]);

	cerr << "rayon : " << rayon << endl;

	vector<Point*> PList = Point::randomizedPoints(ORIGINE, n, rayon);
	cerr << "Nombre de points :" << PList.size() << endl;

	vector<Edge*> EC = Edge::Graham(PList);
	
	cerr << "Taille de l'Enveloppe Convexe : " << EC.size() << endl;


	/* Sortie du Graham */

	CreateResultsDir();
	unsigned char* ImgOut = new unsigned char[HEIGHT * WIDTH];
	initImg(ImgOut, WIDTH, HEIGHT, 255);

	// Dessin des droites
	for(unsigned int i = 0; i < EC.size(); i++){
		Edge* e = EC.at(i);
		DroiteDeBresenham(ImgOut, e->getFrom()->getX(), e->getFrom()->getY(), e->getTo()->getX(), e->getTo()->getY(), 0);
	}

	// Dessin des points
	for(unsigned int i= 0; i < PList.size(); i++){
		Point* p = PList.at(i);
		ImgOut[(int)p->getY() * WIDTH + (int)p->getX()] = 0;
	}


	if(int res = ResToPGM(argv[3], ImgOut, WIDTH, HEIGHT) != 0){
		cerr << "ERREUR : L'image n'a pas pu être créee." << endl;
		exit(EXIT_FAILURE);
	}

	return 0;
}


/* ## DEFINITION DE FONCTIONS ## */

void initImg(unsigned char* img, unsigned int width, unsigned int height, unsigned int val){
	for (unsigned int i=0; i < height; i++){
		for (unsigned int j=0; j < width; j++){
			img[i*width+j] = 255;
		}
	}	
}

void ConvertToGrayScale(unsigned char* img, unsigned int height, unsigned int width, unsigned int rayon){
	for (unsigned int i = 0; i < height; i++){
		for (unsigned int j = 0; j < width; j++){
			img[i * width + j] = (double)img[i * width + j]/(double)rayon * 255;
		}	
	}
}

short int ResToPGM(char* ImgOutPath, unsigned char* ImgOut, unsigned int width, unsigned int height){
	string filename(ImgOutPath);
	string directory("../Results/");
	string path(directory + filename);

	char* cstr = new char[path.size() + 1]; 
	path.copy(cstr, path.size() + 1); cstr[path.size()] = '\0';

	FILE* fichier = fopen(cstr, "w");
	if(ecritPGM(fichier, ImgOut, width, height) == NULL){
		fclose(fichier);
		delete(cstr);
		FreeTab(ImgOut, width * height);
		return -1;
	}

// Libération des variables allouées dynamiquement

	fclose(fichier);
	delete(cstr);
	FreeTab(ImgOut, width * height);

	return 0;
}

template<typename T> void FreeTab(T& t, unsigned int size){
delete t;
}

int CreateResultsDir(){
	return mkdir("../Results", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}

unsigned char* DroiteDeBresenham(unsigned char* ImgOut, unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1, unsigned int color){
	int dx = x1 - x0; int dy = y1 - y0;
	unsigned int y = y0;
	unsigned int incrHor = 2 * dy; unsigned int incrDiag = 2 * (dx - dy);
	int e = (2 * dy) - dx;

	// Initialisation de l'image de sortie et du masque de chanfrein

	/* ## DROITE MONTANTE ##*/
	if(y1 <= y0 && x0 <= x1){
		for(unsigned x = x0; x < x1; ++x){
			ImgOut[y * WIDTH + x] = color;
			if(e >= 0){
				y--;
				e -= incrDiag;
			}else e-= incrHor;
		}	
	}/* ## DROITE DESCENDANTE ##*/
	else if(y1 >= y0 && x0 <= x1){
		for(unsigned x = x0; x < x1; ++x){
			ImgOut[y * WIDTH + x] = color;
			if(e >= 0){
				y++;
				e -= incrDiag;
			}else e+= incrHor;
		}	
	}

	return ImgOut;	
}