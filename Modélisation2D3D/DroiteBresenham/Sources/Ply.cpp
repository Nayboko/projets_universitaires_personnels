#include "Ply.hpp"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>

using namespace std;


Ply::Ply(){}

Ply::Ply(char* FO, std::vector<Point*> V){
	this->setFileOut(FO);
	this->setPoints(V);
	std::vector<Edge*> e = Edge::Graham(V);
	this->setEC(e);
}

Ply::Ply(char* FO, std::vector<Point*> V, std::vector<Edge*> EC){
	this->setFileOut(FO);
	this->setPoints(V);
	this->setEC(EC);
}

Ply::~Ply(){}

char* Ply::getFileOut()const{
	return this->FileOut;
}

std::vector<Edge*> Ply::getEC(){
	return this->EnvConvexe;
}

std::vector<Point*> Ply::getPoints(){
	return this->Points;
}

void Ply::setFileOut(char* FO){
	this->FileOut = FO;
}

void Ply::setEC(std::vector<Edge*> EC){
	this->EnvConvexe = EC;
}

void Ply::setPoints(std::vector<Point*> V){
	this->Points = V;
}

unsigned int Ply::Run(){
	std::ofstream file(this->getFileOut());
	if(file){
		file << "ply" << std::endl;
		file << "format ascii 1.0" << std::endl;
		file << "comment Enveloppe convexe par algorithme de Graham" << std::endl;

		file << "element vertex " << this->getPoints().size() << std::endl;
		file << "property float x" << std::endl;
		file << "property float y" << std::endl;
		file << "property float z" << std::endl;
		file << "element face " << std::endl;

	}else{
		std::cerr << "Erreur dans l'ouverture du fichier " << this->getFileOut() << std::endl;
		return -1;
	}
	return 0;
}