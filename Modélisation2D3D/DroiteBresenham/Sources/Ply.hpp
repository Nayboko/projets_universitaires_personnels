#ifndef PLY_H
#define PLY_H

#include "Edge.hpp"
#include <vector>

class Ply{
protected:
	char* FileOut;
	std::vector<Point*> Points;
	std::vector<Edge*> EnvConvexe;

public:
	Ply();
	Ply(char* FO, std::vector<Point*> V);
	Ply(char* FO, std::vector<Point*> V, std::vector<Edge*> EC);
	~Ply();

	unsigned int Run();

	char* getFileOut()const;
	std::vector<Edge*> getEC();
	std::vector<Point*> getPoints();

	void setFileOut(char* FO);
	void setEC(std::vector<Edge*> EC);
	void setPoints(std::vector<Point*> V);

};

#endif