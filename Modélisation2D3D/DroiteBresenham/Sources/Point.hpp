#ifndef POINT_HPP
#define POINT_HPP

#include <string>
#include <vector>

class Point{
protected:
	double x;
	double y;
	double angle;
	unsigned int ID;
	char* label;
public:
	static unsigned int NB_POINTS;

	Point();
	Point(double _x, double _y);
	Point(double _x, double _y, char* _label);
	Point(const Point& P);
	~Point();

	double getX() const;
	double getY() const;
	double getAngle() const;
	unsigned int getID()const;
	char* getLabel() const;

	void setX(const double _x);
	void setY(const double _y);
	void setAngle(const double _angle);
	void setLabel(char* _label);

	static int computePMin(std::vector<Point*> points);

	static std::vector<Point*> randomizedPoints(const Point Origine, unsigned int n, const unsigned int rayon);

	static double determinant(Point* r, Point* s, Point* t);

	static bool concave(Point* r, Point* s, Point* t);

	static int compare(Point* A, Point* B, Point* PMin);

	// Implémenté sous forme d'un tri rapide théoriquement en n(log(n))
	static std::vector<Point*> triPolaire(std::vector<Point*> V);

	static std::vector<Point*> concatenate(std::vector<Point*> less, std::vector<Point*> greater, Point* pivot);

	friend std::ostream& operator<<(std::ostream& os, const Point& p){
		Point tmp = p;
		//os << tmp.getX() << " " << tmp.getY() ;
		return os;
	}

};


#endif