#include "Edge.hpp"
#include <iostream>

using namespace std;

Edge::Edge(){
	this->from = nullptr;
	this->to = nullptr;
	this->label = "";
}
Edge::Edge(Point* f, Point* t){
	this->from = f;
	this->to = t;
	//this->label = f->getLabel() + t->getLabel();
}

Edge::Edge(double x1, double y1, double x2, double y2){
	this->from = new Point(x1, y1);
	this->to = new Point(x2, y2);
	this->label = "";
}

Edge::~Edge(){
	this->from = nullptr;
	this->to = nullptr;
}
	
std::string Edge::getLabel()const{
	return this->label;
}

void Edge::setLabel(std::string _label){
	this->label = _label;
}

Point* Edge::getFrom(){
	return this->from;
}

Point* Edge::getTo(){
	return this->to;
}

void Edge::setFrom(const Point f){
	Point* p = new Point(f);
	this->from = p;
}

void Edge::setTo(const Point t){
	Point* p = new Point(t);
	this->to = p;
}

double Edge::getX(){
	return this->getTo()->getX() - this->getFrom()->getX(); 
}

double Edge::getY(){
	return this->getTo()->getY() - this->getFrom()->getY();
}

double Edge::getX2(){
	double d = this->getX();
	return d * d; 
}

double Edge::getY2(){
	double d = this->getY();
	return d * d;
}

double Edge::normeCarree(){
	return getX2() + getY2();
}

double Edge::getCosThetaCarre(){
	double n2 = this->normeCarree();
	return (getY() * getY() / n2);
}

int Edge::determinant(Edge e){
	return (this->getTo()->getX() - this->getFrom()->getX()) * (e.getTo()->getY() - e.getFrom()->getY()) - (this->getTo()->getY() - this->getFrom()->getY()) * (e.getTo()->getX() - e.getFrom()->getX());
}

int Edge::detSigne(Edge e){
	int det = this->determinant(e);
	if(det == 0) return 0;
	else if(det > 0) return 1;
	return -1;
}

void AfficheVector(std::vector<Point*> V){
	for(auto& p : V){
		std::cout << p->getX() << " " << p->getY() << endl;
	}
}


std::vector<Edge*> Edge::Graham(std::vector<Point*> V){
	// Récupération du point de référence minimal
	unsigned int IndexPMin = Point::computePMin(V);
	Point* PMin = V.at(IndexPMin);
	V.erase(V.begin() + IndexPMin);

	cerr << "Calcul de l'enveloppe convexe avec Graham" << endl;

	std::vector<Point*> LTop = Point::triPolaire(V);
	AfficheVector(LTop);
	unsigned int m = LTop.size();
	// cerr << "Taille LTop : " << m << endl;
	std::vector<Point*> PEC;
	PEC.push_back(PMin); PEC.push_back(LTop.at(0)); PEC.push_back(LTop.at(1));
	for(int i = 2; i < m; i++){
		Point* P = LTop.at(i);
		unsigned int k = PEC.size() - 1;
		while(k > 1 && Point::concave(PEC.at(k-1), PEC.at(k), P)){
			PEC.erase(PEC.begin() + k);
			k = PEC.size() - 1;
		}
		PEC.push_back(P);
	}

	std::cerr << "Création des arêtes de l'enveloppe convexe" << std::endl;

	// On crée des arêtes pour pouvoir dessiner l'Enveloppe Convexe
	unsigned int i;
	std::vector<Edge*> EC;
	for(i = 0; i < PEC.size() - 1; i++){
		Edge* e = new Edge(PEC.at(i), PEC.at(i+1));
		EC.push_back(e);
	}
	Edge* e = new Edge(PEC.at(i), PEC.at(0));
	EC.push_back(e);

	return EC;
}