#ifndef EDGE_HPP
#define EDGE_HPP

#include "Point.hpp"

class Edge{
protected:
	Point* from;
	Point* to;
	std::string label;

public:
	Edge();
	Edge(Point* f, Point* t);
	Edge(double x1, double y1, double x2, double y2);
	~Edge();
	
	std::string getLabel()const;
	void setLabel(std::string _label);

	Point* getFrom();
	Point* getTo();

	void setFrom(const Point f);
	void setTo(const Point t);

	double getX();
	double getY();
	double getX2();
	double getY2();

	double normeCarree();
	double getCosThetaCarre();

	int determinant(Edge e);
	int detSigne(Edge e);

	static std::vector<Edge*> Graham(std::vector<Point*> V);


};

#endif