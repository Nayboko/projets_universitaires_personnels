#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <algorithm>

#include "Point.hpp"

unsigned int Point::NB_POINTS = 0;

Point::Point(){
	this->x = 0.0;
	this->y = 0.0;
	this->angle = 0.0;
	this->ID = Point::NB_POINTS;
	this->label = nullptr;
	Point::NB_POINTS++;
}

Point::Point(double _x, double _y){
	this->x = _x;
	this->y = _y;
	this->angle = 0.0;
	this->ID = Point::NB_POINTS;
	this->label = new char[255];
	sprintf(this->label, "%d", this->ID);
	Point::NB_POINTS++;
}

Point::Point(double _x, double _y, char* _label){
	this->x = _x;
	this->y = _y;
	this->angle = 0.0;
	this->ID = Point::NB_POINTS;
	this->label = _label;
	Point::NB_POINTS++;
}

Point::Point(const Point& P){
	this->x = P.getX();
	this->y = P.getY();
	this->angle = P.getAngle();
	this->ID = Point::NB_POINTS;
	this->label = new char[255];
	sprintf(this->label, "%d", this->ID);
	Point::NB_POINTS++;
}

Point::~Point(){
	Point::NB_POINTS--;
	delete this->label;
}

double Point::getX() const{
	return this->x;
}

double Point::getY() const{
	return this->y;
}

double Point::getAngle() const{
	return this->angle;
}

unsigned int Point::getID()const{
	return this->ID;
}

char* Point::getLabel() const{
	return this->label;
}

void Point::setX(const double _x){
	this->x = _x;
}

void Point::setY(const double _y){
	this->y = _y;
}

void Point::setAngle(const double _angle){
	this->angle = _angle;
}

void Point::setLabel(char* _label){
	this->label = _label;
}

int Point::computePMin(std::vector<Point*> points){
	int i = 0, j = 0;
	Point* p = points.at(i);

	for(Point* t : points){
		j++;
		if(t->getY() < p->getY() || (t->getY() == p->getY() && t->getX() < p->getX())) {
				p = t;
				i = j;
			}
	}
	return i;
}

std::vector<Point*> Point::randomizedPoints(const Point Origine, unsigned int n, const unsigned int rayon){
	std::vector<Point*> PTab;
	const int MAX = 1024, MIN = 0;
	double dist = (double) rayon +1;
	double x, y;
	srand(time(NULL));

	for(unsigned int i = 0; i < n; ++i){
		do{
			x = round((rand()%(MAX - MIN + 1)) + MIN);
			y = round((rand()%(MAX - MIN + 1)) + MIN);
			dist = sqrt(pow(Origine.getX() - x, 2) + pow(Origine.getY() - y, 2));
		}while(dist > rayon);

		// std::cerr << "(" << x << ", " << y << ")" <<  std::endl;
		// std::cerr << "dist : " << dist << std::endl;

		PTab.push_back(new Point(x, y));
	}

	return PTab;
}

double Point::determinant(Point* r, Point* s, Point* t){
	return (s->getX() - r->getX()) * (t->getY() - r->getY()) - (t->getX() - r->getX()) * (s->getY() - r->getY());
}

bool Point::concave(Point* r, Point* s, Point* t){
	return (Point::determinant(r, s, t) < 0);
}

int Point::compare(Point* A, Point* B, Point* PMin){
	if(A == B) return 0;

	// Conversion en long pour éviter les int-over/underflow
	double thetaA = atan2((long)A->getY() -  PMin->getY(), (long)A->getX() -  PMin->getX());
	double thetaB = atan2((long)B->getY() -  PMin->getY(), (long)B->getX() -  PMin->getX());

	if(thetaA < thetaB) return -1;
	else if(thetaA > thetaB) return 1;
	else{
		// Si collinéraire avec PMin, on compare la distance euclidienne
		double distA = abs(sqrt(pow((long) PMin->getX() - A->getX(), 2) +	pow((long) PMin->getY() - A->getX(), 2)));
		double distB = abs(sqrt(pow((long) PMin->getX() - B->getX(), 2) +	pow((long) PMin->getY() - B->getX(), 2)));
		if(distA < distB) return -1;
		else return 1;
	}
}

std::vector<Point*> Point::concatenate(std::vector<Point*> less, std::vector<Point*> greater, Point* pivot){
	std::vector<Point*> list;
	for(unsigned int i = 0; i < less.size(); i++){
		list.push_back(less.at(i));
	}
	list.push_back(pivot);
	for(unsigned int i = 0; i < greater.size(); i++){
		list.push_back(greater.at(i));
	}

	std::cerr << "Fin concatenate" << std::endl;
	return list;
}


std::vector<Point*> Point::triPolaire(std::vector<Point*> V){
	if(V.size() <= 1) return V;
	int middle = (int)ceil((double)V.size()/2.0);
	Point* pivot = V.at(middle);
	int IndexPMin = computePMin(V);
	Point* PMin = V.at(IndexPMin);

	// On va comparer chaque point vis à vis d'un point pivot
	std::vector<Point*> less, greater;
	for(unsigned int i = 0; i < V.size(); i++){
		if(Point::compare(pivot, V.at(i), PMin) >= 0){
			if(i == middle) continue;
			less.push_back(V.at(i));
		}else greater.push_back(V.at(i));
	}
	return Point::concatenate(less, greater, PMin);
}




