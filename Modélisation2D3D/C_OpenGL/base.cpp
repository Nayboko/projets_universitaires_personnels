#include <stdio.h>      
#include <stdlib.h>     
#include <cmath>
#include <iostream>
#include <fstream>
#include <string.h>
#include <string>
#include <vector>

#include "Vector.hpp"
#include "Point.hpp"
#include "Edge.hpp"
#include "tga.h"


using namespace std; 


#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>

// Definition de la taille de la fen\ufffdtre
#define WIDTH  480
#define HEIGHT 480

// Definition de la couleur de la fen\ufffdtre
#define RED   0.2
#define GREEN 0.2
#define BLUE  0.2
#define ALPHA 1

// Définition des couleurs de tracés

#define CRED 1.0, 0.0, 0.0
#define CGREEN 0.0, 1.0, 0.0
#define CBLUE 0.0, 0.0, 1.0
#define CCYAN 0.0, 1.0, 1.0
#define CWHITE 1.0, 1.0, 1.0
#define CBLACK 0.0, 0.0, 0.0
#define CBROWN 0.78, 0.39, 0.49
#define CORANGE 1.0, 0.5, 0.0


// Touche echap (Esc) permet de sortir du programme
#define KEY_ESC 27

#define Pas 0.1 // Pourcentage de déplacement par rapport à la taille de la fenêtre
#define STEP_OF_U 5
#define MIN_U 0
#define MAX_U 1000

#define STEP_OF_V 5
#define MIN_V 0
#define MAX_V 1000

#define STEP_OF_M 1
#define MIN_M 3
#define MAX_M 1000

#define STEP_OF_P 1
#define MIN_P 2
#define MAX_P 1000

// Entetes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid window_reshape(GLsizei width, GLsizei height, float x_min, float x_max, float y_min, float y_max, float z_min, float z_max);
GLvoid window_key(unsigned char key, int x, int y);
GLvoid window_special(int key, int x, int y);

void vBitmapOutput(float x, float y, float z, std::string string, void *font, Vector color);
void vStrokeOutput(GLfloat x, GLfloat y, char *string, void *font);

// Fonction de dessin
void DrawPoint(Point p, Vector color);
void DrawLine(Point p1, Point p2, Vector color);
void DrawVector(Point Origine, Vector V, Vector color);
void DrawCurve(vector<Point> TabPointsOfCurve, Vector color);
void DrawSurface(vector<vector<Point>> CurveSet, Vector Color);
void DrawPoly(vector<Point> pts, Vector Color);
void DrawQuad(Point A, Point B, Point C, Point D, Vector Color);
void DrawQuadAlpha(Point A, Point B, Point C, Point D, Vector Color);
void DrawMeridien(vector< vector<Point> > cylinder, Vector Color);
void DrawCylinder(vector<vector<Point> > sommets, Vector Color);
void DrawMeridienCone(vector< vector<Point> > cylinder, Vector Color);
void DrawTriangle(Point A, Point B, Point C, Vector Color);
void DrawCone(vector< vector<Point> > cone, Vector ColorB, Vector ColorM, Vector ColorF);
void DrawSphere(vector< vector<Point> > sphere, Vector ColorM, Vector ColorP, Vector ColorF);
void DrawCube(vector< vector<Point> > cube, Vector Color);
void DrawSpot(Point Spot, bool repere);
void DrawHouse(Point Origine, float hauteur, float profondeur, float largeur, float hToit, Vector Color);
void DrawEdge(Edge e);


int fact (int n);
double CoeffBernstein(double i, double n, double u);
Vector ComputeVector(Point A, Point B);
Vector ComputeNormaleSurface(Vector AB, Vector AC);

vector<Point> HermiteCubicCurve(Point P0, Point P1, Vector V0, Vector V1, long nbU);
vector<Point> BezierCurveByBernstein(vector<Point> TabControlPoint, long nbU);
vector<Point> BezierCurveByCasteljau(vector<Point> TabControlPoint, long nbU);

vector<vector<Point>> ComputeCylindricSurface(vector<Point> BezierCurve, Vector droite, int u, int v);
vector<vector<Point>> ComputeRuledSurface(vector<Point> Curve1, vector<Point> Curve2, int U, int V, Vector color);
void BezierSurfaceByBernstein(vector<Point> PtControlU, int U, vector<Point> PtControlV, int V, Vector color);

// Retourne les coordonées des deux 
vector < vector <Point> > ComputeCylinder(Point origine, int meridiens, double rayon, double hauteur);
vector < vector <Point> > ComputeCone(Point origine, Point S, int nbM, double rayon, double hauteur);
vector <vector<Point> > ComputeSphere(Point Origine, int nbM, int nbP, double rayon);
vector< vector<Point> > ComputeCube(Point O, float longueur);

void renderMesh(GLfloat* coords, GLfloat* normales, int* iSommets, int nT, Vector Color);
void MeshedCylinder(Point origine, int nbM, double rayon, double hauteur);
Point ComputeBarycentre(Point A, Point B, Point C); // Calculer le centre de gravite entre 3 points
void MeshedSphere(Point Origine, int nbM, int nbP, int rayon,int angleMax);

void ImageGaussienne(int nbSommets, int nbSurfaces, float* coords, int* iSurfaces, GLfloat* normales);


/* VARIABLES DE PROGRAMME */

float fHorizLeft=0;
float fVerticLeft=0;

float fHorizRight=0;
float fVerticRight=0;

int U = 50;
int V = 20;

int M = 50;
int P = 50;

// Rotation objet
float anglex = 0.0f;
float xcamBase = 0.0f;
float xcam = xcamBase;
float angley = 0.0f;
float ycamBase = 0.0f;
float ycam = ycamBase;
float zcamBase = 0.0f;
float zcam = zcamBase;
float alpha = 0.0f;

float u = 0.5;
float v = 0.5;


#define VITESSE_ROTATION 0.01f;


#define ALPHA_STEP 0.1f

string path = "";

int main(int argc, char **argv) {  

	// if(argc != 2){
	// 	cerr << "Usage : " << argv[0] << " fichier.off" << endl;
	// 	return -1;
	// }else path = argv[1];

// initialisation  des parametres de GLUT en fonction
// des arguments sur la ligne de commande
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);

// definition et creation de la fen\ufffdtre graphique, ainsi que son titre
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("TP6 : Maillage");

// initialisation de OpenGL et de la sc\ufffdne
	initGL();  
	init_scene();

// choix des proc\ufffddures de callback pour 
// le trace graphique
	glutDisplayFunc(&window_display);
// le redimensionnement de la fen\ufffdtre
	glutReshapeFunc(&window_reshape);
// la gestion des evenements clavier
	glutKeyboardFunc(&window_key);
// la gestion des touches spéciales du clavier
	glutSpecialFunc(&window_special);

// la boucle prinicipale de gestion des evenements utilisateur
	glutMainLoop();  

	return 1;
}

// initialisation du fond de la fenetre graphique : noir opaque
GLvoid initGL() 
{	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glClearColor(RED, GREEN, BLUE, ALPHA);
	glShadeModel(GL_FLAT);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_TEXTURE_2D);
	glCullFace(GL_FRONT);
}

void init_scene()
{
}

// fonction de call-back pour l\ufffdaffichage dans la fen\ufffdtre

GLvoid window_display()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	// gluLookAt(xcam, ycam, zcam,\
	// 0.0f, 0.0f,  0.0f,\
	// 0.0f, 0.0f,  1.0f);
	gluLookAt(sin(v * M_PI) * sin(u * 2 * M_PI) * 0.01, cos(v * M_PI) * 0.01, sin(v * M_PI) * cos(u * 2 * M_PI) * 0.01, 0.0,0.0,0.0,0.0,1.0,0.0);

	// gluLookAt(0.25f, 0.5f, 0.25f,
 //      0.0f, 0.0f,  0.0f,
 //      0.0f, 0.0f,  0.25f);

	//printf("%f\n", 0.25f + zcam);
	//glRotatef(alpha*180/3.14,1,1,1);


	render_scene();

	glFlush();
}

// fonction de call-back pour le redimensionnement de la fen\ufffdtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{  
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-30.0, 30.0, -30.0, 30.0, -30.0, 30.0);

	glMatrixMode(GL_MODELVIEW);
}

GLvoid window_reshape(GLsizei width, GLsizei height, float x_min, float x_max, float y_min, float y_max, float z_min, float z_max){
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float x_min_f = x_min - (abs(x_max - x_min));
	float x_max_f = x_max + (abs(x_max - x_min));
	float y_min_f = y_min - (abs(y_max - y_min));
	float y_max_f = y_max + (abs(y_max - y_min));
	float z_min_f = z_min - (abs(z_max - z_min));
	float z_max_f = z_max + (abs(z_max - z_min));

	cout << x_min_f << " " << x_max_f << " " << y_min_f << " " << y_max_f << " " << z_min_f << " " << z_max_f << endl; 

	glOrtho(x_min_f, x_max_f, y_min_f, y_max_f, z_min_f, z_max_f);
	
	glMatrixMode(GL_MODELVIEW);
}


GLvoid window_key(unsigned char key, int x, int y){  

	alpha = fmod(alpha, 180.0f);
	anglex = fmod(anglex, 2 * M_PI);
	angley = fmod(angley, 2 * M_PI);

	switch (key) {    
		case KEY_ESC:  
		exit(1);                    
		break;
		case 'z':
		fVerticLeft += Pas;
		angley -= VITESSE_ROTATION;
		ycam = cos(angley) * ycamBase + (-sin(angley)) * zcamBase;
		zcam = sin(angley) * ycamBase + cos(angley) * zcamBase;
		break;  
		case 'q':
		fHorizLeft -= Pas;
		anglex += VITESSE_ROTATION;
		xcam = cos(anglex) * xcamBase + (-sin(anglex)) * zcamBase;
		zcam = sin(anglex) * xcamBase + cos(anglex) * zcamBase;
 		break;
		case 'd':
		fHorizLeft += Pas;
		anglex -= VITESSE_ROTATION;
		xcam = cos(anglex) * xcamBase + (-sin(anglex)) * zcamBase;
		zcam = sin(anglex) * xcamBase + cos(anglex) * zcamBase;
		break;
		case 's':
		fVerticLeft -= Pas;
		angley += VITESSE_ROTATION;
		ycam = cos(angley) * ycamBase + (-sin(angley)) * zcamBase;
		zcam = sin(angley) * ycamBase + cos(angley) * zcamBase;
		break;
		case 'i':
		fVerticRight += Pas;
		break;  
		case 'j':
		fHorizRight -= Pas;
		break;
		case 'k':
			u -= VITESSE_ROTATION;
			if(u < 0.0) u = 1.0;
		break;
		case 'm':
			u += VITESSE_ROTATION;
			if(u > 1.0) u = 0.0;
		break;
		case 'l':
			v -= VITESSE_ROTATION;
			if(v < 0.0) v = 1.0;
		break;
		case 'o':
			v += VITESSE_ROTATION;
			if(v > 1.0) v = 0.0;
		break;
		case '+':
		U += STEP_OF_U;
		if(U > MAX_U) U = MAX_U;
		M += STEP_OF_M;
		if(M > MAX_M) M = MAX_M;
		P += STEP_OF_P;
		if(P > MAX_P) P = MAX_P;
		alpha += ALPHA_STEP;
		break;
		case '-':
		U -= STEP_OF_U;
		if(U < MIN_U) U = MIN_U;
		M -= STEP_OF_M;
		if(M < MIN_M) M = MIN_M;
		P -= STEP_OF_P;
		if(P < MIN_P) P = MIN_P;
		alpha -= ALPHA_STEP;
		break;
		case '*':
		V += STEP_OF_V;
		if(V > MAX_V) V = MAX_V;
		break;
		case '/':
		V -= STEP_OF_V;
		if(V < MIN_V) V = MIN_V;
		break;
		default:
		printf ("La touche %d n'est pas active.\n", key);
		break;
	}
	glutSwapBuffers(); 
	glutPostRedisplay();     
}

GLvoid window_special(int key, int x, int y){
	float speed = 0.1f;
	switch(key){
		case GLUT_KEY_RIGHT:
			zcam += speed;
			glutPostRedisplay();
		break;
		case GLUT_KEY_LEFT:
			zcam -= speed;
			glutPostRedisplay();
		break;
	}
}




void render_scene(){
//Definition de la couleur
	glColor3f(CWHITE);
	Vector* cVert = new Vector(CGREEN);
	Vector* cBlanc = new Vector(CWHITE);
	Vector* cRouge = new Vector(CRED);
	Vector* cBleu = new Vector(CBLUE);
	Vector* cCyan = new Vector(CCYAN);
	Vector* cOrange = new Vector(CORANGE);

	Point O = Point(0.0,0.0,0.0); // Origine

	glPointSize(3);

	

	// ################################
	//      Test Point et Vector       #
	// ################################

	/*Point* P1 = new Point(0.7, 1, 0);
	Point* P2 = new Point(-1, 1, 0);
	Point* P3 = new Point(1, -1, 0);
	Point* P4 = new Point(1, 0, 0);
	Point* Origine = new Point(0, 0, 0);

	Vector* V = new Vector(0.5, 0.5, 0);
	Vector* U = new Vector(-0.5, 0.4, 0);
	Vector* NormalOfPlane = new Vector(0, 0.5, -0.2);


	glPointSize(5);
	DrawPoint(*P1, *cVert);


	DrawLine(*P2, *P3, *cBlanc);


	Point P1Prime = P1->ProjectOnLine(*P2, *P3);
	DrawPoint(P1Prime, *cRouge);
	DrawLineStipple(P1Prime, *P1, *cRouge);


	DrawPoint(Point(P2->getX() + V->getX(),
		P2->getY() + V->getY(),
		P2->getZ() + V->getZ()), *cBleu);
	DrawVector(*Origine, *V, *cRouge);
	DrawVector(*P2, *V, *cRouge);

	DrawVector(*Origine, *U, *cVert);
	DrawVector(*P2, *U, *cVert);



	Point P1Seconde = P1->ProjectOnLine(*V, *P2);
	DrawPoint(P1Seconde, *cBleu);

	DrawPoint(*P4, *cRouge);
	DrawVector(*Origine, *NormalOfPlane, *cCyan);
	DrawVector(*P4, *NormalOfPlane, *cCyan);

	DrawLineStipple(Point(-2, 0, 0), Point(2, 0, 0), *cBlanc);
	Point P1Tierce = P1->ProjectOnPlane(*P4, *NormalOfPlane);
	DrawPoint(P1Tierce, *cBleu);

	// Affichage des labels
	vBitmapOutput(P1->getX(), P1->getY() + 0.1, "P1", GLUT_BITMAP_9_BY_15, *cBlanc);
	vBitmapOutput(P2->getX(), P2->getY() + 0.1, "P2", GLUT_BITMAP_9_BY_15, *cBlanc);
	vBitmapOutput(P3->getX(), P3->getY() + 0.1, "P3", GLUT_BITMAP_9_BY_15, *cBlanc);
	vBitmapOutput(P4->getX(), P4->getY() + 0.1, "P4", GLUT_BITMAP_9_BY_15, *cBlanc);
	vBitmapOutput(P1Prime.getX() - 0.1, P1Prime.getY() - 0.2, "P1'", GLUT_BITMAP_9_BY_15, *cBlanc);
	vBitmapOutput(P1Seconde.getX() - 0.1, P1Seconde.getY() - 0.2, "P1''", GLUT_BITMAP_9_BY_15, *cBlanc);
	vBitmapOutput(P1Tierce.getX() + 0.1, P1Tierce.getY() + 0.2, "P1'''", GLUT_BITMAP_9_BY_15, *cBlanc);

	// Libération du tas
	delete P1;
	delete P2;
	delete P3;
	delete P4;
	delete Origine;
	delete V;
	delete U; */

	// ##########################################################
	//      Création fonction pour courbes paramétriques        #
	// ##########################################################

	/*Point* P0 = new Point(0.0, 0.0, 0.0);
	Point* P1 = new Point(2.0, 0.0, 0.0);

	Vector* V0 = new Vector(1.0, 1.0, 0.0);
	Vector* V1 = new Vector(1.0, -1.0, 0.0);
	V0->Normalize(); //DrawVector(*P0, *V0, *cCyan);
	V1->Normalize(); //DrawVector(*P1, *V1, *cCyan);

	long nbU = U;

	// // Tracer courbe de P0 à P1
	vector<Point> TabPointsOfCurve;
	TabPointsOfCurve.push_back(*P0);
	TabPointsOfCurve.push_back(*P1);

	DrawCurve(TabPointsOfCurve, *cVert, false);

	// Tracer la courbe d'Hermite de P0 à P1

	vector<Point> HermiteCurve = HermiteCubicCurve(*P0, *P1, *V0, *V1, nbU);
	DrawCurve(HermiteCurve, *cRouge, false);

	// Tracer la courbe de Bézier avec les Polynomes de Bernstein
	vector<Point> PtControla;
	vector<Point> PtControlb;

	Point* PC0a = new Point(-2.25 + fHorizLeft, -1.0 + fVerticLeft, 0.0);  PtControla.push_back(*PC0a);
	Point* PC1a = new Point(-1.25, 0.5, 0.0);    PtControla.push_back(*PC1a);
	Point* PC2a = new Point(-0.25, 0.4, 0.0);    PtControla.push_back(*PC2a);
	Point* PC3a = new Point(-0.25, -1.1, 0.0);   PtControla.push_back(*PC3a);
	Point* PC4a = new Point(-1.25, -1.0, 0.0);   PtControla.push_back(*PC4a);
	Point* PC5a = new Point(-2.25, 1.0, 0.0);   PtControla.push_back(*PC5a);
	Point* PC6a = new Point(-1.5 + fHorizLeft, 1.0 + fVerticLeft, 0.0);   PtControla.push_back(*PC6a);

	vector<Point> BezierCurve_B_a = BezierCurveByBernstein(PtControla, nbU);
	DrawCurve(PtControla, *cBlanc, false);
	DrawCurve(BezierCurve_B_a, *cCyan, false);



	Point* PC0b = new Point(-1.0, 1.0, 0.0);  PtControlb.push_back(*PC0b);
	Point* PC1b = new Point(-0.5, 1.75, 0.0);   PtControlb.push_back(*PC1b);
	Point* PC2b = new Point(0.25, 1.75, 0.0);   PtControlb.push_back(*PC2b);
	Point* PC3b = new Point(1.0, 0.5, 0.0);  PtControlb.push_back(*PC3b);
	Point* PC4b = new Point(1.5, 0.75, 0.0);  PtControlb.push_back(*PC4b);
	Point* PC5b = new Point(2.0, 1.5, 0.0);   PtControlb.push_back(*PC5b);
	Point* PC6b = new Point(2.5, 2.5, 0.0);    PtControlb.push_back(*PC6b);

	vector<Point> BezierCurve_B_b = BezierCurveByBernstein(PtControlb, nbU);
	DrawCurve(PtControlb, *cBlanc, true);
	DrawCurve(BezierCurve_B_b, *cCyan, false);

	cout << "PC6b : " << PC6b->getX() << " " << PC6b->getY() << " " << PC6b->getZ() << " " << endl;
	cout << "Last point of BezierCurve_B_b : " << BezierCurve_B_b.at(BezierCurve_B_b.size() - 1).getX() << " " << BezierCurve_B_b.at(BezierCurve_B_b.size() - 1).getY() << " " << BezierCurve_B_b.at(BezierCurve_B_b.size() - 1).getZ() << " " << endl;

	cout << "PC0b : " << PC0b->getX() << " " << PC0b->getY() << " " << PC0b->getZ() << " " << endl;
	cout << "First point of BezierCurve_B_b : " << BezierCurve_B_b.at(0).getX() << " " << BezierCurve_B_b.at(0).getY() << " " << BezierCurve_B_b.at(0).getZ() << " " << endl;

	// Affichage du nom des points
	vBitmapOutput(P0->getX(), P0->getY() + 0.1, "P0", GLUT_BITMAP_9_BY_15, *cBlanc);
	vBitmapOutput(P1->getX(), P1->getY() + 0.1, "P1", GLUT_BITMAP_9_BY_15, *cBlanc);
	string s = "Valeur de U : " + std::to_string(nbU);
	vBitmapOutput(0.75, -2.5, s, GLUT_BITMAP_9_BY_15, *cBlanc);

	// Tracer la courbe de Bézier avec l'algorithme de Casteljau
	vector<Point> PtControlc;

	Point* PC0c = new Point(-1.5 + fHorizRight, -2.75 + fVerticRight, 0.0);  PtControlc.push_back(*PC0c);
	Point* PC1c = new Point(-0.5, -1.25, 0.0);   PtControlc.push_back(*PC1c);
	Point* PC2c = new Point(0.5, -1.75, 0.0);   PtControlc.push_back(*PC2c);
	Point* PC3c = new Point(1.0, -0.75, 0.0);  PtControlc.push_back(*PC3c);
	Point* PC4c = new Point(2.0 + fHorizRight, -1.5 + fVerticRight, 0.0);  PtControlc.push_back(*PC4c);

	vector<Point> BezierCurve_C = BezierCurveByCasteljau(PtControlc, nbU);
	DrawCurve(PtControlc, *cBlanc, false);
	DrawCurve(BezierCurve_C, *cRouge, false);

	delete P0;
	delete P1;
	delete V0;
	delete V1;

	delete PC0a;
	delete PC1a;
	delete PC2a;
	delete PC3a;
	delete PC4a;
	delete PC5a;
	delete PC6a; */

	// ################################
	//      Surfaces paramétriques    #
	//#################################

	/*long nbU = U;
	long nbV = V;

	glPointSize(5);

	vector<Point> PtControl;

	Point* PC0 = new Point(-2.0, 1.5, 0.0);     PtControl.push_back(*PC0);
	Point* PC1 = new Point(-1.5, 1.75, 0.0);    PtControl.push_back(*PC1);
	Point* PC2 = new Point(-0.75, 1.75, 0.0);    PtControl.push_back(*PC2);
	Point* PC3 = new Point(0.0, 0.5, 0.0);     PtControl.push_back(*PC3);
	Point* PC4 = new Point(0.5, 0.75, 0.0);    PtControl.push_back(*PC4);
	Point* PC5 = new Point(1.0, 1.5, 0.0);      PtControl.push_back(*PC5);
	Point* PC6 = new Point(1.5, 1.75, 0.0);     PtControl.push_back(*PC6);

	

	Vector droite = Vector(1.0, 1.0, 0.0);

	vector<Point> BezierCurve_B = BezierCurveByBernstein(PtControl, nbU);
	// cout << "Nombre de points de BezierCurve_B : " << BezierCurve_B.size() << endl;
	vector<vector<Point>> BezierSurface_B = ComputeCylindricSurface(BezierCurve_B, droite, nbU, nbV);

	DrawSurface(BezierSurface_B, *cVert);
	//DrawCurve(PtControl, *cBlanc);
	//DrawCurve(BezierCurve_B, *cCyan);

	vector<Point> PtControla, PtControlb;

	Point* P0a = new Point(-2.0, 0.0, 0.0); PtControla.push_back(*P0a);
	Point* P1a = new Point(-1.5, 0.5, 0.0); PtControla.push_back(*P1a);
	Point* P2a = new Point(-1.0, -0.25, 0.0); PtControla.push_back(*P2a);
	Point* P3a = new Point(-0.25, -0.75, 0.0); PtControla.push_back(*P3a);

	Point* P0b = new Point(-2.5, -2.0, 0.0); PtControlb.push_back(*P0b);
	Point* P1b = new Point(-2.0, -1.5, 0.0); PtControlb.push_back(*P1b);
	Point* P2b = new Point(-1.5, -1.25, 0.0); PtControlb.push_back(*P2b);
	Point* P3b = new Point(-0.25, -1.75, 0.0); PtControlb.push_back(*P3b);

	vector<Point> BezierCurve_AA = BezierCurveByBernstein(PtControla, nbU);
	vector<Point> BezierCurve_AB = BezierCurveByBernstein(PtControlb, nbU);

	vector<vector<Point>> BezierSurface_Adjusted = ComputeRuledSurface(BezierCurve_AA, BezierCurve_AB, nbU, nbV, *cRouge);

	DrawSurface(BezierSurface_Adjusted, *cRouge);
	DrawCurve(PtControla, *cBlanc);
	DrawCurve(BezierCurve_AA, *cCyan);
	DrawCurve(PtControlb, *cBlanc);
	DrawCurve(BezierCurve_AB, *cCyan);




	vector<Point> PtControlc, PtControld, PtControle;

	Point* P0c = new Point(0.0, 0.0, 0.0); PtControlc.push_back(*P0c); DrawPoint(*P0c, *cBlanc);
	Point* P1c = new Point(1.0, 0.25, 0.0); PtControlc.push_back(*P1c); DrawPoint(*P1c, *cBlanc);
	Point* P2c = new Point(1.5, -0.25, 0.0); PtControlc.push_back(*P2c); DrawPoint(*P2c, *cBlanc);

	Point* P0d = new Point(0.0, -2.0, 0.0); PtControld.push_back(*P0d); DrawPoint(*P0d, *cBlanc);
	Point* P1d = new Point(1.0, -1.5, 0.0); PtControld.push_back(*P1d); DrawPoint(*P1d, *cBlanc);
	Point* P2d = new Point(1.75, -1.25, 0.0); PtControld.push_back(*P2d); DrawPoint(*P2d, *cBlanc);


	// Point* P0e = new Point(0.25, -1.0, 0.0); PtControle.push_back(*P0e); DrawPoint(*P0e, *cBlanc);
	// Point* P1e = new Point(0.75, -0.75, 0.0); PtControle.push_back(*P1e); DrawPoint(*P1e, *cBlanc);
	// Point* P2e = new Point(1.5, -0.75, 0.0); PtControle.push_back(*P2e); DrawPoint(*P2e, *cBlanc);

	vector< vector<Point> > Grille;
	Grille.push_back(PtControlc); Grille.push_back(PtControld); //Grille.push_back(PtControle);

	vector< vector<Point> > Res = BezierSurfaceByBernstein(Grille, nbU, nbV);
	// DrawCurve(PtControlc, *cVert);
	// DrawCurve(PtControld, *cVert);
	// DrawCurve(PtControle, *cVert);
	DrawSurface(Res, *cRouge);


	//  Affichage U et V
	string s1 = "Valeur de U : " + std::to_string(nbU);
	vBitmapOutput(0.75, -2.5, s1, GLUT_BITMAP_9_BY_15, *cBlanc);
	string s2 = "Valeur de V : " + std::to_string(nbV);
	vBitmapOutput(0.75, -2.75, s2, GLUT_BITMAP_9_BY_15, *cBlanc); 
	delete PC0;
	delete PC1;
	delete PC2;
	delete PC3;
	delete PC4;
	delete PC5;
	delete PC6;
	
	delete PC0a;
	delete PC1a;
	delete PC2a;
	delete PC3a;
	delete PC0b;
	delete PC1b;
	delete PC2b;
	delete PC3b;
	delete PC0c;
	delete PC1c;
	delete PC2c;
	delete PC0d;
	delete PC1d;
	delete PC2d;
	delete PC0e;
	delete PC1e;
	delete PC2e;


	*/
	// #############################################
	//      Représentations surfaciques       #
	//##############################################

	Point O = Point(0.0, 0.0, 0.0);

	// Creation d'un cylindre
	vector<vector<Point> > cylindre = ComputeCylinder(O, M, 10, 20);

	vector<Point> Haut = cylindre.at(0);
	vector<Point> Bas = cylindre.at(1);

	cout << "Taile Haut : " << Haut.size() << endl;
	cout << "Taile Bas : " << Bas.size() << endl;

	DrawCylinder(cylindre, *cCyan);
	DrawPoly(Bas, *cRouge);
	DrawMeridien(cylindre, *cBlanc);
	DrawPoly(Haut, *cVert);

	// Création d'un cylindre

	/*Point S = Point(0.0,0.0,20.0); // Sommet du cône
	double hauteur = 20.0;
	double rayon = 15.0;
	int nbM = M; // Nombre de méridiens

	vector< vector<Point> > Cone = ComputeCone(O,S, nbM, rayon, hauteur);
	DrawCone(Cone, *cRouge, *cBlanc, *cBleu);*/

	// Création d'une sphère

	// int nbM = M; // Nombre de méridiens
	// int nbP = P; // Nombre de parallèles
	// double rayon = 20;

	// vector< vector <Point> > sphere = ComputeSphere(O, nbM, nbP, rayon);
	// DrawSphere(sphere, *cRouge, *cOrange, *cCyan);


	// string s1 = "Valeur de M : " + std::to_string(M);
	// vBitmapOutput(0.0, 0.0, 30.0, s1 , GLUT_BITMAP_9_BY_15, *cBlanc);
	// string s2 = "Valeur de P : " + std::to_string(P);
	// vBitmapOutput(0.0, 0.0, 25.0, s2 , GLUT_BITMAP_9_BY_15, *cBlanc);

	// ######################################
	//     Mise en lumière et création d'objets     #
	//#######################################

	/*
		MISE EN PLACE DES LUMIERES
	*/
	// Valeur des couleurs de la lumière
	GLfloat Light0Diff[]	= {0.2f,0.2f,0.2f,1.0f};
	GLfloat Light0Spec[]	= {1.0f,0.2f,0.2f,1.0f};
	GLfloat Light0Amb[]		= {0.3f,0.3f,0.3f,1.0f};

	// Valeur de position Light0
	GLfloat Light0Pos[] = {10.0f,15.0f,15.0f,0.0f};

	Point Spot0 = Point(Light0Pos[0],Light0Pos[1],Light0Pos[2]);
	DrawSpot(Spot0, false);

	// Fixation des paramètres de couleur de Light0

	glLightfv(GL_LIGHT0,GL_DIFFUSE,Light0Diff);
	glLightfv(GL_LIGHT0,GL_SPECULAR,Light0Spec);
	glLightfv(GL_LIGHT0,GL_AMBIENT,Light0Amb);

	// Fixation de la position de Light0o
	glLightfv(GL_LIGHT0, GL_POSITION, Light0Pos);
	
	GLfloat fogColor[] = { 0.4f,0.4f,0.4f,0.0f };
	glFogf(GL_FOG_MODE, GL_EXP);
	glFogf(GL_FOG_DENSITY, 0.02f);
	glFogfv(GL_FOG_COLOR, fogColor);
	glEnable(GL_FOG);

	// 	CREATION DES OBJETS


	// Point A = Point(-5.0,-5.0,0.0);
	// Point B = Point(20.0,10.0,2.0);
	// Point C = Point(-20.0,10.0,2.0);
	// vector< vector<Point> > cubeA = ComputeCube(A, 5.0f);
	// DrawCube(cubeA, *cRouge);
	// vector< vector<Point> > cubeB = ComputeCube(B, 5.0f);
	// DrawCube(cubeB, *cBleu);
	// vector< vector<Point> > cubeC = ComputeCube(C, 5.0f);
	// DrawCube(cubeC, *cVert);

	// Modélisation d'une maison

	/*glDisable(GL_LIGHTING);
	glDisable(GL_LIGHT0);

	Point A = Point(4.0f, 0.0f, -5.0f);
	DrawHouse(A, 5.0f, 8.0f, 10.0f, 3.0f, *cRouge);


	DrawSpot(Spot0, true);
	DrawCube(cubeC, *cVert);

	// Other method to draw house
	Point A = Point(4.0f, 0.0f, -5.0f); Point B = Point(4.0f, 0.0f, 5.0f);
	Point C = Point(-4.0f, 0.0f, 5.0f); Point D = Point(-4.0f, 0.0f, -5.0f);

	Vector AB = ComputeVector(A, B);
	Vector AC = ComputeVector(A, C);
	Vector N = ComputeNormaleSurface(AB, AC);
	N.Normalize();
	glNormal3f(N.getX(), N.getY(), N.getZ());
	DrawQuad(A, B, C, D, *cRouge); // Bottom
	Point E = Point(4.0f, 5.0f, -5.0f); Point F = Point(4.0f, 5.0f, 5.0f);
	Point G = Point(-4.0f, 5.0f, 5.0f); Point H = Point(-4.0f, 5.0f, -5.0f);
	DrawQuad(A, B, E, F, *cRouge); // Front
	DrawQuad(C, D, H, G, *cRouge); // Back
	DrawQuad(D, A, E, H, *cRouge); // Left
	DrawQuad(B, C, G, F, *cRouge); // Right
	Point I = Point(0.0f, 8.0f, -5.0f); Point J = Point(0.0f, 8.0f, 5.0f);
	DrawQuad(E, F, J, I, *cRouge); // Toit Front
	DrawQuad(G, H, I, J, *cRouge); // Toit Back
	DrawTriangle(E, H, I, *cRouge); // Toit Left
	DrawTriangle(F, G, J, *cRouge); // Toit Right */

	// ######################################
	//      Création de maillage à partir de .OFF				    #
	//#######################################

	/*ifstream file(path, ios::in);
	if (file.is_open()) {
		string type;
		file >> type;

		unsigned int nP, nT, nE; // Nombre de points, de triangles et d'arretes du modèle 3D
		file >> nP; // Nombre points
		file >> nT; // Nombre triangles
		file >> nE; // Nombre edges

		if (nP != 0 && nT != 0) {
			GLfloat* coords = new GLfloat[nP * 3]; // Coordonnées de chaque points (x, y, z)
			float xMin, xMax, yMin, yMax, zMin, zMax; // Coordonnes minimales pour resize la fenetre

			// On récupère les coordonnées des points de chaque triangle
			for (int i = 0; i < nP; i++) {
				file >> coords[3 * i];
				file >> coords[3 * i + 1];
				file >> coords[3 * i + 2];
				if (i == 0) { // Initialisation des coords minimales
					xMin = coords[0];
					xMax = coords[0];
					yMin = coords[1];
					yMax = coords[1];
					zMin = coords[2];
					zMax = coords[2];
				}
				else { // Mise à jours des coords minimales
					if (coords[3 * i] < xMin) xMin = coords[3 * i];
					if (coords[3 * i] > xMax) xMax = coords[3 * i];
					if (coords[3 * i + 1] < yMin) yMin = coords[3 * i + 1];
					if (coords[3 * i + 1] > yMax) yMax = coords[3 * i + 1];
					if (coords[3 * i + 2] < zMin) zMin = coords[3 * i + 2];
					if (coords[3 * i + 2] > zMax) zMax = coords[3 * i + 2];
				}
			}

			int* iSommets = new int[nT * 3]; // Chaque triangle a 3 sommets
			GLfloat* normales = new GLfloat[nT * 3]; // Tableau des coords (x,y,z) des normales de chaque triangle

			for (int i = 0; i < nT; i++) {
				int nS; // Nombre de sommets du triangle
				file >> nS;
				// On parcourt les sommets du triangle
				for (int j = 0; j < nS; j++) {
					file >> iSommets[3 * i + j];
				}

				Point P1 = Point(coords[iSommets[3 * i] * 3], coords[iSommets[3 * i] * 3 + 1], coords[iSommets[3 * i] * 3 + 2]);
				Point P2 = Point(coords[iSommets[3 * i + 1] * 3], coords[iSommets[3 * i + 1] * 3 + 1], coords[iSommets[3 * i + 1] * 3 + 2]);
				Point P3 = Point(coords[iSommets[3 * i + 2] * 3], coords[iSommets[3 * i + 2] * 3 + 1], coords[iSommets[3 * i + 2] * 3 + 2]);

				Vector V1 = ComputeVector(P1, P2);
				Vector V2 = ComputeVector(P1, P3);

				// Calcul de la normale de la surface
				Vector N = ComputeNormaleSurface(V1, V2);
				N.Normalize();

				normales[i * 3] = N.getX();
				normales[i * 3 + 1] = N.getY();
				normales[i * 3 + 2] = N.getZ();
			}

			file.close();

			renderMesh(coords, normales, iSommets, nT, *cRouge);

			window_reshape(480, 480, xMin, xMax, yMin, yMax, zMin, zMax);

		}
		else {
			cerr << "Erreur dans le fichier sur le nombre de points ou le nombre de faces. Ils ne peuvent pas être nuls !" << endl;
			exit(-2);
		}
	}
	else {
		cerr << "Impossible d'ouvrir le fichier " << path << " !" << endl;
		exit(-1);
	}*/

	// ######################################
	//      Création d'objets meshés		    #
	//#######################################	


	// MeshedCylinder(O, M, 10, 30);
	MeshedSphere(O,M,P, 10,30);

	delete cVert;
	delete cBlanc;
	delete cRouge;
	delete cBleu;
	delete cCyan;
	delete cOrange;

}

void renderMesh(GLfloat* coords, GLfloat* normales, int* iSommets, int nT, Vector Color){
	glColor3f(Color.getX(), Color.getY(), Color.getZ());

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, coords);
	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_FLOAT, 0, normales);

	// glDrawElements(GL_TRIANGLES, 3 * nT, GL_UNSIGNED_INT, iSommets);

	for(int i = 0; i < nT; i++){
		glBegin(GL_TRIANGLES);
		glNormal3f(normales[i * 3], normales[i * 3 + 1], normales[i * 3 + 2]); glVertex3f(coords[iSommets[3 * i] * 3], coords[iSommets[3 * i] * 3 + 1], coords[iSommets[3 * i] * 3 + 2]);
		glNormal3f(normales[i * 3], normales[i * 3 + 1], normales[i * 3 + 2]); glVertex3f(coords[iSommets[3 * i + 1] * 3], coords[iSommets[3 * i + 1] * 3 + 1], coords[iSommets[3 * i + 1] * 3 + 2]);
		glNormal3f(normales[i * 3], normales[i * 3 + 1], normales[i * 3 + 2]); glVertex3f(coords[iSommets[3 * i + 2] * 3], coords[iSommets[3 * i + 2] * 3 + 1], coords[iSommets[3 * i + 2] * 3 + 2]);

		glEnd();
	}

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
}

void DrawSpot(Point Spot, bool repere){
	glPointSize(15);
	DrawPoint(Spot, Vector(1.0,1.0,1.0));
	glPointSize(3);
	if(repere){
		DrawVector(Spot,Vector(5.0,0.0,0.0), Vector(0.0,0.0,1.0));
		DrawVector(Spot,Vector(0.0,5.0,0.0), Vector(1.0,0.0,0.0));
		DrawVector(Spot,Vector(0.0,0.0,5.0), Vector(0.0,1.0,0.0));
	}
}

void DrawHouse(Point Origine, float hauteur, float profondeur, float largeur, float hToit, Vector Color){

/*	BYTE *img;
	int l, h;
	GLuint texture;*/

	// x : Profondeur - y : Hauteur - z : Largeur

	Point A = Origine;
	Point B = Point(Origine.getX(), Origine.getY(), Origine.getZ() + largeur);
	Point C = Point(Origine.getX() - profondeur, Origine.getY(), Origine.getZ() + largeur);
	Point D = Point(Origine.getX() - profondeur, Origine.getY(), Origine.getZ());

	Point E = Point(Origine.getX(), Origine.getY() + hauteur, Origine.getZ());
	Point F = Point(Origine.getX(), Origine.getY() + hauteur, Origine.getZ() + largeur);
	Point G = Point(Origine.getX() - profondeur, Origine.getY() + hauteur, Origine.getZ() + largeur);
	Point H = Point(Origine.getX() - profondeur, Origine.getY() + hauteur, Origine.getZ());

	Point I = Point(Origine.getX() - (profondeur/2.0), Origine.getY() + (hauteur + hToit),Origine.getZ()); 
	Point J = Point(Origine.getX() - (profondeur/2.0), Origine.getY() + (hauteur + hToit),Origine.getZ() + largeur);

	// Pour chaque face, on calcule la normale au plan pour la gestion de la lumiere
	Vector N  = ComputeNormaleSurface(ComputeVector(A,B), ComputeVector(A,D));
	N.Normalize();
	glNormal3f(N.getX(), N.getY(), N.getZ());
	DrawQuad(A,B,C,D, Color); // Bottom

/*	N  = ComputeNormaleSurface(ComputeVector(E,F), ComputeVector(E,H));
	glNormal3f(N.getX(), N.getY(), N.getZ());
	DrawQuad(E,F,G,H, Color); // Top*/
	
	// FRONT

// PROVOQUE SEGFAULT
/*	glGenTextures(1, &texture);
	img = load_tga("./Textures/facade.tga", &l, &h);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, l, h, 0, GL_RGB, GL_UNSIGNED_BYTE, img);

	N  = ComputeNormaleSurface(ComputeVector(A,B), ComputeVector(A,E));
	glNormal3f(N.getX(), N.getY(), N.getZ());
	glColor4f(Color.getX(), Color.getY(), Color.getZ(), 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_QUADS);
	glTexCoord2f(0.0f,0.0f);
	glVertex3f(A.getX(), A.getY(), A.getZ());
	glTexCoord2f(1.0f,0.0f);
	glVertex3f(B.getX(), B.getY(), B.getZ());
	glTexCoord2f(1.0f,1.0f);
	glVertex3f(F.getX(), F.getY(), F.getZ());
	glTexCoord2f(0.0f,1.0f);
	glVertex3f(E.getX(), E.getY(), E.getZ());
	glEnd();

	glDisable(GL_BLEND);*/

	N  = ComputeNormaleSurface(ComputeVector(A,B), ComputeVector(A,E));
	glNormal3f(N.getX(), N.getY(), N.getZ());
	DrawQuad(A,B,F,E, Color); // Back

	
	N  = ComputeNormaleSurface(ComputeVector(C,D), ComputeVector(C,G));
	glNormal3f(N.getX(), N.getY(), N.getZ());
	DrawQuad(C,D,H,G, Color); // Back

	N  = ComputeNormaleSurface(ComputeVector(D,A), ComputeVector(D,H));
	glNormal3f(N.getX(), N.getY(), N.getZ());
	DrawQuad(D,A,E,H, Color); // Left

	N  = ComputeNormaleSurface(ComputeVector(B,C), ComputeVector(B,F));
	glNormal3f(N.getX(), N.getY(), N.getZ());
	DrawQuad(B,C,G,F, Color); // Right

	N  = ComputeNormaleSurface(ComputeVector(H,I), ComputeVector(H,E));
	glNormal3f(N.getX(), N.getY(), N.getZ());
	DrawTriangle(H,E,I, Color); // Toit Left

	N  = ComputeNormaleSurface(ComputeVector(F,G), ComputeVector(F,J));
	glNormal3f(N.getX(), N.getY(), N.getZ());
	DrawTriangle(F,G,J, Color); // Toit Right
	
	N  = ComputeNormaleSurface(ComputeVector(E,F), ComputeVector(E,I));
	glNormal3f(N.getX(), N.getY(), N.getZ());
	DrawQuad(E,F,J,I, Color); // Toit Front

	N  = ComputeNormaleSurface(ComputeVector(G,H), ComputeVector(G,J));
	glNormal3f(N.getX(), N.getY(), N.getZ());
	DrawQuad(G,H,I,J, Color); // Toit Back


}



void vBitmapOutput(float x, float y, float z, std::string string, void *font, Vector color)
{
	glColor3f(color.getX(), color.getY(), color.getZ());
	int len,i; // len donne la longueur de la chaîne de caractères

	glRasterPos3f(x,y,z); // Positionne le premier caractère de la chaîne
	len = string.length(); // Calcule la longueur de la chaîne
	for (i = 0; i < len; i++) glutBitmapCharacter(font,string[i]); // Affiche chaque caractère de la chaîne
}

void DrawPoint(Point p, Vector color){
	glColor3f(color.getX(),color.getY(), color.getZ());
	glBegin(GL_POINTS);
	glVertex3f(p.getX(), p.getY(), p.getZ());
	glEnd();
}

void DrawLine(Point p1, Point p2, Vector color){
	DrawPoint(p1, color);
	DrawPoint(p2, color);
	glColor3f(color.getX(), color.getY(), color.getZ());
	glBegin(GL_LINES);
	glVertex3f(p1.getX(), p1.getY(), p1.getZ());
	glVertex3f(p2.getX(), p2.getY(), p2.getZ());
	glEnd();
}

void DrawLineStipple(Point p1, Point p2, Vector color){
	DrawPoint(p1, color);
	DrawPoint(p2, color);
	glColor3f(color.getX(), color.getY(), color.getZ());
	glLineStipple(1, 0x00FF);
	glEnable(GL_LINE_STIPPLE);
	glBegin(GL_LINES);
	glVertex3f(p1.getX(), p1.getY(), p1.getZ());
	glVertex3f(p2.getX(), p2.getY(), p2.getZ());
	glEnd();
	glDisable(GL_LINE_STIPPLE);
}

void DrawVector(Point Origine, Vector V, Vector color){
	glColor3f(color.getX(), color.getY(), color.getZ());
	Vector VPrime = Vector(V.getX() + Origine.getX(), V.getY() + Origine.getY(), V.getZ() + Origine.getZ());
	glBegin(GL_LINES);
	glVertex3f(Origine.getX(), Origine.getY(), Origine.getZ());
	glVertex3f(VPrime.getX(), VPrime.getY(), VPrime.getZ());
	glEnd();
	//DrawPoint(Point(VPrime.getX(), VPrime.getY(), VPrime.getZ()), color);
}

void DrawEdge(Edge e){
	Point A = e.getFrom(); Point B = e.getTo(); Vector Color = e.getColor();
	glColor3f(Color.getX(), Color.getY(), Color.getZ());
	glBegin(GL_LINES);
	glVertex3f(A.getX(), A.getY(), A.getZ());
	glVertex3f(B.getX(), B.getY(), B.getZ());
	glEnd();
}

void DrawCurve(vector<Point> TabPointsOfCurve, Vector color){
	glColor3f(color.getX(), color.getY(), color.getZ());
	glBegin(GL_LINE_STRIP);
	glEnable(GL_LINE_SMOOTH);
	glLineWidth(2.0);
	for(long i = 0; i < (long)TabPointsOfCurve.size(); i++){
		glVertex3f(TabPointsOfCurve.at(i).getX(), TabPointsOfCurve.at(i).getY(), TabPointsOfCurve.at(i).getZ());
	}
	glEnd();
}

void DrawSurface(vector<vector<Point>> CurveSet, Vector Color){
	for(int i = 0; i < (int)CurveSet.size(); i++){
		DrawCurve(CurveSet.at(i), Color);
	}
}

void DrawPoly(vector<Point> pts, Vector Color){
	glColor4f(Color.getX(), Color.getY(), Color.getZ(), 0.5f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
	glBegin(GL_POLYGON);
	for(int i = 0; i < (int)pts.size(); i++){
		glVertex3f(pts.at(i).getX(), pts.at(i).getY(), pts.at(i).getZ());
	}
	glEnd();
	glDisable(GL_BLEND);
}

void DrawQuadAlpha(Point A, Point B, Point C, Point D, Vector Color){
	glColor4f(Color.getX(), Color.getY(), Color.getZ(), 0.3f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
	glBegin(GL_QUADS);
	glVertex3f(A.getX(), A.getY(), A.getZ());
	glVertex3f(B.getX(), B.getY(), B.getZ());
	glVertex3f(C.getX(), C.getY(), C.getZ());
	glVertex3f(D.getX(), D.getY(), D.getZ());
	glEnd();
	glDisable(GL_BLEND);
}

void DrawQuad(Point A, Point B, Point C, Point D, Vector Color){
	glColor4f(Color.getX(), Color.getY(), Color.getZ(), 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
	glBegin(GL_QUADS);
	glVertex3f(A.getX(), A.getY(), A.getZ());
	glVertex3f(B.getX(), B.getY(), B.getZ());
	glVertex3f(C.getX(), C.getY(), C.getZ());
	glVertex3f(D.getX(), D.getY(), D.getZ());
	glEnd();
	glDisable(GL_BLEND);
}

// Chaque vector<Point> est une face du cube
vector< vector<Point> > ComputeCube(Point O, float longueur){
	vector< vector<Point> > cube;
	Point A = Point( O.getX() + longueur, O.getY() + longueur,O.getZ()-longueur);    // Top Right Of The Quad (Top)
    Point B = Point(O.getX()-longueur, O.getY()+longueur,O.getZ()-longueur);    // Top Left Of The Quad (Top)
    Point C = Point(O.getX()-longueur, O.getY()+longueur, O.getZ()+longueur);    // Bottom Left Of The Quad (Top)
    Point D = Point(O.getX()+ longueur, O.getY()+longueur, O.getZ()+longueur);    // Bottom Right Of The Quad (Top)
    vector<Point> Top; 
    Top.push_back(A);Top.push_back(B);
    Top.push_back(C);Top.push_back(D);
    cube.push_back(Top);


    Point E = Point(O.getX()+ longueur,O.getY()-longueur, O.getZ()+longueur);    // Top Right Of The Quad (Bottom)
   	Point F = Point(O.getX()-longueur,O.getY()-longueur, O.getZ()+longueur);    // Top Left Of The Quad (Bottom)
    Point G = Point(O.getX()-longueur,O.getY()-longueur,O.getZ()-longueur);    // Bottom Left Of The Quad (Bottom)
   	Point H = Point( O.getX()+longueur,O.getY()-longueur,O.getZ()-longueur);    // Bottom Right Of The Quad (Bottom)
   	vector<Point> Bottom; 
    Bottom.push_back(E);Bottom.push_back(F);
    Bottom.push_back(G);Bottom.push_back(H);
    cube.push_back(Bottom);


    Point M = Point( O.getX()+longueur,O.getY()-longueur,O.getZ()-longueur);    // Top Right Of The Quad (Back)
    Point N = Point(O.getX()-longueur,O.getY()-longueur,O.getZ()-longueur);    // Top Left Of The Quad (Back)
    Point O2 = Point(O.getX()-longueur, O.getY()+longueur,O.getZ()-longueur);    // Bottom Left Of The Quad (Back)
    Point P = Point( O.getX()+longueur, O.getY()+longueur,O.getZ()-longueur);    // Bottom Right Of The Quad (Back)
    vector<Point> Back;
    Back.push_back(M);Back.push_back(N);
    Back.push_back(O2);Back.push_back(P);
    cube.push_back(Back);

    Point Q = Point(O.getX()-longueur, O.getY()+longueur, O.getZ()+longueur);    // Top Right Of The Quad (Left)
    Point R = Point(O.getX()-longueur, O.getY()+longueur,O.getZ()-longueur);    // Top Left Of The Quad (Left)
    Point S = Point(O.getX()-longueur,O.getY()-longueur,O.getZ()-longueur);    // Bottom Left Of The Quad (Left)
    Point T = Point(O.getX()-longueur,O.getY()-longueur, O.getZ()+longueur);    // Bottom Right Of The Quad (Left)
    vector<Point> Left;
    Left.push_back(Q);Left.push_back(R);
    Left.push_back(S);Left.push_back(T);
    cube.push_back(Left);

    Point U = Point( O.getX()+longueur, O.getY()+longueur,O.getZ()-longueur);    // Top Right Of The Quad (Right)
    Point V = Point( O.getX()+longueur, O.getY()+longueur, O.getZ()+longueur);    // Top Left Of The Quad (Right)
    Point W = Point( O.getX()+longueur,O.getY()-longueur, O.getZ()+longueur);    // Bottom Left Of The Quad (Right)
    Point X = Point( O.getX()+longueur,O.getY()-longueur,O.getZ()-longueur);
    vector<Point> Right;
    Right.push_back(U);Right.push_back(V);
    Right.push_back(W);Right.push_back(X);
    cube.push_back(Right);

    Point I = Point( O.getX()+longueur, O.getY()+longueur, O.getZ()+longueur);    // Top Right Of The Quad (Front)
    Point J = Point(O.getX()-longueur, O.getY()+longueur, O.getZ()+longueur);    // Top Left Of The Quad (Front)
    Point K = Point(O.getX()-longueur,O.getY()-longueur, O.getZ()+longueur);    // Bottom Left Of The Quad (Front)
    Point L = Point( O.getX()+longueur,O.getY()-longueur, O.getZ()+longueur);    // Bottom Right Of The Quad (Front)
    vector<Point> Front;
    Front.push_back(I);Front.push_back(J);
    Front.push_back(K);Front.push_back(L);
    cube.push_back(Front);

    return cube;
}

void DrawCube(vector< vector<Point> > cube, Vector Color){
	for(int i = 0; i < (int)cube.size(); i++){
		Vector AB = ComputeVector(cube.at(i).at(0), cube.at(i).at(1));
		Vector AC = ComputeVector(cube.at(i).at(0), cube.at(i).at(3));
		Vector N  = ComputeNormaleSurface(AB, AC);
		N.Normalize();

		// printf("Normale du plan %d : %f %f %f\n", i, N.getX(), N.getY(), N.getZ());

		glNormal3f(N.getX(), N.getY(), N.getZ());
		glBegin(GL_QUADS);
		glColor4f(Color.getX(),Color.getY(),Color.getZ(), 1.0f);
		for(int j = 0; j < (int)cube.at(i).size(); j++){
			Point Tmp = cube.at(i).at(j);
			glVertex3f(Tmp.getX(), Tmp.getY(), Tmp.getZ());
		}
		glEnd();
	}
}

// Calcule les coordonnées d'un Vector à partir de deux Points
Vector ComputeVector(Point A, Point B){
	double x = B.getX() - A.getX();
	double y = B.getY() - A.getY();
	double z = B.getZ() - A.getZ();
	return Vector(x, y, z);
}

// Retourne la normale d'un plan défini par les vecteurs AB et AC
Vector ComputeNormaleSurface(Vector AB, Vector AC){
	return AB.Vectoriel(AC);
}

void DrawCylinder(vector<vector<Point> > cylinder, Vector Color){
	for(int i = 0; i < (int)cylinder.at(0).size() - 1; i++){
		DrawQuad(cylinder.at(1).at(i+1), cylinder.at(0).at(i+1), cylinder.at(0).at(i), cylinder.at(1).at(i), Color);
	}
	DrawQuad(cylinder.at(0).at(cylinder.at(0).size() - 1), cylinder.at(1).at(cylinder.at(0).size() - 1), cylinder.at(1).at(0), cylinder.at(0).at(0), Color);
}


void DrawMeridien(vector< vector<Point> > cylinder, Vector Color){
	glColor3f(Color.getX(), Color.getY(),Color.getZ());
	for(int i = 0; i < (int)cylinder.at(0).size(); i++){
		DrawLine(cylinder.at(0).at(i), cylinder.at(1).at(i), Color);
	}
}


void DrawMeridienCone(vector< vector<Point> > cylinder, Vector Color){
	glColor3f(Color.getX(), Color.getY(),Color.getZ());
	for(int i = 0; i < (int)cylinder.at(1).size(); i++){
		DrawLine(cylinder.at(1).at(i), cylinder.at(0).at(0), Color);
	}
}

void DrawTriangle(Point A, Point B, Point C, Vector Color){
	glColor4f(Color.getX(), Color.getY(), Color.getZ(), 1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
	glBegin(GL_TRIANGLES);
	glVertex3f(A.getX(), A.getY(), A.getZ());
	glVertex3f(B.getX(), B.getY(), B.getZ());
	glVertex3f(C.getX(), C.getY(), C.getZ());
	glEnd();
	glDisable(GL_BLEND);
}

void DrawCone(vector< vector<Point> > cone, Vector ColorB, Vector ColorM, Vector ColorF){
	DrawMeridienCone(cone, ColorM);
	DrawPoly(cone.at(1), ColorB);
	int i = 0;
	for(i = 0; i < (int)cone.at(1).size() - 1; i++){
		DrawTriangle(cone.at(1).at(i), cone.at(1).at(i+1), cone.at(0).at(0), ColorF);
	}
	DrawTriangle(cone.at(1).at(i), cone.at(1).at(0), cone.at(0).at(0), ColorF);
}

void DrawMeridienSphere(vector<vector<Point> > sphere, Vector ColorM){
	for(int i = 0; i < M; i++){
		//cout << "sphere.at(" << i << ") = " << sphere.at(i).size() << endl;
		for(int j = 1; j < (int)sphere.at(i).size() - 1; j++){
			//cout << "\r" << j << endl;
			DrawLine(sphere.at(i).at(j-1),sphere.at(i).at(j),ColorM);
		}
	}
}

void DrawParalleleSphere(vector<vector<Point> > sphere, Vector ColorP){
	for(int j = 0; j < P ; j++){
		for(int i = 0; i < M - 1; i++){
			DrawLine(sphere.at(i).at(j), sphere.at(i+1).at(j),ColorP);
		}
		DrawLine(sphere.at(M-1).at(j), sphere.at(0).at(j),ColorP);
	}
}

void DrawFacette(vector<vector<Point> > sphere, Vector ColorF){
	for(int i = 0; i < M - 1; i++){
		for(int j = 1; j < P; j++){
			DrawQuad(sphere.at(i).at(j-1), sphere.at(i+1).at(j-1), sphere.at(i+1).at(j), sphere.at(i).at(j), ColorF);
		}
	DrawQuad(sphere.at(i).at(P-1), sphere.at(i+1).at(P-1), sphere.at(i+1).at(P), sphere.at(i).at(P), ColorF); // Je sais pas pourquoi mais ça fait le bas
}

for(int j = 0; j < P; j++){
	DrawQuad(sphere.at(M-1).at(j), sphere.at(0).at(j), sphere.at(0).at(j+1), sphere.at(M-1).at(j+1), ColorF);
}
}

void DrawSphere(vector< vector<Point> > sphere, Vector ColorM, Vector ColorP, Vector ColorF){
	DrawMeridienSphere(sphere, ColorM);
	DrawParalleleSphere(sphere, ColorP);
	DrawFacette(sphere, ColorF);
}


vector<Point> HermiteCubicCurve(Point P0, Point P1, Vector V0, Vector V1, long nbU){
	vector<Point> resultat;
	for(int i = 0; i <= (int)nbU; i++){
		float u = (float) i/(int)nbU;
		float F1 = 2 * pow(u, 3) - 3 * pow(u, 2) + 1;
		float F2 = -2 * pow(u, 3) + 3 * pow(u, 2);
		float F3 = pow(u, 3) - 2 * pow(u, 2) + u;
		float F4 = pow(u, 3) - pow(u, 2);

		float x = F1 * P0.getX() + F2 * P1.getX() + F3 * V0.getX() + F4 * V1.getX();
		float y = F1 * P0.getY() + F2 * P1.getY() + F3 * V0.getY() + F4 * V1.getY();
		float z = F1 * P0.getZ() + F2 * P1.getZ() + F3 * V0.getZ() + F4 * V1.getZ();

		Point P = Point(x, y, z);
		resultat.push_back(P);

		// cout << "Point " << i << "(" << P.getX() << ","<< P.getY() << ","<< P.getZ() << ")" << endl;
	}

	return resultat;
}

double CoeffBernstein(double i, double n,double u){
	return (double)(fact(n)/(fact(i)*fact(n-i)))*pow(double(1-u), n-i)*pow(u, i);
}

vector<Point> BezierCurveByBernstein(vector<Point> TabControlPoint, long nbU){
	vector<Point> resultat;
	long n = TabControlPoint.size() -1;
	long double u, B, Bx, By, Bz = 0.0;
// Tel que Somme 1/nbU = 1
	for(long i = 0; i < nbU; i++){
		u = (double) i/(double)(nbU  - 1.0) ;
		B = 0;
		Bx = By = Bz = B;

// Application du polynome de Bernstein au degré n
// Afin de calculer les coords des points de la courbe
		for(int j = 0; j <= n ; j ++){
			Bx+=CoeffBernstein(j,n,u)*TabControlPoint.at(j).getX();
			By+=CoeffBernstein(j,n,u)*TabControlPoint.at(j).getY();
			Bz+=CoeffBernstein(j,n,u)*TabControlPoint.at(j).getZ();
		}
		resultat.push_back(Point(Bx, By, Bz));
	}

	return resultat;
}

int fact (int n){
	int facto, i;
	facto=1;
	i=1;

	while (i<=n){
		facto=facto*i;
		i=i+1;
	}
	return facto;
}

// Application de la forme de Casteljau pour trouver les coords
// d'un point P' = (1 -u) * P0 + u * P1
Point Subdivision(double u, Point P0, Point P1){
	double x = (1-u) * P0.getX() + u * P1.getX();
	double y = (1-u) * P0.getY() + u * P1.getY();

	return Point(x, y, 0);
}

vector<Point> BezierCurveByCasteljau(vector<Point> TabControlPoint, long nbU){
	vector<Point> resultat;
	double u = (double)(1.0/(double)nbU);
// On parcours les t/nbU points de la courbe
// tel que la somme des 1/nbu = 1
	for(double t = 0; t < 1; t = t + u){
		vector<Point> Tmp;
		Tmp = TabControlPoint;
// Tant qu'on a des points intermédiaires à calculer entre les points de
// contrôles courants
		while(Tmp.size() > 1){
			vector<Point> TmpBis;
			glBegin(GL_LINE_STRIP);
			for(int i = 0 ; i < (int)Tmp.size() - 1 ; i++){
// Calcul du point intermédiaire situé à t entre P0 et P1
				Point P0 = Point(Tmp.at(i).getX(), Tmp.at(i).getY(), 0.0);
				Point P1 = Point(Tmp.at(i+1).getX(), Tmp.at(i+1).getY(), 0.0);
				TmpBis.push_back(Subdivision(t, P0, P1));
				glColor3f(0.5,0.25,0.25);
// On places les points de façon à ce que GLUT dessine les 
// segments de construction
				glVertex3f((TmpBis.at(i)).getX(), (TmpBis.at(i)).getY(),(TmpBis.at(i)).getZ());
			}
			glEnd();
			Tmp = TmpBis;
		}
		resultat.push_back(Tmp.at(0));
	}
	return resultat;
}

vector<vector<Point>> ComputeCylindricSurface(vector<Point> BezierCurve, Vector droite, int U, int V){
	vector< vector<Point> > CurveSet(V);

// Pour chaque Courbe à calculer
	for(int i = 0; i < V; i++){
//cout << "i : " << i << endl;
double v = (double)i/(double)(V-1); // Car somme(i/V-1) = 1
// Pour chaque point de la courbe
for (int j = 0; j < U; j++){
	Point B1 = BezierCurve.at(j);
	B1.setX(B1.getX() + droite.getX() * v);
	B1.setY(B1.getY() + droite.getY() * v);
	B1.setZ(B1.getZ() + droite.getZ() * v);

	CurveSet.at(i).push_back(B1);
//cout << "Fin calcul point B1 : " << B1.ToString() << endl;
// Trace de la droite directrice
	DrawVector(BezierCurve.at(j), droite,Vector(1.0, 0.0, 0.0)); 
}
}
return CurveSet;
}


vector<vector<Point>> ComputeRuledSurface(vector<Point> Curve1, vector<Point> Curve2, int U, int V, Vector color){
	vector< vector<Point> > CurveSet(V);

// Pour chaque Courbe à calculer
	for(int i = 0; i < V; i++){
double v = (double)i/(double)(V-1); // Car somme(i/V-1) = 1
// Pour chaque point de la courbe
for (int j = 0; j < U; j++){
	Point P = Curve1.at(j);
	Point Q = Curve2.at(j);
	double x = P.getX() * (1-v) + Q.getX() * v;
	double y = P.getY() * (1-v) + Q.getY() * v;
	double z = P.getZ() * (1-v) + Q.getZ() * v;

	Point R = Point(x, y, z);

	CurveSet.at(i).push_back(R);

}
}

for(int i =0; i < U;i++){
	glColor3f(color.getX(), color.getY(), color.getZ());
	glBegin(GL_LINE_STRIP);
	glVertex3f(CurveSet.at(0).at(i).getX(),CurveSet.at(0).at(i).getY(),CurveSet.at(0).at(i).getZ());
	glVertex3f(CurveSet.at(V - 1).at(i).getX(),CurveSet.at(V - 1).at(i).getY(),CurveSet.at(V - 1).at(i).getZ());
	glEnd();
}
return CurveSet;
}

vector< vector<Point> > BezierSurfaceByBernstein(vector< vector<Point> > GrilleControlPoint, long nbU, long nbV){
	vector< vector<Point> > CurveSet(nbV);

	double intervalU = 1.0/(double)(nbU-1);
	double intervalV = 1.0/(double)(nbV-1);
	long n = GrilleControlPoint.size() - 1;
	long m = GrilleControlPoint.at(0).size() - 1;


// Pour le nombre de points en U
	for(long i = 0; i < nbU; ++i){

// cout << "i = " << i;
		double u = i * intervalU;
		CurveSet.push_back(std::vector<Point>());
// Pour le nombre de points en V
		for(long j = 0; j < nbV; ++j){
// cout << " j = " << j << endl;
			double v = j * intervalV;

			Point Puv = Point(0.0,0.0,0.0);

// Somme[i=0..n](Somme[j=0..m](Sij * Bi * Bj))
			for(long k = 0; k <= n; k++){
				double Bi = CoeffBernstein(k, n, u);
				for(long l = 0; l <= m; l++){
//cout << "k : " << k << " l : " << l << endl;
					double Bj = CoeffBernstein(l, m, v);
					Point Sij = GrilleControlPoint.at(k).at(l);
// cout << "S" << k << l << " : " << Sij.ToString() << endl;

					Puv.setX(Puv.getX() + (Sij.getX() * Bi * Bj));
					Puv.setY(Puv.getY() + (Sij.getY() * Bi * Bj));
					Puv.setZ(Puv.getZ() + (Sij.getZ() * Bi * Bj));
				}
			}
			cout << "P (" << u <<", " << v << ") : " << Puv.ToString() << endl;
			CurveSet.at(i).push_back(Puv);

		}
	}
	return CurveSet;
}

vector < vector <Point> >ComputeCylinder(Point origine, int nbM, double rayon, double hauteur){

	vector<Point> SommetsHauts;
	vector<Point> SommetsBas;
	double angle = 0.0;
	double Hx, Hy , Hz, Bx, By, Bz; // H: Haut, B: Bas

	for(int i = 0; i < nbM; i++){
		angle += (2.0 * M_PI)/ nbM;
		cout << angle << endl;
		Hx = origine.getX() + rayon * cos(angle);
		Hy = origine.getY() + rayon * sin(angle);
		Hz = origine.getZ() + hauteur / 2.0;
		Point H = Point(Hx,Hy,Hz);
		SommetsHauts.push_back(H);

		Bx = origine.getX() + rayon * cos(angle);
		By = origine.getY() + rayon * sin(angle);
		Bz = origine.getZ() + - (hauteur / 2.0);
		Point B = Point(Bx,By,Bz);
		SommetsBas.push_back(B);

		cout << "H : " << H.ToString() << endl;
		cout << "B : " << B.ToString() << endl << endl;
		//DrawPoint(H, Vector(1.0,0.0,0.0));
		//DrawPoint(B, Vector(0.0,0.0,1.0));		
	}

	vector< vector<Point> > sommets;
	sommets.push_back(SommetsHauts);
	sommets.push_back(SommetsBas);

	return sommets;
}

void MeshedCylinder(Point Origine, int nbM, double rayon, double hauteur){

	int nP = nbM * 2;
	int nT = nP;
	int c = 0; // Pour passer d'un point a un autre

	GLfloat* Coords = new GLfloat[nP * 3];
	GLfloat* Normales = new GLfloat[nT * 3];
	int* ISommets = new int[nT * 3];

	// Calcul des coordonnees du haut et du bas du cylindre
	vector< vector<Point>> cylinder = ComputeCylinder(Origine, nbM, rayon, hauteur);
	vector<Point> H = cylinder.at(0);
	vector<Point> B = cylinder.at(1);

	// Recuperation des coords des points du cylindre
	for(int i = 0; i < nbM; i++){
		Coords[3 * c] = B.at(i).getX();
		Coords[3 * c + 1] = B.at(i).getZ();
		Coords[3 * c + 2] = B.at(i).getY();

		Coords[3 * c + 3] = H.at(i).getX();
		Coords[3 * c + 4] = H.at(i).getZ();
		Coords[3 * c + 5] = H.at(i).getY();
		c+=2;
	}

	for(int i = 0; i < nT; i++){
		ISommets[3 * i] = i % nT;
		ISommets[3 * i + 1] = (i+1) % nT;
		ISommets[3 * i + 2] = (i+2) % nT;

		Point A = Point(Coords[ISommets[3 * i] * 3], Coords[ISommets[3 * i] * 3 + 1], Coords[ISommets[3 * i] * 3 + 2]);
		Point B = Point(Coords[ISommets[3 * i + 1] * 3], Coords[ISommets[3 * i + 1] * 3 + 1], Coords[ISommets[3 * i + 1] * 3 + 2]);
		Point C = Point(Coords[ISommets[3 * i + 2] * 3], Coords[ISommets[3 * i + 2] * 3 + 1], Coords[ISommets[3 * i + 2] * 3 + 2]);
		Vector V1 = ComputeVector(A,B); V1.Normalize();
		Vector V2 = ComputeVector(A,C); V2.Normalize();

		Vector N = ComputeNormaleSurface(V2, V1);

		Normales[i * 3] = N.getX();
		Normales[i * 3 + 1] = N.getY();
		Normales[i * 3 + 2] = N.getZ();
	}

	ImageGaussienne(nP, nT, Coords, ISommets, Normales);

	renderMesh(Coords, Normales, ISommets, nT, Vector(1.0,0.0,0.0));
}


Point** calculeSphere(Point Origine, double rayon, int nbMeridient, int nbParallele) {
	Point** tab = new Point*[nbParallele];
	nbParallele++;
	for (int i = 0; i < nbParallele; i++) {
		tab[i] = new Point[nbMeridient];
	}
	const double PI = atan(1) * 4;
	double angleBase = 0, angleHauteur = PI / (double)(nbParallele);
	for (int i = 0; i < nbParallele; i++) {
		angleBase = 0;
		for (int j = 0; j < nbMeridient; j++) {
			double x = Origine.getX() + rayon * sin(angleHauteur) * cos(angleBase);
			double y = Origine.getY() + rayon * cos(angleHauteur);
			double z = Origine.getZ() + rayon * sin(angleHauteur) * sin(angleBase);
			tab[i][j] = Point(x, y, z);
			glVertex3f(x, y, z);
			angleBase += 2.0 * PI / (double)(nbMeridient);
		}
		angleHauteur += PI / (double)nbParallele;
	}
	return tab;
}
vector <vector<Point> > ComputeSphere(Point Origine, int nbM, int nbP, double rayon){
	Point Nord 	= Point(Origine.getX(),Origine.getY(),rayon); 
	Point Sud 	= Point(Origine.getX(),Origine.getY(), -rayon); 

	vector <vector<Point>> sphere;

	double theta = 0.0;
	double phi = 0.0;
	double Px, Py, Pz;

	for(int i = 0; i < nbM; i++){
		theta += (2.0 * M_PI) / nbM;
		vector<Point> Tmp;
		Tmp.push_back(Nord);
		for(int j = 0; j < nbP; j++){
			phi += M_PI / nbP;

			Px = rayon * sin(phi) * cos(theta);
			Py = rayon * sin(phi) * sin(theta);
			Pz = rayon * cos(phi);

			Tmp.push_back(Point(Px, Py, Pz));
		}
		Tmp.push_back(Sud);
		phi = 0.0;
		sphere.push_back(Tmp);
	}
	return sphere;
}


void MeshedSphere(Point Origine, int nbM, int nbP, int rayon,int angleMax) {
	// Construction d'une sphère
	Point ** sphere = calculeSphere(Origine,rayon, nbM, nbP);

	int nP = nbM * nbP + 2; // +2 pour Nord et Sud
	int nT = nbM * nbP * 2; 
	int c = 0; // Pour passer d'un point a un autre

	GLfloat* Coords = new GLfloat[nP * 3];
	GLfloat* Normales = new GLfloat[nT * 3];
	int* iSommets = new int[nT * 3];

	// Récupération des Coordonnées de la Sphère
	for (int i = 0; i < nbP; i++) {
		for (int j = 0; j < nbM; j++) {
			Coords[c * 3] = sphere[i][j].getX();
			Coords[c * 3 + 1] = sphere[i][j].getY();
			Coords[c * 3 + 2] = sphere[i][j].getZ();
			c++;
		}
	}
	// Sommet supérieur de la sphere
	Coords[c * 3] = Origine.getX();
	Coords[c * 3 + 1] = rayon;
	Coords[c * 3 + 2] = Origine.getZ();
	c++;
	// Sommet inférieur de la sphere
	Coords[c * 3] = Origine.getX();
	Coords[c * 3 + 1] = -rayon;
	Coords[c * 3 + 2] = Origine.getZ();

	
	c = 0;

	// Calcul des indices des points de chaque triangle
	for (int i = 0; i < nbP - 1; i++) {
		for (int j = 0; j < nbM; j++) {
			// Triangle bas gauche
			iSommets[c * 3] = i * nbM + j;
			iSommets[c * 3 + 1] = (i + 1) * nbM + j;
			if (j + 1 < nbM)
				iSommets[c * 3 + 2] = (i + 1) * nbM + j + 1;
			else
				iSommets[c * 3 + 2] = (i + 1) * nbM;
			c++;
			// Triangle Haut droite
			iSommets[c * 3] = i * nbM + j;
			if (j + 1 < nbM) {
				iSommets[c * 3 + 1] = (i + 1) * nbM + j + 1;
				iSommets[c * 3 + 2] = i * nbM + j + 1;
			}
			else {
				iSommets[c * 3 + 1] = (i + 1) * nbM;
				iSommets[c * 3 + 2] = i * nbM;
			}
			c++;
		}
	}

	// Calcul des surfaces au sommet supérieur
	for (int i = 0; i < nbM; i++) {
		iSommets[c * 3] = nP - 2;
		if (i + 1 <nbM)
			iSommets[c * 3 + 1] = i + 1;
		else
			iSommets[c * 3 + 1] = 0;
		iSommets[c * 3 + 2] = i;
		c++;
	}

	// Calcul des surfaces au sommet inférieur
	for (int i = 0; i < nbM; i++) {
		iSommets[c * 3 + 1] = (nbP - 1) * nbM + i;
		iSommets[c * 3] = nP - 1;
		if (i + 1 < nbM)
			iSommets[c * 3 + 2] = (nbP - 1) * nbM + i + 1;
		else
			iSommets[c * 3 + 2] = (nbP - 1) * nbM;
		c++;
	}

	
	// Calcul des normales de chaque surface
	for (int i = 1; i < nT; i++) {
		Point* p1 = new Point(Coords[iSommets[3 * i] * 3], Coords[iSommets[3 * i] * 3 + 1], Coords[iSommets[3 * i] * 3 + 2]);
		Point* p2 = new Point(Coords[iSommets[3 * i + 1] * 3], Coords[iSommets[3 * i + 1] * 3 + 1], Coords[iSommets[3 * i + 1] * 3 + 2]);
		Point* p3 = new Point(Coords[iSommets[3 * i + +2] * 3], Coords[iSommets[3 * i + 2] * 3 + 1], Coords[iSommets[3 * i + 2] * 3 + 2]);
		Vector v1 = ComputeVector(*p1, *p2);
		Vector v2 = ComputeVector(*p1, *p3);

		v1.Normalize();
		v2.Normalize();

		Vector N = ComputeNormaleSurface(v1,v2);

		Normales[i * 3] = N.getX();
		Normales[i * 3 + 1] = N.getY();
		Normales[i * 3 + 2] = N.getZ();

	}

	// for(int i = 0; i < nT * 3; i++){
	// 	Point P = Point(Normales[i] * 2 * rayon, Normales[i+1]* 2 * rayon, Normales[i+2] * 2 * rayon);
	// 	DrawEdge(Edge(Origine, P));
	// }

	ImageGaussienne(nP, nT, Coords, iSommets, Normales);
	//renderMesh(Coords, Normales, iSommets, nT, Vector(1.0,0.9,0.9));
}

Point ComputeBarycentre(Point A, Point B, Point C){
	return Point((A.getX()+B.getX()+C.getX())/3.0, (A.getY()+B.getY()+C.getY())/3.0, (A.getZ()+B.getZ()+C.getZ())/3.0);
}


vector < vector <Point> >ComputeCone(Point origine, Point S, int nbM, double rayon, double hauteur){
	vector<Point> SommetsHauts;
	vector<Point> SommetsBas;
	double angle = 0.0;
double Bx, By, Bz; // H: Haut, B: Bas

for(int i = 0; i < nbM; i++){
	angle += (2.0 * M_PI)/ nbM;
	Bx = origine.getX() + rayon * cos(angle);
	By = origine.getY() + rayon * sin(angle);
	Bz = origine.getZ();
	Point B = Point(Bx,By,Bz);
	SommetsBas.push_back(B);
	cout << "B : " << B.ToString() << endl << endl;
	DrawPoint(B, Vector(0.0,0.0,1.0));		
}

DrawPoint(S, Vector(1.0,0.0,0.0));
SommetsHauts.push_back(S);

vector< vector<Point> > sommets;
sommets.push_back(SommetsHauts);
sommets.push_back(SommetsBas);

return sommets;
}

void ImageGaussienne(int nbSommets, int nbSurfaces, float* coords, int* iSurfaces, GLfloat* normales){
	int n = 0;
	float x = 0, y = 0, z = 0;
	GLfloat* TempNormales = new GLfloat[ 3 * nbSommets];
	for(int i = 0; i < nbSommets; i++){
		for(int j = 0; j < nbSurfaces; j++){
			// On cherche association triangle/sommets
			if(iSurfaces[3 * j] == i || iSurfaces[3 * j + 1] == i || iSurfaces[3 * j + 2] == i){
				x += normales[3 * j];
				y += normales[3 * j + 1];
				z += normales[3 * j + 2];
				n++;
			}
		}

		TempNormales[3 * i] 	= (GLfloat) (x/n);
		TempNormales[3 * i + 1] = (GLfloat) (y/n);
		TempNormales[3 * i + 2] = (GLfloat) (z/n);
		n = 0;
		x = y = z = 0;
	}

	for(int i = 0; i < nbSommets; i++){
		Point p1 = Point(coords[3 * i] + TempNormales[3 * i], coords[3 * i + 1] + TempNormales[3 * i + 1], coords[3 * i + 2] + TempNormales[3 * i + 2]);
		Point p2 = Point(coords[3 * i], coords[3 * i + 1], coords[3 * i + 2]);
		DrawLine(p1, p2, Vector(1.0f, 1.0f, 1.0f));
	}
}


