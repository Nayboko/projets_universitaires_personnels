#include <iostream>
#include <cmath>

#include "Point.hpp"


using namespace std;

// Constructeurs et destructeur

Point::Point() : x(0.0), y(0.0), z(0.0){}

Point::Point(double _x, double _y, double _z) : x(_x), y(_y), z(_z){}

Point::Point(const Point& p){
	this->x = p.getX();
	this->y = p.getY();
	this->z = p.getZ();
}

Point::~Point(){}

// Getters et Setters

double Point::getX() const{
	return this->x;
}

void Point::setX(const double _x){
	this->x = _x;
}

double Point::getY() const{
	return this->y;
}

void Point::setY(const double _y){
	this->y = _y;
}

double Point::getZ() const{
	return this->z;
}

void Point::setZ(const double _z){
	this->z = _z;
}



// Méthodes

Point Point::ProjectOnLine(Point B, Point C){
	Vector BC = Vector(C.getX() - B.getX(), 
					   C.getY() - B.getY(),
					   C.getZ() - B.getZ());

	Vector BA = Vector(this->getX() - B.getX(),
					   this->getY() - B.getY(), 
					   this->getZ() - B.getZ());

	BC.Normalize();

	double WNorme = BA.Scalar(BC) / BC.Norme();

	return Point(B.getX() + (BC.getX() * WNorme),
	 			 B.getY() + (BC.getY() * WNorme), 
	 			 B.getZ() + (BC.getZ() * WNorme));
}

Point Point::ProjectOnLine(Vector vecteur, Point PLine){
	vecteur.Normalize();
	Point P = Point(PLine.getX() + vecteur.getX(),
				    PLine.getY() + vecteur.getY(),
				    PLine.getZ() + vecteur.getZ());

	return this->ProjectOnLine(PLine, P);
}

Point Point::ProjectOnPlane(Point PointOnPlane, Vector NormalOfPlane){
	Vector MA = Vector(PointOnPlane.getX() - this->getX(),
					  PointOnPlane.getY() - this->getY(),
					  PointOnPlane.getZ() - this->getZ());
	MA.Normalize();
	NormalOfPlane.Normalize();
	double NormeMM = abs(MA.Scalar(NormalOfPlane) / NormalOfPlane.Norme());
	std::cout << "|MM| = " << NormeMM << std::endl;

	return Point(this->getX() - (NormalOfPlane.getX() * NormeMM),
				 this->getY() - (NormalOfPlane.getY() * NormeMM),
				 this->getZ() - (NormalOfPlane.getZ() * NormeMM));
}

std::string Point::ToString() const{
	return "(" + to_string(this->getX()) + ", " + to_string(this->getY()) + ", " + to_string(this->getZ()) + ")";
}