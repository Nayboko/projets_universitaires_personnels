#ifndef POINT_HPP
#define POINT_HPP

#include "Vector.hpp"
#include <string>

class Point
{
protected:
	double x;
	double y;
	double z;

public:
	Point();
	Point(double _x, double _y, double _z);
	Point(const Point& p);
	~Point();
	
	// Getters et Setters

	double getX() const;
	void setX(const double _x);

	double getY() const;
	void setY(const double _y);

	double getZ() const;
	void setZ(const double _z);


	// Méthodes

	Point ProjectOnLine(Point Point1Line, Point Point2Line);

	Point ProjectOnLine(Vector vecteur, Point PLine);

	Point ProjectOnPlane(Point PointOnPlane, Vector NormalOfPlane);

	std::string ToString() const;

};

#endif