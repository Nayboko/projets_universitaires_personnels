#ifndef VECTOR_HPP
#define VECTOR_HPP


class Vector
{
protected:
	double x;    // coord sur l'axe Ox 
	double y;    // coord sur l'axe Oy
	double z;    // coord sur l'axe Oz

public:

	Vector();
	Vector(double _x, double _y, double _z);
	Vector(const Vector& p);
	~Vector();
	
	// Getters et Setters

	double getX() const;
	void setX(const double _x);

	double getY() const;
	void setY(const double _y);

	double getZ() const;
	void setZ(const double _z);

	// Méthodes

	double Norme();

	void Normalize();

	Vector CpNormalize();

	double Scalar(Vector Vector2);

	Vector Vectoriel(Vector Vector2);

	double Angle(Vector Vector2);

	double RadToDeg(double radian);

	double DegToRad(double degree);

};


#endif