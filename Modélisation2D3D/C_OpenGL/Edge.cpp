#include <iostream>
#include <cmath>

#include "Edge.hpp"

	Edge::Edge(){
		this->From = Point(0.0,0.0,0.0);
		this->To = Point(1.0,1.0,0.0);
		this->Color = Vector(1.0,1.0,1.0);
	}

	Edge::Edge(Point f, Point t){
		this->From = f;
		this->To = t;
		this->Color = Vector(1.0,1.0,1.0);
	}

	Edge::Edge(Point f, Point t, Vector c){
		this->From = f;
		this->To = t;
		this->Color = c;
	}

	Edge::~Edge(){}

	Point Edge::getFrom() const{
		return this->From;
	}
	void Edge::setFrom(const Point f){
		this->From = f;
	}

	Point Edge::getTo() const{
		return this->To;
	}

	void Edge::setTo(const Point t){
		this->To = t;
	}

	Vector Edge::getColor() const{
		return this->Color;
	}

	void Edge::setColor(const Vector c){
		this->Color = c;
	}

	std::string Edge::toString(){
		std::string s = ""; s += "[";
		s+= this->From.ToString();
		s+= " ; ";
		s+= this->To.ToString();
		s+= "]";
		return s;
	}

