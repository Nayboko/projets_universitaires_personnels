#ifndef EDGE_HPP
#define EDGE_HPP

#include "Point.hpp"
#include "Vector.hpp"
#include <string>

class Edge{

protected:
	Point From;
	Point To;
	Vector Color;

public:
	Edge();
	Edge(Point f, Point t);
	Edge(Point f, Point t, Vector c);
	~Edge();

	Point getFrom() const;
	void setFrom(const Point f);

	Point getTo() const;
	void setTo(const Point t);

	Vector getColor() const;
	void setColor(const Vector Color);

	std::string toString();

};

#endif