#include <iostream>
#include <cmath>

#include "Vector.hpp"

#define PI 3.141592653589793238463

using namespace std;

// Constructeurs et destructeur

Vector::Vector() : x(0.0), y(0.0), z(0.0){}

Vector::Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z){}

Vector::Vector(const Vector& p){
	this->x = p.getX();
	this->y = p.getY();
	this->z = p.getZ();
}

Vector::~Vector(){}

// Getters et Setters

double Vector::getX() const{
	return this->x;
}

void Vector::setX(const double _x){
	this->x = _x;
}

double Vector::getY() const{
	return this->y;
}

void Vector::setY(const double _y){
	this->y = _y;
}

double Vector::getZ() const{
	return this->z;
}

void Vector::setZ(const double _z){
	this->z = _z;
}

// Méthodes

double Vector::Norme(){
	return sqrt(pow(this->getX(), 2) + pow(this->getY(), 2) + pow(this->getZ(),2));
}

void Vector::Normalize(){
	double norme = this->Norme();
	this->setX(this->getX() / norme);
	this->setY(this->getY() / norme);
	this->setZ(this->getZ() / norme);
}

Vector Vector::CpNormalize(){
	double norme = this->Norme();
	return Vector(this->getX()/norme, this->getY()/norme, this->getZ()/norme);
}

double Vector::Scalar(Vector Vector2){
	return this->getX() * Vector2.getX() + this->getY() * Vector2.getY() + this->getY() * Vector2.getY(); 
}

Vector Vector::Vectoriel(Vector Vector2){
	double Vx = this->getY() * Vector2.getZ() - this->getZ() * Vector2.getY();
	double Vy = this->getZ() * Vector2.getX() - this->getX() * Vector2.getZ();
	double Vz = this->getX() * Vector2.getY() - this->getY() * Vector2.getX();

	return Vector(Vx, Vy, Vz);
}

double Vector::RadToDeg(double radian){
	return radian * (180/PI);
}

double Vector::DegToRad(double degree){
	return degree / (180/PI);
}

double Vector::Angle(Vector Vector2){
	return acos(this->Scalar(Vector2) / (this->Norme() * Vector2.Norme()));
}

