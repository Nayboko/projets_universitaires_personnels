# HereOuiGo : La plateforme de covoiturage communautaire

## Utilisation de l'application

### Installation du Serveur Web

Veuillez disposer d'un serveur Web (Apache par exemple) lié à une base de donnée MySQL et un moteur d'interprétation PHP 7.1 et d'un accès à PHPmyAdmin.

Voici une liste de plateforme de développement en fonction de votre système d'exploitation :
* **WINDOWS** : [WAMP](http://www.wampserver.com/) 
* **LINUX** : [XAMPP](https://www.apachefriends.org/fr/index.html)
* **MAC OS** : [MAMP](https://www.mamp.info/en/)

Vous pouvez également installer manuellement chaque composante. Pensez à sécuriser vos accès FTP, MySQL et PHPMyAdmin.

### Installation de l'application

Rendez-vous sur votre interface PHPMyAdmin puis créez une nouvelle *BD bdd_hereouigo*;
Aller dans l'onglet "Importer" de votre nouvelle BD puis selectionner le fichier *./BD/bdd_hereouigo.sql*;
Dans le fichier ./BD/info_bd.php, changez éventuellement le $username et le $password si besoin est pour la connexion à la BD MySQL;

Placer le dossier HereOuiGo à la racine de votre dossier htdocs (Emplacement pouvant différent en fonction de votre configuration);

Une fois fait, vous pouvez naviguer sur l'Application à partir de la racine du projet.

## Problèmes à résoudre

// A venir