<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");
?>
<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>

<?php 
	if(isset($_POST['avis']) && isset($_SESSION['auth'])){
		// Connexion à la BDD
		$offre = $_POST['avis'];
		$email = $_SESSION['mail'];
		$commentaire = $_POST['commentaire'];
		$note = intval($_POST['note']);

		try{
			$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
			$req = $bdd->prepare("INSERT INTO avis(id_offre, id_membre, note, commentaire) VALUES(:id_offre, :id_membre, :note, :commentaire);");
			if($req->execute(array("id_offre" => $offre, "id_membre" => $email, "note" => $note, "commentaire" => $commentaire))){
				header("Location: leave_avis.php");

			}else{
				echo "
	            <div class='error_box'>
	            <p>Erreur dans l'insertion de votre avis dans la base de donnée.</p>
	            <p><a href='leave_avis.php'> Retourner à vos avis </a></p>
	            </div>";
			}
			// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
			$req->closeCursor();
 
			// Déconnexion de la BDD
			unset( $bdd );
		}catch(PDOException $e){
            print"Erreur ! : ".$e->getMessage()."</br>";
            die();
        }




	}else{
		echo "
            <div class='error_box'>
            <p>Vous n'avez pas accès à cette demande.</p>
            <p><a href='index.php'> Retourner à l'accueil </a></p>
            </div>";
	}
	



?>

</body>
