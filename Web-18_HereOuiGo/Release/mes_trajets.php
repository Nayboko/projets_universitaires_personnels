<?php
    error_reporting(E_ALL);
    empty($_SESSION)? session_start() : print "";
    include("./BD/info_bd.php");
    function afficher_data($data){
        print_r($data);
    }
?>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title> HereOuiGo - voyagez tranquille </title>
        <link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
        <link rel="stylesheet" href="styles.css"/>
        <script src="./Scripts/monscript.js"></script>

        <!--[if lt IE 9]>
            <script src="./Scripts/html5shiv.js"></script>
        <![endif] -->
    </head>
    <body>

    <?php
        include("./include/header.php");
    ?>
    <div id="main">
        <h2>Mes trajets</h2>
    <?php
        if(isset($_SESSION['auth'])){
            $email=$_SESSION['mail'];
            $now = date("Y-m-d");
            try{
                // Connexion à la BDD
                $bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);

                /* Trajets en tant que conducteur */
                echo "<h3>Les trajets que je propose</h3>";
                $req_verif =$bdd->prepare("SELECT * 
                                            FROM offre, trajet
                                            WHERE offre.id_membre = :email
                                            AND offre.id_trajet=trajet.id_trajet AND trajet.date_trajet >= :now;");
                if($req_verif->execute(array('email' => $email, 'now' => $now))){
                    $offres=$req_verif->fetchAll();
                     // On récupère les trajets dans lequel l'user est conducteur
                    if(count($offres) > 0){
                        
                        echo "<table class='avis_passager'>";
                        echo "  <tr>
                                    <th>Info</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>";
                        foreach($offres as $offre){
                            // On affiche les infos de chaque offre
                            echo "<tr>";
                            echo "  <td class='info_passager'><p><strong class='bold_vd'>{$offre['ville_depart']}</strong> → <strong class='bold_va'>{$offre['ville_arrivee']}</strong></p><p>Avec votre voiture immatriculée {$offre['id_voiture']}</p></td>

                                    <td><p>{$offre['date_trajet']}</p></td>

                                    <td style='text-align:center;'><form action='delete_trajet.php' method='post' id={$offre['id_offre']}>
                                            <button type='submit' name='id_offre' value={$offre['id_offre']} form={$offre['id_offre']}>Annuler</button>
                                        </form></td>";
                            
                            // Puis pour chaque offre, on va chercher les passagers
                            $req_verif =$bdd->prepare("SELECT membre.pseudo, membre.mail, membre.numero
                                                FROM membre, offre, passager
                                                WHERE membre.mail = passager.mail
                                                AND passager.id_offre = offre.id_offre
                                                AND offre.id_offre = :id_offre;");
                            if($req_verif->execute(array("id_offre" => $offre['id_offre']))){
                                $passagers = $req_verif->fetchAll();
                                if(count($passagers) > 0){
                                    foreach($passagers as $passager){
                                        echo "<tr>
                                                <td><a href='profil.php?pseudo={$passager['pseudo']}'>{$passager['pseudo']}</a></td>
                                                <td>{$passager['mail']}</td>
                                                <td>{$passager['numero']}</td>                      
                                            <tr>";
                                    }

                                }else{
                                    echo "<tr>
                                            <td>Vous n'avez aucun passager pour cette offre ! </td>
                                         </tr>";
                                }
                                echo "</tr>";
                            }else{
                                echo "
                                    <tr>
                                        <td><p>Vous n'avez aucun trajet en cours !<p>
                                        <p><a href='add_trajet.php' class='green_button'>Proposez un trajet !</a></p> </td>
                                    </tr>";
                            } 
                        }
                        echo "</table>";
                    }else{
                        echo "
                            <div class='error_box'>
                            <p>Vous n'avez aucun trajet en cours en tant que conducteur !</p>
                            <p><a href='add_trajet.php' class='green_button'>Proposez un trajet !</a></p>
                            </div>";
                    }
                    
                    //afficher_data($data);
                }
                else{
                    echo "
                        <div class='error_box'>
                        <p>Une erreur s'est produite lors de l'execution de votre demande, veuillez <a href='contact.php'>contacter un administrateur !</a></p>
                        </div>";
                }

                /* Trajets en tant que passagers */
                echo "<h3>Les trajets pour lesquels je suis passager</h3>";

                $req = $bdd->prepare("SELECT * FROM passager, offre, trajet 
                                        WHERE passager.mail = :email
                                        AND passager.id_offre = offre.id_offre
                                        AND offre.id_trajet = trajet.id_trajet
                                        AND trajet.date_trajet >= :now");
                if($req->execute(array("email" => $email, "now" => $now))){
                    $offres = $req->fetchAll();
                    if(count($offres) >0){
                        echo "<table class='avis_passager'>";
                        echo "  <tr>
                                    <th>Info</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>";
                        foreach($offres as $offre){
                            /* On cherche les infos du conducteur */
                            $req = $bdd->prepare("SELECT membre.pseudo, membre.numero FROM membre WHERE membre.mail = :conducteur");
                            $req->execute(array("conducteur" => $offre['id_membre']));
                            $conducteur = $req->fetch();

                            echo "<tr>";
                            echo "  <td><p><strong class='bold_vd'>{$offre['ville_depart']}</strong> → <strong class='bold_va'>{$offre['ville_arrivee']}</strong></p>
                                        <p>Proposé par <a href='profil.php?pseudo={$conducteur['pseudo']}' title='Consulter le profil de {$conducteur['pseudo']}'>{$conducteur['pseudo']}</a></p><p>Contact : {$conducteur['numero']}</p></td>

                                    <td><p>{$offre['date_trajet']}</p></td>

                                    <td><form action='delete_passager.php' method='post' id={$offre['id_offre']}>
                                            <button type='submit' name='id_offre' value={$offre['id_offre']} form={$offre['id_offre']}>Se désinscrire</button>
                                        </form></td>";
                            echo "</tr>";
                        }

                    }else{
                        echo "
                            <div class='error_box'>
                            <p>Vous n'avez aucun trajet en cours en tant que passager !</p>
                            <p><a href='rechercher_trajet.php' class='green_button'>Consulter les trajets</a></p>
                            </div>";
                    }

                }else{
                    echo "
                        <div class='error_box'>
                        <p>Une erreur s'est produite lors de l'execution de votre demande, veuillez <a href='contact.php'>contacter un administrateur !</a></p>
                        </div>";
                }
                // On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
                $req_verif->closeCursor();
                $req->closeCursor();
 
                // Déconnexion de la BDD
                unset( $bdd );


            }
            catch(PDOException $e){
                print"Erreur ! : ".$e->getMessage()."</br>";
                die();
            }
        }
        else{
            echo "
                <div class='error_box'>
                <p>Vous n'avez pas accès à cette demande.</p>
                <a href='index.php'> Retourner à l'accueil </a>
                </div>";            
        }
    ?>

       
    </div>
        <?php
            include("./include/footer.php");
        ?>
    </body>
</html>