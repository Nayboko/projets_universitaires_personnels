<header>
	<a href="./index.php"><div id="titre">
		<img id="logo" src="./WebContents/icon.png"/>
		<h1> HereOuiGo </h1>
	</div></a>
	<nav id="top-menu">
		<a href="./index.php"> Accueil </a>
		<?php
			if(isset($_SESSION['auth'])){
				echo '<a href="profil.php?pseudo='.$_SESSION['pseudo'].'"> Profil </a>';
			}
		?>

		<a href="./rechercher_trajet.php"> Offres </a>
		<a href="./contact.php"> Contacts </a>
	</nav>

	<?php  
		
		if(!empty($_SESSION) && isset($_SESSION['mail']) && isset($_SESSION['admin'])){
			echo "<div id='right-menu'>
			<a  href='index.php?admin=TRUE'><button  id='admin'>Administration</button></a>

			<a href='./deconnexion.php'><button id='logout'>Déconnexion</button></a>
			<p style='font-size: 0.8em'>Bienvenue {$_SESSION['pseudo']}!<p>
			</div>";
		}elseif(!empty($_SESSION) && isset($_SESSION['mail'])){
			echo "<div id='right-menu'>
			<a href='./deconnexion.php'><button id='logout'>Déconnexion</button></a>
			<p style='font-size: 0.8em'>Bienvenue {$_SESSION['pseudo']}!<p>
			</div>";}

		else{
			echo '
			<div id="right-menu">
			<a href="./connexion.php"><button id="login">Connexion</button></a>
			<a href="./inscription.php"><button id="inscription">Inscription</button></a>
			</div>';
		}
	?>
	<img id="banniere" src="./WebContents/banniere.png"/>
</header>