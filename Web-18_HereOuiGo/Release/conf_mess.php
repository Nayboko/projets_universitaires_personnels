<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
?>
?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>		
		<?php
			include("./include/header.php");
		?>
		<div id="main">
			<div class='valid_box'>
				<p> Votre message a bien été envoyé !</p>
			</div>
		</div>
		<?php
			include("./include/footer.php");
		?>
	</body>
</html>