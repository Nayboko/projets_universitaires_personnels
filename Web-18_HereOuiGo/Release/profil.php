<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");

	function afficher_membre($data){
		global $username, $password;

		/* ### Informations Générales ### */
		echo "<div class='info_profil' id='info_generales_profil'>
			<h3> Informations Générales </h3>
			<ul>
				<li><p>Pseudo : ".$data['pseudo']."</p></li>
				<li><p> Nom : ".$data['nom']."</p></li>
				<li><p> Prénom : ".$data['prenom']."</p></li>
				<li><p> E-mail : ".$data['mail']."</p></li>";
		if($data['pseudo'] == $_SESSION['pseudo'] || isset($_SESSION['admin'])){
			echo"<li><p> Date de naissance : ".$data['date_naissance']."</p></li>
			<li><p> Numéro de téléphone : ".$data['numero']."</p></li>
			";
			if($data['pseudo'] == $_SESSION['pseudo'])
				echo "</ul>
				<form action='#' method=post id='alter_info'>
					<button type='submit' name='immatriculation' value=".$data['pseudo']." form='alter_info'> Modifier </button>
				</form>
				</div>";

			/* ### Adresse postales ### */
			echo "<div class='info_profil' id='info_adresse_profil'>";
			echo "<h3>Adresse</h3>";
			echo "<ul>";
			echo "<li><p>".$data['numero_voie']." ".$data['nom_voie']."</p></li>";
			echo "<li><p>".$data['complement_adresse']."</p></li>";
			echo "<li><p>".$data['code_postal']." ".$data['ville']."</p></li>";
			echo "</ul>";
			if($data['pseudo'] == $_SESSION['pseudo'])
				echo "<form action='#' method=post id='alter_adresse'>
					<button type='submit' name='immatriculation' value=".$data['pseudo']." form='alter_adresse'> Modifier </button>
					</form>";
			echo "</div>";


			/* ### Véhicules enregistrés ### */

			try{
				// Connexion à la BDD
				$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
				$mail = $data['mail'];
				// Vérifier que le membre existe déjà dans la BDD
				$req_verif =$bdd->prepare("SELECT vehicule.immatriculation, vehicule.marque, vehicule.modele, vehicule.type_carburant, vehicule.nombre_place 
											FROM membre, membre_vehicule, vehicule
											WHERE membre.mail = membre_vehicule.mail
											AND membre_vehicule.immatriculation = vehicule.immatriculation
											AND membre.mail = :mail;");

				if($req_verif->execute(array('mail' => $mail))){
					$voitures = $req_verif->fetchAll();
					echo "<div class='info_profil' id='info_voiture_profil'>";
						echo "<h3>Vos vehicules</h3>";
					if(count($voitures) > 0){
						
						echo "<table>
								<tr>
									<th>ID</th>
									<th>Marque</th>
									<th>Modèle</th>
									<th>Carburant</th>
									<th>Places</th>
								</tr>";

						foreach($voitures as $row){
							echo "<tr>";
							echo "<td><p>".$row['immatriculation']."<p></td>";
							echo "<td><p>".$row['marque']."<p></td>";
							echo "<td><p>".$row['modele']."<p></td>";
							echo "<td><p>".$row['type_carburant']."<p></td>";
							echo "<td><p>".$row['nombre_place']."<p></td>";
							if($data['pseudo'] == $_SESSION['pseudo'])
								echo "<td><form action='delete_vehicule.php' method='post' id=".$row['immatriculation'].">
										<button type='submit' name='immatriculation' value=".$row['immatriculation']." form=".$row['immatriculation']."> Supprimer </button>
											</form></td>";
							echo "</tr>";
						}
						echo "</table>";
						echo '<p><a href="add_vehicule.php" id="but_info_trajet">Ajouter un vehicule</a></p>';
					}
					else{
						echo "<p>Aucun vehicule n'est enregistré sur votre profil !</p>";
						echo '<p><a href="add_vehicule.php" id="but_info_trajet">Ajouter un vehicule</a></p>';
					}
					echo "</div>";
				}
				else{
					echo "
					<div class='error_box'>
					<p>Une erreur s'est produite, veuillez <a href='contact.php'>contacter un administrateur</a> ! {$mail}</p>
					</div>";
				}
				// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation
				$req_verif->closeCursor();
 
				// Déconnexion de la BDD
				unset( $bdd );
			}
			catch(PDOException $e){
				print"Erreur ! : ".$e->getMessage()."</br>";
				die();
			}
		}

		/* ### Mes trajets ### */
	}
?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>

	<?php
		include("./include/header.php");
	?>
	<div id="main">
	<?php
		if(isset($_GET['pseudo']) && isset($_SESSION['auth'])){
			$pseudo=$_GET['pseudo'];
			try{
				// Connexion à la BDD
				$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
				// Vérifier que le membre existe déjà dans la BDD
				$req_verif =$bdd->prepare("SELECT * 
							FROM membre, adresse
							WHERE pseudo = :pseudo
							AND membre.fk_adresse = adresse.id_adresse;");

				if($req_verif->execute(array('pseudo' => $pseudo))){
					$data = $req_verif->fetch();
					if(count($data) > 1){
						afficher_membre($data);
					}
					else{
						echo "
							<div class='error_box'>
							<p>Cet utilisateur n'est pas référencé dans la base de donnée.</p>
							<p><a href='index.php'>Retourner à l'accueil</a></p>
							</div>";
					}
				}
				else{
					echo "
						<div class='error_box'>
						<p>Une erreur s'est produite, veuillez réessayer !</p>
						<p><a href='index.php'>Retourner à l'accueil</a></p>
						</div>";
				}
				// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation
				$req_verif->closeCursor();
 
				// Déconnexion de la BDD
				unset( $bdd );
			}
			catch(PDOException $e){
				print"Erreur ! : ".$e->getMessage()."</br>";
				die();
			}
		}
		elseif(isset($_SESSION['auth'])){
			header("Location : index.php");
			exit();
		}
		else{
				echo "
					<div class='error_box'>
					<p>Vous n'avez pas accès à cette demande.</p>
					<a href='index.php'> Retourner à l'accueil </a>
					</div>";
		}
	?>

		
	</div>
		<?php
			include("./include/footer.php");
		?>
	</body>
</html>
