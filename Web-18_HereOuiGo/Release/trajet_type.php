<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");
?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>
		<?php
			include("./include/header.php");
		?>
		<div id="main">
			<?php
			if(isset($_POST['submit']) && isset($_SESSION['admin'])){
				$v_depart=strtoupper($_POST['ville_dep']);
				$v_arrivee=strtoupper($_POST['ville_arr']);
				$distance=intval($_POST['distance']);
				$t_moyen=$_POST['time'];
				try{
					// Connexion à la BDD
					$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
					$req_verif = $bdd->prepare("INSERT INTO trajet_type (ville_depart, ville_arrivee, distance, temps_moyen) VALUES(:v_depart, :v_arrivee, :distance, :t_moyen);");

					if($req_verif->execute(array('v_depart'=>$v_depart, 'v_arrivee'=>$v_arrivee, 'distance'=>$distance, 't_moyen'=>$t_moyen))){
							echo "
								<div class='valid_box'>
								<p>Votre trajet type a bien été enregistré !</p>
								</div>";
					}
					else{
						include("./include/formulaire_trajet_type.php");
						print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre trajet type.</p></div>";
						exit();
					}
					// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation
					$req_verif->closeCursor();
 
					// Déconnexion de la BDD
					unset( $bdd );
				}
				catch(PDOException $e){
					print "<p>Erreur ! : ".$e->getMessage()."</p></br>";
					die();
				}

			}
			elseif(isset($_SESSION['admin'])){
				include("./include/formulaire_trajet_type.php");
			}
			else{
				echo "
					<div class='error_box'>
					<p>Vous n'avez pas accès à cette demande.</p>
					<a href='index.php'> Retourner à l'accueil </a>
					</div>";
			}
		?>

		</div>
		<?php
			include("./include/footer.php");
		?>
	</body>
</html>
