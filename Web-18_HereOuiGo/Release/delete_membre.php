<?php
error_reporting(E_ALL);
empty($_SESSION)? session_start() : print "";
include("./BD/info_bd.php");
?>

<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title> HereOuiGo - voyagez tranquille </title>
	<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
	<link rel="stylesheet" href="styles.css"/>
	<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
			<![endif] -->
		</head>
		<body>

			<?php
			include("./include/header.php");
			?>
			<div id="main">
				<?php
				if(isset($_POST['submit']) && isset($_SESSION['admin'])){
					$email = $_POST['pseudo'];
					$now = date("Y-m-d");
					$error_message = false;
					$error_str = "";
					try{
					// Connexion à la BDD
						$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
						$adresse_membre = "";
					// Vérifier que le membre existe déjà dans la BDD
						$req_verif = $bdd->prepare("SELECT * 
													FROM membre 
													WHERE mail = :email;");
						if($req_verif->execute(array('email' => $email))){

							$data = $req_verif->fetch();
							if(count($data) > 0 && $data != false){
								$pseudo_membre = $data['pseudo'];
								$adresse_membre = $data['fk_adresse'];
							// On cherche les id_offre des offres qu'il propose
								$req_verif = $bdd->prepare("SELECT offre.id_offre FROM offre, trajet WHERE offre.id_membre = :email AND offre.id_trajet = trajet.id_trajet AND trajet.date_trajet >= :now;");
								if($req_verif->execute(array("email" => $email, "now"=> $now))){
									$data = $req_verif->fetchAll();
									if(count($data) > 0){
									//Pour chaque trajet qu'il propose on récupère les mails des passagers 
										foreach($data as $ligne){
											$req_verif = $bdd->prepare("SELECT passager.mail FROM passager WHERE passager.id_offre = :offre;");
											if($req_verif->execute(array('offre' => $ligne['id_offre']))){
												$mails = $req_verif->fetchAll();
												if(count($mails) > 0){
													// Une offre a des passagers, on envoie un mail 
													foreach($mails as $mail){
														$a = $mail['mail'];
														$sujet = "Annulation d'un trajet";
														$message = "Cher utilisateur, \n Un trajet a été annulé. Veuillez consulter la rubrique 'Mes Trajets' accessible depuis votre compte. \n Veuillez nous excuser du désagrément occasionné, \n Bien cordialement, \n L'équipe HereOuiGo.";
														$entete = "De:  HereOuiGo\r\n";
														$entete .= "Content-type: text/plain; charset=UTF-8" . "\r\n";
														if(@mail($a,$sujet,$message,$entete)){
															// Bin ouais c'est cool
															print "";
														}
														else{
															$error_message = true;
															$error_str .= $mail['mail']. " ";
														}
													}
													if($error_message){
														echo "
															<div class='error_box'>
														<p>Une erreur est survenue lors de l'envoi des mails d'annulation aux passagers</p>
														<p>Elle concerne : {$error_str}</p>
														</div>";
													}

													// puis on les supprime de passager
													$req_verif = $bdd->prepare("DELETE FROM passager WHERE passager.id_offre = :id_offre;");
													$req_verif->execute(array("id_offre" => $ligne['id_offre']));


												}
												// L'offre n'a pas de passagers ou bien on a supprimé les passagers, on l'efface
												$req_verif = $bdd->prepare("DELETE FROM offre WHERE offre.id_offre = :id_offre ;");
												$req_verif->execute(array("id_offre" => $ligne['id_offre']));

											}else{
												print "";
												//Erreur execution requete recherche de passager
											}
										}
									}	
									
								}else{ 
								//Erreur
									print "";
								}
								// Il n'a aucune offre de disponible sur le site

								// On vérifie que ses véhicules ne sont pas liés à d'autres personnes
								$req_verif = $bdd->prepare("SELECT vehicule.immatriculation FROM vehicule, membre_vehicule WHERE vehicule.immatriculation = membre_vehicule.immatriculation AND membre_vehicule = :email;"); // Récupère les imatt de ses voitures
								if($req_verif->execute(array("email" => $email))){
									$voitures = $req_verif->fetchAll();
									foreach($voitures as $voiture){ // Pour chacune de ses voitures
										// On cherche si elle a d'autres propriétaires
										$immat = $voiture['immatriculation'];
										$req_verif = $bdd->prepare("SELECT membre_vehicule.mail FROM vehicule, membre_vehicule WHERE vehicule.immatriculation = membre_vehicule.immatriculation AND vehicule.immatriculation = :immat;");
										if(!$req_verif->execute(array("immat" => $immat))){
											// Erreur
											print "";
										}else{
											$nb_conducteurs = $req_verif->fetchAll();
											if(count($nb_conducteurs) < 3){
												// Il n'y a que lui comme conducteur 
												$req = $bdd->prepare("DELETE FROM vehicule WHERE vehicule.immatriculation=:immat;");
												if(!$req->execute(array("immat" => $immat))){
													//Erreur
													print "";
												}
											// D'autres conducteurs sont enregistrés sur la voiture, donc on fait rien
											}
										}
									}
								}

								// On le supprime de membre_vehicule
								$req_verif = $bdd->prepare("DELETE FROM membre_vehicule WHERE membre_vehicule.mail=:email;");
								if(!$req_verif->execute(array('email'=>$email))){
									echo "
										<div class='error_box'>
									<p>Une erreur est survenue lors de la suppression du membre de la table des conducteurs</p>
									<p>Veuillez contacter la personne en charge du développement de l'application</p>
									</div>";
								}


								// On l'efface des offres auxquelles il est passager

								$req_verif = $bdd->prepare("SELECT passager.id_offre FROM passager, offre, trajet WHERE passager.id_offre = offre.id_offre AND offre.id_trajet = trajet.id_trajet AND passager.mail=:email;");
								if($req_verif->execute(array("email" => $email))){
									$offres = $req_verif->fetchAll();
									foreach($offres as $offre){
										$num_offre = $offre['id_offre'];
										$req = $bdd->prepare("UPDATE offre SET nb_places = nb_places + 1 WHERE id_offre = :id_offre;");
										if(!$req->execute(array("id_offre" => $num_offre))){
											//Erreur
											print "";
										}else{
											// On l'efface du trajet
											$req = $bdd->prepare("DELETE FROM passager WHERE passager.mail=:email AND passager.id_offre = :id_offre;");
											if(!$req->execute(array("email" => $email, "id_offre" => $num_offre))){

											}
										}
										$req->closeCursor();
									}
																		
								}else{
									echo "
										<div class='error_box'>
									<p>Une erreur est survenue lors de la recherche des offres du membre à supprimer.</p>
									<p>Veuillez contacter la personne en charge du développement de l'application</p>
									</div>";
								}

								// On l'efface des membres tout court

								$req_verif = $bdd->prepare("DELETE FROM membre WHERE membre.mail=:email;");
								if(!$req_verif->execute(array("email" => $email))){
									// Mauvaise execution de la suppression membre
									echo "
										<div class='error_box'>
									<p>Une erreur est survenue lors de la suppression du membre</p>
									<p>Veuillez contacter la personne en charge du développement de l'application</p>
									</div>";
								}else{
									echo "
										<div class='valid_box'>
									<p>L'utilisateur {$pseudo_membre} ayant pour e-mail {$email} a bien été supprimé de l'application.</p>
									<p><a href='index.php?admin=TRUE'>Retourner au volet d'administration</a></p>
									</div>";
								}

								// On vérifie que son adresse n'est liée à personne d'autre avant de la supprimer
								$req_verif = $bdd->prepare("SELECT * FROM membre WHERE fk_adresse = :adresse_membre;");
								if($req_verif->execute(array("adresse_membre" => $adresse_membre))){
									$data = $req_verif->fetchAll();
									if(count($data) < 1){
										// Personne n'a la même adresse que lui, on supprime son adresse
										$req_verif = $bdd->prepare("DELETE FROM adresse WHERE id_adresse=:id_adresse;");
										if(!$req_verif->execute(array("id_adresse" => $adresse_membre))){
											// Mauvaise execution de l'adresse du membre
											echo "
												<div class='error_box'>
												<p>Une erreur est survenue lors de la suppression de l'adresse du membre.</p>
												<p>Veuillez contacter la personne en charge du développement de l'application</p>
												</div>";
										}
										// Sinon tant mieux, on fait rien
									}
									// Sinon on fait rien car quelqu'un d'autre habite à cette adresse
								}
								

								
							}else{
								include("./include/formulaire_delete_membre.php");
								echo "
										<div class='error_box'>
									<p>Cette adresse e-mail ne correspond à aucun membre</p>
									</div>";
							}
						}else{
							include("./include/formulaire_delete_membre.php");
							echo "
								<div class='error_box'>
								<p>Une erreur s'est produite lors de la recherche du membre, veuillez réessayer !</p>
								</div>";
						}
						// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
						$req_verif->closeCursor();
						 // Déconnexion de la BDD
                    	unset( $bdd );
				}catch(PDOException $e){
					print"Erreur ! : ".$e->getMessage()."</br>";
					die();
				}
			}elseif(isset($_SESSION['admin'])){
				include("./include/formulaire_delete_membre.php");
			}else{
				echo "<div class='error_box'>
					<p>Vous n'avez pas accès à cette demande !</p>
					</div>";
			}
	?>
	</div>
		<?php
			include("./include/footer.php");
		?>
	</body>
</html>