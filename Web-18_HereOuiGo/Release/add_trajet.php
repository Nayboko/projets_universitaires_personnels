<?php
error_reporting(E_ALL);
empty($_SESSION)? session_start() : print "";
include("./BD/info_bd.php");

	// Connexion à la BDD
$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
	$vitesse_moyenne = 80; // Vitesse moyenne d'un trajet en France

	function calculer_age($date_naissance){
		$date = $date_naissance;
		$arrnaiss = explode("-",$date);
		$arractu = array(date('Y'), date('m'),date('j'));
		$age=intval($arractu[0])-intval($arrnaiss[0]);
		/*si le mois est le même et que la différence entre le jour actuel et le jour de naissance est <0 alors on décrémente car la date anniversaire n'est pas passée*/
		if(((intval($arractu[1])-intval($arrnaiss[1]))==0)&&((intval($arractu[2])-intval($arrnaiss[2]))<0)){
			$age-=1;
		}

		return $age;
	}

	function dectosexa($x){
		$res = explode('.',(string)$x);
		$decimales = ($x - intval($x)) * 60;
		$heures = $res[0];
		$minutes = intval($decimales);

		return (string)$heures . ":" . $minutes;

	}


	/*
	Description : Calcul de la distance entre 2 points en fonction de leur latitude/longitude
	Auteur : Rajesh Singh (2014)
	Site web : AssemblySys.com */
	function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
		// Calcul de la distance en degrés
		$degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
		
		// Conversion de la distance en degrés à l'unité choisie (kilomètres, milles ou milles nautiques)
		switch($unit) {
			case 'km':
				$distance = $degrees * 111.13384; // 1 degré = 111,13384 km, sur base du diamètre moyen de la Terre (12735 km)
				break;
				case 'mi':
				$distance = $degrees * 69.05482; // 1 degré = 69,05482 milles, sur base du diamètre moyen de la Terre (7913,1 milles)
				break;
				case 'nmi':
				$distance =  $degrees * 59.97662; // 1 degré = 59.97662 milles nautiques, sur base du diamètre moyen de la Terre (6,876.3 milles nautiques)
			}
			return round($distance, $decimals);
		}

		$bad_price = false;


		?>

		<!doctype html>
		<html lang="fr">
		<head>
			<meta charset="utf-8"/>
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<title> HereOuiGo - voyagez tranquille </title>
			<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
			<link rel="stylesheet" href="styles.css"/>
			<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
			<![endif] -->
		</head>
		<body>
			<?php
			include("./include/header.php");
			?>
			<div id="main">
				<?php
			// Un utilisateur authentifié a executé l'ajout d'un trajet en tant que conducteur
				if(isset($_POST['submit']) && isset($_SESSION['auth'])){
					$id_membre = $_SESSION['mail'];
					$v_depart=strtoupper($_POST['ville_dep']);
					$v_arrivee=strtoupper($_POST['ville_arr']);
					$adr_rdv = $_POST['adr_rdv'];
					$adr_depot = $_POST['adr_depot'];
					$id_voiture = strtoupper($_POST['vehicule']);
					$prix = intval($_POST['prix']);
					$date_trajet=$_POST['date'];
					try{
					// On vérifie que le nom des villes de départ et d'arrivée sont existantes en France
						$req_villes_1 = $bdd->prepare("SELECT ville_nom FROM villes_france_free WHERE ville_nom = :vd");
						$req_villes_1->execute(array("vd" => $v_depart));
						$data_depart = $req_villes_1->fetch();
						$req_villes_2 = $bdd->prepare("SELECT ville_nom FROM villes_france_free WHERE ville_nom = :va;");
						$req_villes_2->execute(array("va" => $v_arrivee));
						$data_arrivee = $req_villes_2->fetch();


						if(count($data_depart) > 1 && count($data_arrivee) > 1){

						// On vérifie si le trajet est un trajet-type
							$req = $bdd->prepare("SELECT * FROM trajet_type WHERE ville_depart =:vd AND ville_arrivee = :va;");
							if($req->execute(array("vd"=>$v_depart, "va"=>$v_arrivee))){
								$data = $req->fetch();
								/* ### TRAJET-TYPE EXISTE ### */
								if(count($data) > 1){
								$reg_price = 0.10; // plafond €/km
								$user_price = number_format($prix / $data['distance'], 2);
									// On vérifie le rapport prix/distance
								if($user_price > $reg_price){
									print "<div class='error_box'><p>Ce trajet est référencé. Vous ne pouvez pas dépasser un tarif fixé à ".$reg_price ."€ par kilomètre.</p><p>Votre proposition : ".$user_price."€ par kilomètre pour une distance de ".$data['distance']." km.</p></div>";
									include("./include/formulaire_trajet.php");
									$bad_price = true;
										// Plutôt faire une redirection avec un message particulier récupéré en get
								}

							}else{
								/* ### TRAJET-TYPE N'EXISTE PAS ### */



									// On va calculer la distance entre ville_depart et ville_arrivee puis insérer un nouveau trajet-type
								$req = $bdd->prepare("SELECT ville_nom, ville_longitude_deg, ville_latitude_deg FROM villes_france_free WHERE ville_nom = :vd");

								if($req->execute(array("vd" => $v_depart))){
									$data_vd = $req->fetch();
									$req = $bdd->prepare("SELECT ville_nom, ville_longitude_deg, ville_latitude_deg FROM villes_france_free WHERE ville_nom = :va");
									if($req->execute(array("va" => $v_arrivee))){
										$data_va = $req->fetch();

										if(count($data_vd) > 1 && count($data_va) > 1){
													// On détermine la distance entre la VD et la VA
											$x_vd = $data_vd['ville_longitude_deg'];
											$y_vd = $data_vd['ville_latitude_deg'];
											$x_va = $data_va['ville_longitude_deg'];
											$y_va = $data_va['ville_latitude_deg'];

											$dist_vd_va = distanceCalculation($y_vd, $x_vd, $y_va, $x_va,'km',0);
											$temps_moyen = dectosexa($dist_vd_va / $vitesse_moyenne);

											$reg_price = 0.15;
											$user_price = $prix / $dist_vd_va;
									// On vérifie le rapport prix/distance
											if($user_price > $reg_price){
												print "<div class='error_box'><p>Vous ne pouvez pas dépasser un tarif fixé à ".$reg_price ."€ par kilomètre.</p><p>Votre proposition : ".$user_price."€ par kilomètre pour une distance de ".$dist_vd_va." km.</p></div>";
												include("./include/formulaire_trajet.php");
												$bad_price = true;
										// Plutôt faire une redirection avec un message particulier récupéré en get
											}

											$req_verif = $bdd->prepare("INSERT INTO trajet_type (ville_depart, ville_arrivee, distance, temps_moyen) VALUES(:v_depart, :v_arrivee, :distance, :temps_moyen);");
											if($req_verif->execute(array("v_depart"=>$v_depart,
												"v_arrivee" =>$v_arrivee,
												"distance" => $dist_vd_va,
												"temps_moyen" => $temps_moyen))){

														// On a réussi à créer un trajet type

											}else{
													// Erreur insertion trajet-type
												print "<div class='error_box'><p>Une erreur est survenue lors de la création d'un trajet-type.</p>
												<p>Veuillez renouveler votre saisie.</p></div>";
												include("./include/formulaire_trajet.php");
											}

										}else{
												// Erreur Recherche existence des villes données par l'Utilisateur
											print "<div class='error_box'><p>L'une des villes que vous avez renseigné n'existe pas.</p>
											<p>Veuillez renouveler votre saisie.</p></div>";
											include("./include/formulaire_trajet.php");
										}
									}else{
											// Erreur d'execution de la requête de recherche Longitude - latitude Ville Arrivée
										print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre saisie.</p></div>";
										include("./include/formulaire_trajet.php");
									}
								}else{
										// Erreur d'execution de la requête de recherche Longitude - latitude Ville Départ
									print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre saisie.</p></div>";
									include("./include/formulaire_trajet.php");
								}
							}

						}else{
							print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre saisie.</p></div>";
							include("./include/formulaire_trajet.php");
						}
						
						/* ### PARTIE TRAJET ### */
						// On a terminé de vérifier les trajets types
						// Maintenant on crée un trajet s'il n'existe pas déjà, sinon on récupère son identifiant id_trajet

						$req = $bdd->prepare("SELECT * FROM trajet WHERE ville_depart = :vd AND ville_arrivee = :va AND date_trajet = :dt");
						if($req->execute(array("vd" => $v_depart, "va" => $v_arrivee, "dt" => $date_trajet)) && $bad_price == false){
							$data = $req->fetch();
							if(count($data) > 1){
								// Trajet existe et on récupère simplement son identifiant
								$id_trajet = $data['id_trajet'];

							}else{
								// Trajet n'existe pas, on va donc le créer
								$req = $bdd->prepare("INSERT INTO trajet (ville_depart, ville_arrivee, date_trajet) VALUES(:v_depart, :v_arrivee, :date_trajet);");
								if($req->execute(array('v_depart'=>$v_depart, 'v_arrivee'=>$v_arrivee, 'date_trajet'=>$date_trajet))){
									// On va récupérer la clé id_trajet
									$req = $bdd->prepare("SELECT id_trajet FROM trajet WHERE ville_depart = :vd AND ville_arrivee = :va AND date_trajet = :dt");
									if($req->execute(array("vd"=>$v_depart, 'va'=>$v_arrivee, 'dt'=>$date_trajet))){
										$data = $req->fetch();
										if(count($data) > 1){
											// On a id_trajet, on l'enregistre
											$id_trajet = $data['id_trajet'];
										}else{
											print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre trajet.</p></div>";
											include("./include/formulaire_trajet.php");
										}
									}else{
										print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre trajet.</p></div>";
										include("./include/formulaire_trajet.php");
									}
								}else{
									print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre trajet.</p></div>";
									include("./include/formulaire_trajet.php");	
								}

							}	
						}else{
							print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre saisie.</p></div>";
							//include("./include/formulaire_trajet.php");
						}



						/* ### PARTIE OFFRE ### */

						if(isset($id_membre) && isset($id_trajet) && isset($id_voiture) && $bad_price == false){
							// On va récupérer le nombre de place de la voiture sélectionnée
							$req = $bdd->prepare("SELECT nombre_place FROM vehicule WHERE immatriculation = :id_voiture");
							if($req->execute(array("id_voiture" => $id_voiture))){
								$data = $req->fetch();
								if(count($data) > 1){
									$nb_places = $data['nombre_place'];
									$nb_places_dispo = $_POST['nb_places'];
									if($nb_places > $nb_places_dispo && $nb_places_dispo >= 1){

										$req = $bdd->prepare("INSERT INTO `offre` (`id_offre`, `id_trajet`, `id_membre`, `id_voiture`, `nb_places`, `prix`, `adr_rdv`, `adr_depot`) VALUES (:idf, :idt, :idm, :idvoit, :nb_places, :prix, :adr_rdv, :adr_depot);");

										if($req->execute(array("idf" => NULL, "idt" =>$id_trajet, "idm" => $id_membre, "idvoit" => $id_voiture, "nb_places" => $nb_places_dispo, "prix" => $prix, "adr_rdv" => $adr_rdv, "adr_depot" => $adr_depot))){
											echo "<div class='valid_box'><p>Votre offre a été créee avec succès!</p><p><a href='index.php'>Retourner à l'accueil</a></p></div>";

										}else{
											print "<div class='error_box'><p>Une erreur s'est produite lors de la création de l'offre.</p><p> Veuillez renouveler votre saisie ou contacter un administrateur <a href='contact.php'>via le formulaire de contact</a>.</p><br>
											</div>";
											include("./include/formulaire_trajet.php");
										}
									}else{
										print "<div class='error_box'><p>Le nombre de places disponible que vous avez signifié n'est pas cohérent. Veuillez renouveler votre saisie.</p><br>
										</div>";
										include("./include/formulaire_trajet.php");
									}
								}else{
									print "<div class='error_box'><p>Une erreur s'est produite lors de la récupération du nombre de place de votre véhicule. Veuillez renouveler votre saisie ou contacter un administrateur <a href='contact.php'>via le formulaire de contact</a>.</p></div>";
									//include("./include/formulaire_trajet.php");
								}

							}else{
								print "<div class='error_box'><p>Une erreur s'est produite lors de la détermination du nombre de place libre dans votre vehicule selectionné. Veuillez renouveler votre saisie.</p></div>";
								include("./include/formulaire_trajet.php");
							}
						}else{
							print "<div class='error_box'><p>Une erreur s'est produite, toutes les informations nécessaires à la création de l'offre n'ont pas été trouvée. Veuillez renouveler votre saisie.</p></div>";
							//include("./include/formulaire_trajet.php");
						}

					}else{
						print "<div class='error_box'><p>L'une des villes que vous avez saisie n'est pas référencée. Veuillez renouveler votre saisie ou contacter un administrateur <a href='contact.php'>via le formulaire de contact</a></p></div>";
						include("./include/formulaire_trajet.php");
					}
					// On oubli pas de  fermer le curseur d'analyse des résultats après l'utilisation
					$req->closeCursor();	
					$req_villes_1->closeCursor();
					$req_villes_2->closeCursor();

				}catch(PDOException $e){
					print "<div class='error_box'><p>Erreur ! : ".$e->getMessage()."</p></div>";
					include("./include/formulaire_trajet.php");
					die();
				}

			}

			/*### Affichage du formulaire si l'internaute est authentifié ### */
			elseif(isset($_SESSION['auth'])){
				try{
					$acces = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
							// On vérifie si l'utilisateur a au moins une voiture

					$req = $acces->prepare("SELECT * FROM membre_vehicule WHERE mail = :mail");
					if($req->execute(array("mail" => $_SESSION['mail']))){
						$data = $req->fetch();
						if(count($data) > 1){
									// L'Utilisateur a au moins un vehicule

							$req = $acces->prepare("SELECT date_naissance FROM membre WHERE mail = :mail");
							if($req->execute(array("mail"=> $_SESSION['mail']))){
								$data = $req->fetch();
								if(count($data) > 1){
									$age = calculer_age($data['date_naissance']);
									if($age < 18){
										print "<div class='error_box'><p>Veillez à avoir votre permis de conduite accompagnée.</p><p>Un accompagnateur titulaire du permis B depuis au moins 5 ans révolu et sans infraction grave doit être présent parmis les passagers</p></div>";
										include("./include/formulaire_trajet.php");

									}else{
										require("./include/formulaire_trajet.php");
									}

								}else{
									print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre trajet.</p></div>";
									include("./include/formulaire_trajet.php");								
								}
							}else{
								print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre trajet.</p></div>";
								include("./include/formulaire_trajet.php");
							}
						}else{
									// User n'a pas de vehicule, redirection vers add_vehicule.php
							header("Location: ./add_vehicule.php?msg=true");
							exit();
						}

					}else{
						print "<div class='error_box'><p>Une erreur s'est produite. Veuillez renouveler votre trajet.</p></div>";
						include("./include/formulaire_trajet.php");
					}	
							// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation
					$req->closeCursor();
							// Déconnexion de la BDD
					unset( $acces );

				}catch(PDOException $e){
					print "<div class='error_box'><p>Erreur ! : ".$e->getMessage()."</p></div>";
					include("./include/formulaire_trajet.php");
					die();
				}
			}else{
				echo "
				<div class='error_box'>
				<p>Vous n'avez pas accès à cette demande.</p>
				<a href='index.php'> Retourner à l'accueil </a>
				</div>";
			} 
		// Déconnexion de la BDD
			unset( $bdd );
			?>

		</div>
		<?php
		include("./include/footer.php");
		?>
	</body>
	</html>
