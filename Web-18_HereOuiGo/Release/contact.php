<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");
?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		<script src="./Scripts/contact.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>

			<?php
				include("./include/header.php");
			?>

			<div id="main">
			<?php
				/*ne fonctionne pas en local. il faudrait un serveur smtp*/
				if(isset($_POST['submit'])){
						$nom = $_POST['nom'];
						$email = $_POST['mail'];
						$message1 = $_POST['message'];
						$a = "contact@hereouigo.com";
						$sujet = "Formulaire de contact ";
						$message2 = "Nom: ".$nom."\r\n E-mail: ".$email."\r\n Message: ".$message1;
						$de = "HereOuiGo";
						$entete = "De: ".$de."\r\n";
						$entete.= "Content-type: text/plain; charset=UTF-8" . "\r\n";
						if(@mail($a,$sujet,$message2,$entete)){
							header("Location:conf_mess.php");
						}
						else{
							echo "<div class='error_box'><p>Une erreur s'est produite lors de l'envoie de votre message.</p>
								  <p><a href='index.php'> Retourner à l'accueil</a></p>
								  </div>";
						}
				}
				else{
					include("./include/formulaire_contact.php");
				}
			?>
			</div>
			<?php
				include("./include/footer.php");
			?>

	</body>
</html>