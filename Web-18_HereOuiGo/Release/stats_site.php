<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");
	include("./include/statistiques.php");

?>


<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>

	<?php
		include("./include/header.php");
	?>
	<div id="main">
		<?php
			if(isset($_SESSION['admin'])){
		?>
		<h2>Statistiques générales du site</h2>

		<section id="stats">
			<table>
				<tr>
					<td class="label">Nombre de membres</td>
					<td class="res"><?php echo compter_membre();?></td>
				</tr>

				<tr>
					<td class="label">Nombre d'offres en cours</td>
					<td class="res"><?php echo compter_offre(); ?></td>
				</tr>
	
				<tr>
					<td class="label">Nombre de trajets disponibles</td>
					<td class="res"><?php echo compter_trajet(); ?></td>
				</tr>

				<tr>
					<td class="label">Nombre d'avis déposés</td>
					<td class="res"><?php echo compter_avis(); ?></td>
				</tr>

			</table>

			
			<form action="stats_site.php" method="post" id='stats_trajet'>
				<h3>Recherche du prix moyen d'un trajet</h3>
				<label for='ville_dep'> Ville de départ : </label>
				<input type='text' name='ville_dep' pattern='^[a-zA-Z]*(-|\s)?([a-zA-Z]*)?' autofocus required/>
				<br>
				<label for='ville_arr'> Ville d'arrivée : </label>
				<input type='text' name='ville_arr' maxlength='40' pattern='^[a-zA-Z]*(-|\s)?([a-zA-Z]*)?'  required/>
				<br>
				<input type="submit" name="submit" value="Rechercher" />
			</form>
			<?php
				if(isset($_POST['submit'])){

					try{
						echo "<p>Prix moyen du trajet <strong class='bold_vd'>" . $_POST['ville_dep']."</strong> → <strong class='bold_va'>" . $_POST['ville_arr'] . "</strong> : <strong class='res'>" . (int)prix_moyen_trajet($_POST['ville_dep'], $_POST['ville_arr']). "</strong>€ </p>";
					}catch(PDOException $e){
						print"Erreur ! : ".$e->getMessage()."</br>";
						die();
					}
					
				}
			?>
			
		</section>

		<?php
	}else{
		echo "
                <div class='error_box'>
                <p>Vous n'avez pas accès à cette demande.</p>
                <a href='index.php'> Retourner à l'accueil </a>
                </div>";            
    }
		?>
		
	</div>
		<?php
			include("./include/footer.php");
		?>
	</body>
</html>
