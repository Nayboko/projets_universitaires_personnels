<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");
?>
<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>
		 
			<?php
				require("./include/header.php");?>
		<div id="main">
			<?php
				if(isset($_SESSION['auth'])){
			?>
			
				<h2>Vos avis</h2>
				<p>Vous pouvez poser un avis sur les différents covoiturages dans lesquels vous avez été conducteur ou passager.</p>

			<?php
				try{
					$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
					$email = $_SESSION['mail'];
					$now = date("Y-m-d");


					/* Génération des avis en tant que passager */

					// On va récupérer les informations où l'user connecté a été passager dans une offre passée et où il n'a pas donné d'avis
					echo "<h3 class='avis_h3'>En tant que passager : </h3>";
					/*$passager = $bdd->prepare("
						SELECT DISTINCT trajet.ville_depart, trajet.ville_arrivee, trajet.date_trajet, offre.id_offre, offre.id_membre, offre.prix, membre.pseudo, trajet_type.distance FROM passager, offre, trajet, trajet_type, membre WHERE passager.id_offre = offre.id_offre AND offre.id_trajet = trajet.id_trajet AND trajet_type.ville_depart = trajet.ville_depart AND trajet_type.ville_arrivee = trajet.ville_arrivee AND passager.mail = :email AND date_trajet < :now AND offre.id_membre = membre.mail AND offre.id_offre NOT IN (SELECT avis.id_offre FROM avis WHERE avis.id_membre = :user);");*/
					$passager = $bdd->prepare("SELECT trajet.ville_depart, trajet.ville_arrivee, trajet.date_trajet, offre.id_offre, offre.id_membre, offre.prix FROM passager, offre, trajet WHERE passager.mail = :email AND passager.id_offre = offre.id_offre AND trajet.date_trajet < :now AND offre.id_trajet = trajet.id_trajet AND passager.id_offre NOT IN (SELECT avis.id_offre FROM avis WHERE avis.id_membre = :user);");
					if($passager->execute(array("email" => $email, "now" => $now, "user" =>$email))){
						$data = $passager->fetchAll();
						
						//var_dump($data);
						if(count($data) > 0){
							echo "<table class='avis_passager'>
								<tr>
									<th>Offre</th>
									<th>Prix</th>
									<th>Avis</th>

								</tr>";
							// L'User a été passager dernièrement
							foreach($data as $row){
								// Note et commentaire pour chaque trajet
								echo "<tr>
										<td class='info_passager'><p><strong class='bold_vd'>{$row['ville_depart']}</strong> → <strong class='bold_va'>{$row['ville_arrivee']}</strong> </p>
										<p>le {$row['date_trajet']} avec <strong>{$row['id_membre']}</strong></p></td>
										<td class='prix_passager'>{$row['prix']}€</td>

										<td class='form_passager'>
										<form method='post' action='add_avis.php' id=".$row['id_offre'].">
												<textarea name='commentaire' class='commentaire' placeholder='Veuillez saisir votre message' rows='10' cols='50'></textarea>
												<label for='note'>Note</label>
												<select name='note' required>
													<option value='1'>1</option>
													<option value='2'>2</option>
													<option value='3'>3</option>
													<option value='4'>4</option>
													<option value='5'>5</option>
												</select>
												<button type='submit' name='avis' value=".$row['id_offre']." form=".$row['id_offre'].">Envoyer</button>
											</form></td>										
									</tr>";
							}
							echo "</table>";
						}else{
							// L'User n'a pas été passager dernièrement ou a déjà déposé ses avis
							echo "<div class='error_box'><p>Vous n'avez aucun avis à déposer !</p><p><a href='rechercher_trajet.php' class='green_button'>Consultez les offres !</a></p></div>";
						}

					}else{
						print "<div class='error_box'><p>Une erreur est survenue durant la recherche des covoiturages auxquels vous êtiez passagers. Veuillez nous excuser du désagrément.</p></div>";
					}


					/* Génération des avis en tant que conducteur */

					echo "<h3 class='avis_h3'>En tant que conducteur : </h3>";
					$req = $bdd->prepare("SELECT * 
											FROM offre, membre_vehicule, vehicule, trajet, trajet_type
											WHERE offre.id_membre = :email
											AND offre.id_membre = membre_vehicule.mail
											AND membre_vehicule.immatriculation = vehicule.immatriculation
											AND offre.id_trajet = trajet.id_trajet
											AND offre.id_voiture = vehicule.immatriculation
											AND trajet.ville_depart = trajet_type.ville_depart
											AND trajet.ville_arrivee = trajet_type.ville_arrivee
											AND trajet.date_trajet < :now AND offre.id_offre NOT IN (SELECT avis.id_offre FROM avis WHERE avis.id_membre = :user);");
					if($req->execute(array("email" => $email, "now" => $now, "user" => $email))){
						$offres = $req->fetchAll();

						if(count($offres) > 0){
							echo "<table class='avis_passager'>
								<tr>
									<th>Offre</th>
									<th>Passagers</th>
									<th>Avis</th>

								</tr>";
							// L'User a été passager dernièrement
							foreach($offres as $row){
								// Note et commentaire pour chaque trajet
								$req = $bdd->prepare("SELECT membre.pseudo
														FROM membre, passager
														WHERE passager.id_offre = :offre
														AND passager.mail = membre.mail;");
								$req->execute(array("offre" => $row['id_offre']));
								$passagers = $req->fetchAll();

								echo "<tr>
										<td class='info_conducteur'><p><strong class='bold_vd'>{$row['ville_depart']}</strong> → <strong class='bold_va'>{$row['ville_arrivee']}</strong> </p><p><em>({$row['distance']} km)</em></p>
										<p>le {$row['date_trajet']} avec <strong>{$row['marque']} {$row['modele']} ({$row['immatriculation']})</strong></p></td>

										<td class='liste_passagers'>";

										if(count($passagers)> 0){
											foreach ($passagers as $passager) {
												echo "<p><a href='profil.php?pseudo={$passager['pseudo']}' title='Consulter le profil de {$passager['pseudo']}' alt='Consulter le profil de {$passager['pseudo']}'>{$passager['pseudo']}</a></p>";
											}
										}else{
											echo "<p>Aucun passager enregistré</p>";
										}

										echo "</td>

										<td class='form_passager'>
										<form method='post' action='add_avis.php' id=".$row['id_offre'].">
												<textarea name='commentaire' class='commentaire' placeholder='Veuillez saisir votre message' rows='10' cols='50'></textarea>
												<label for='note_passager'>Note</label>
												<select name='note' required>
													<option value='1'>1</option>
													<option value='2'>2</option>
													<option value='3'>3</option>
													<option value='4'>4</option>
													<option value='5'>5</option>
												</select>
												<button type='submit' name='avis' value=".$row['id_offre']." form=".$row['id_offre'].">Envoyer</button>
											</form></td>										
									</tr>";
							}
							echo "</table>";

						}else{
							// L'User n'a pas été conducteur dernièrement ou a déjà déposé ses avis
							echo "<div class='error_box'><p>Vous n'avez aucun avis à déposer !</p><p><a href='add_trajet.php' class='green_button'>Proposez un trajet !</a></p></div>";
						}

					}else{
						print "<div class='error_box'><p>Une erreur est survenue durant la recherche des covoiturages auxquels vous êtiez passagers. Veuillez nous excuser du désagrément.</p></div>";
					}
					// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation
					$req->closeCursor();
					// Déconnexion de la BDD
					unset( $bdd );
				}
				catch(PDOException $e){
					print "<div class='error_box'><p>Erreur ! : ".$e->getMessage()."</p></div>";
					die();
				}
			?>



			<?php
				}else{
					echo "
					<div class='error_box'>
					<p>Vous n'avez pas accès à cette demande.</p>
					<a href='index.php'> Retourner à l'accueil </a>
					</div>";
				}
				require("./include/footer.php");
			?>
		</div>
	</body>
</html>