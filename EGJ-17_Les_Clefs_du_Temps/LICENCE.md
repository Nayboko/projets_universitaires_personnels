# Termes de la Licence de l'application "Les Clefs du Temps"

## ARTICLE 9 DU REGLEMENT DE L' EDU GAME JAM 2017

Les réalisations exécutées dans le cadre de la Game Jam sont des exercices tombant sous l’égide des textes de lois régissant les
droits d’auteurs et la propriété intellectuelle. Sauf demande explicite écrite adressée à l’organisation, l’inscription à l’événement
engage les participants placer leur réalisation sous Licence Creative Commons CC-BY-NC-SA
(https://creativecommons.org/licenses/by-nc-sa/3.0/fr/ : citation de l’auteur, pas d’utilisation commerciale, autorisation d’adapter le
travail sous condition qu’il soit repartagé aux mêmes conditions). Les participants concèdent également à l’organisation les droits
d’exploitation sur tout support de communication des travaux réalisés dans le cadre de cette opération. L’organisation s’engage à 
ne faire aucun commerce de quelque nature (vente, échange, etc...) des travaux réalisés par les candidats dans le cadre de la
Game Jam.

## CONDITIONS D'UTILISATION

