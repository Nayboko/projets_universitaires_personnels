package utils.go;

import java.awt.geom.AffineTransform;
import java.util.ArrayList;

public class Transformation {
	AffineTransform af;

	public Transformation(){

	}

	/**
	 * @brief Effectue la rotation des points d'un triangle selon un angle donne
	 * @param Triangle Triangle qui va subir la rotation
	 * @param angle Angle donnee en degre
	 * @return Liste des nouveaux points du triangle apres la rotation
	 */
	public ArrayList<PointVisible> rotation(Triangle t, double angle){
		ArrayList<PointVisible> p = new ArrayList<>(); // Futurs points
		af = new AffineTransform(Math.cos(angleToRadian(angle)), Math.sin(angleToRadian(angle)), -Math.sin(angleToRadian(angle)), Math.cos(angleToRadian(angle)), 0, 0);
		ArrayList<PointVisible> points = new ArrayList<PointVisible>(); // Points actuels du triangle
		points.add(t.getP1());
		points.add(t.getP2());
		points.add(t.getP3());
		PointVisible centre = t.getCentre();
		for(PointVisible point : points){
			PointVisible newPoint = new PointVisible(0.0, 0.0);
			point = translation(point, -centre.x, -centre.y);
			af.transform(point.P, newPoint.P);
			newPoint = translation(newPoint, centre.x, centre.y);
			newPoint.copyCoords();
			newPoint.poids = point.poids;
			newPoint.idTri = point.idTri;
			p.add(newPoint);
		}
		return p;
	}
	
	/**
	 * @brief Effectue une translation d'un point
	 * @param p Point à translater
	 * @param _x Coordonnée en x
	 * @param _y Coordonnée en y
	 * @return Retourne un point translaté par rapport à p selon les coordonées _x et _y
	 */
	public PointVisible translation(PointVisible p, int _x, int _y){
		p.P.x += _x;
		p.P.y += _y;
		p.copyCoords();
		return p;
	}


	/**
	 * @brief Conversion d'un angle donné en degré en radian
	 * @param angle Valeur de l'angle en degré
	 * @return Retourne un angle en radian
	 */
	private double angleToRadian(double angle){
		return angle*Math.PI/180.0;
	}
}
