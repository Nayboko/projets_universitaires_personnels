package utils.go;

import java.awt.Color;
import java.awt.Graphics2D;
import java.lang.Math;


public class Etoile {
	double origineX;
	double origineY;
	double length;
	
	public Etoile(double OrigineX, double OrigineY, double Length) {
		this.origineX = OrigineX;
		this.origineY = OrigineY;
		this.length = Length;
	}
	
	public void Draw(Graphics2D g){
		double x2 = 0.0;
		double y2 = 0.0;
		double x1 = this.getOrigineX() + this.getLength();
		double y1 = this.getOrigineY();
		g.setColor(Color.BLACK);
		double angleLine = 0.0;
		
		g.drawLine((int) this.getOrigineX() , (int) this.getOrigineY()  , (int) x1, (int) y1);
		
		for(int i = 0; i < 8; i++) {
			angleLine += 45.0;
			x2 = this.getOrigineX() + (this.getLength() * Math.cos(Math.toRadians(angleLine)));
			y2 = this.getOrigineY() + (this.getLength() * Math.sin(Math.toRadians(angleLine)));
			g.drawLine((int) this.getOrigineX() , (int) this.getOrigineY()  , (int) x2, (int) y2);
		}
	}
	double getOrigineX() {
		return this.origineX;
	}
	double getOrigineY() {
		return this.origineY;
	}
	double getLength() {
		return this.length;
	}
	void setOrigineX(double x) {
		this.origineX = x;
	}
	void setOrigineY(double y) {
		this.origineY = y;
	}
	void setLength(double l) {
		this.length = l;
	}
	
}
